package com.myntra.portal.crm.dao.impl;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.apache.log4j.Logger;

import com.myntra.commons.dao.impl.BaseDAOImpl;
import com.myntra.commons.exception.ERPServiceException;
import com.myntra.portal.crm.constant.CRMPortalServiceConstants;
import com.myntra.portal.crm.dao.CustomerOrderDAO;
import com.myntra.portal.crm.entity.CodOnHoldLogEntity;
import com.myntra.portal.crm.entity.CustomerOrderCommentEntity;
import com.myntra.portal.crm.entity.OrderExchangeEntity;
import com.myntra.portal.crm.entity.OrderPaymentLogEntity;
import com.myntra.portal.crm.entity.OrderRtoEntity;

/**
 * DAO for order related data
 * 
 * @author Arun Kumar
 */
public class CustomerOrderDAOImpl extends BaseDAOImpl<OrderPaymentLogEntity> implements CustomerOrderDAO {

	private static final Logger LOGGER = Logger.getLogger(CustomerOrderDAOImpl.class);

	@Override
	public OrderPaymentLogEntity getOrderPaymentLog(Long orderId) throws ERPServiceException {

		if (orderId == null) {
			throw new ERPServiceException("Order id is null, please provide order id", 0);
		}

		Object[] obj;
		try {
			Query q = null;
			q = getEntityManager().createNamedQuery(
					CRMPortalServiceConstants.NAMED_QUERIES.ORDER_PAYMENT_LOG_BY_ORDERID);
			q.setParameter("orderId", orderId);

			obj = (Object[]) q.getSingleResult();
		} catch (NoResultException nre) {
			LOGGER.debug("No Payment Log data in Old Portal Database");
			return null;
		}

		int index = 0;

		Long logId = null;
		Long order = null;
		Long binNumber = null;

		Integer logIdInt = (Integer) obj[index++];
		if (logIdInt != null) {
			logId = Long.valueOf(logIdInt);
		}

		String login = (String) obj[index++];

		Integer orderInt = (Integer) obj[index++];
		if (orderInt != null) {
			order = Long.valueOf(orderInt);
		}
		String userAgent = (String) obj[index++];
		String iPAddress = (String) obj[index++];
		String gateway = (String) obj[index++];
		String paymentOption = (String) obj[index++];
		String paymentIssuer = (String) obj[index++];
		String bankTransactionId = (String) obj[index++];
		String gatewayPaymentId = (String) obj[index++];
		String completedVia = (String) obj[index++];
		String cardBankName = (String) obj[index++];

		Integer binNumberInt = (Integer) obj[index++];
		if (binNumberInt != null) {
			binNumber = Long.valueOf(binNumberInt);
		}

		Double amount = null;
		BigDecimal amountBigDecimal = (BigDecimal) obj[index++];
		if (amountBigDecimal != null) {
			amount = amountBigDecimal.doubleValue();
		}

		Double amountPaid = null;
		String amountPaidString = (String) obj[index++];
		if (amountPaidString != null) {
			amountPaid = Double.parseDouble(amountPaidString);
		}

		Double amountToBePaid = null;
		String amountToBePaidString = (String) obj[index++];
		if (amountToBePaidString != null) {
			amountToBePaid = Double.parseDouble(amountToBePaidString);
		}

		Boolean isInline = (Boolean) obj[index++];
		Boolean isTampered = (Boolean) obj[index++];
		Boolean isComplete = (Boolean) obj[index++];
		Boolean isFlagged = (Boolean) obj[index++];
		String responseCode = (String) obj[index++];
		String responseMessage = (String) obj[index++];

		Date logInsertTime = null;
		Integer logInsertInt = (Integer) obj[index++];
		if (logInsertInt != null) {
			logInsertTime = new Date(logInsertInt * 1000L);
		}

		Date returnTime = null;
		Integer returnInt = (Integer) obj[index++];
		if (returnInt != null) {
			returnTime = new Date(returnInt * 1000L);
		}

		Date transactionTime = null;
		Integer transactionInt = (Integer) obj[index++];
		if (transactionInt != null) {
			transactionTime = new Date(transactionInt * 1000L);
		}

		OrderPaymentLogEntity orderPaymentLogEntity = new OrderPaymentLogEntity(logId, login, order, userAgent,
				iPAddress, gateway, paymentOption, paymentIssuer, bankTransactionId, gatewayPaymentId, completedVia,
				cardBankName, binNumber, amount, amountPaid, amountToBePaid, isInline, isTampered, isComplete,
				isFlagged, responseCode, responseMessage, logInsertTime, returnTime, transactionTime);

		LOGGER.debug("orderPaymentLogEntity : " + orderPaymentLogEntity.toString());
		return orderPaymentLogEntity;

	}

	@Override
	public List<CustomerOrderCommentEntity> getCustomerOrderCommentLog(Long orderId) throws ERPServiceException {
		if (orderId == null) {
			throw new ERPServiceException("Order Id for comment is null.", 0);
		}

		Query q = null;
		q = getEntityManager().createNamedQuery(
				CRMPortalServiceConstants.NAMED_QUERIES.CUSTOMER_ORDER_COMMENT_BY_ORDERID);
		q.setParameter("orderId", orderId);

		List<Object[]> list = q.getResultList();

		List<CustomerOrderCommentEntity> customerOrderCommentEntityList = new ArrayList<CustomerOrderCommentEntity>();

		for (Object[] objArray : list) {

			int index = 0;

			Long commentId = null;
			Integer commentIdInt = (Integer) objArray[index++];
			if (commentIdInt != null) {
				commentId = Long.valueOf(commentIdInt);
			}

			Long order = null;
			Integer orderInt = (Integer) objArray[index++];
			if (orderInt != null) {
				order = Long.valueOf(orderInt);
			}

			String type = (String) objArray[index++];
			String title = (String) objArray[index++];
			String commentBy = (String) objArray[index++];
			String description = (String) objArray[index++];

			Date commentDate = null;
			Integer commentDateInt = (Integer) objArray[index++];
			if (commentDateInt != null) {
				commentDate = new Date(commentDateInt * 1000L);
			}

			String newAddress = (String) objArray[index++];
			String giftWrapMessage = (String) objArray[index++];

			String attachmentName = (String) objArray[index++];

			CustomerOrderCommentEntity customerOrderCommentEntity = new CustomerOrderCommentEntity(commentId, order,
					type, title, commentBy, description, newAddress, giftWrapMessage, commentDate, attachmentName);

			customerOrderCommentEntityList.add(customerOrderCommentEntity);
		}

		return customerOrderCommentEntityList;
	}

	@Override
	public List<CodOnHoldLogEntity> getCodOnHoldLog(Long orderId) throws ERPServiceException {
		if (orderId == null) {
			throw new ERPServiceException("Order Id for COD on hold reason log is null.", 0);
		}

		Query q = null;
		q = getEntityManager().createNamedQuery(CRMPortalServiceConstants.NAMED_QUERIES.COD_ORDER_OH_LOG_BY_ORDERID);
		q.setParameter("orderId", orderId);

		List<Object[]> list = q.getResultList();

		List<CodOnHoldLogEntity> onHoldOrderLogEntityList = new ArrayList<CodOnHoldLogEntity>();

		for (Object[] objArray : list) {

			int index = 0;

			Long order = null;
			Integer orderIdInt = (Integer) objArray[index++];
			if (orderIdInt != null) {
				order = Long.valueOf(orderIdInt);
			}

			String disposition = (String) objArray[index++];
			String reason = (String) objArray[index++];
			String reasonDisplayName = (String) objArray[index++];

			Integer trialNumber = (Integer) objArray[index++];
			;

			String createdBy = (String) objArray[index++];
			String comment = (String) objArray[index++];

			Date createdDate = null;
			Timestamp createdDateTime = (Timestamp) objArray[index++];
			if (createdDateTime != null) {
				createdDate = new Date(createdDateTime.getTime());
			}

			CodOnHoldLogEntity onHoldOrderLogEntity = new CodOnHoldLogEntity(order, disposition, reason,
					reasonDisplayName, trialNumber, createdBy, comment, createdDate);

			onHoldOrderLogEntityList.add(onHoldOrderLogEntity);
		}

		return onHoldOrderLogEntityList;
	}

	@Override
	public List<OrderExchangeEntity> getExchangeDetail(Long orderId) throws ERPServiceException {
		if (orderId == null) {
			throw new ERPServiceException("Order id is null, please provide order id", 0);
		}

		Query q = null;
		q = getEntityManager().createNamedQuery(CRMPortalServiceConstants.NAMED_QUERIES.EXCHANGE_DETAIL_BY_ORDERID);
		q.setParameter("orderId", orderId);

		List<Object[]> list = q.getResultList();

		List<OrderExchangeEntity> orderExchangeEntityList = new ArrayList<OrderExchangeEntity>();

		for (Object[] objArray : list) {

			int index = 0;

			Long exchangeOrderId = null;
			Integer exchangeOrderIdInt = (Integer) objArray[index++];
			if (exchangeOrderIdInt != null) {
				exchangeOrderId = Long.valueOf(exchangeOrderIdInt);
			}

			Long shipmentId = null;
			Integer shipmentIdInt = (Integer) objArray[index++];
			if (shipmentIdInt != null) {
				shipmentId = Long.valueOf(shipmentIdInt);
			}

			Long itemId = null;
			Integer itemIdInt = (Integer) objArray[index++];
			if (itemIdInt != null) {
				itemId = Long.valueOf(itemIdInt);
			}

			Long returnId = null;
			Integer returnIdInt = (Integer) objArray[index++];
			if (returnIdInt != null) {
				returnId = Long.valueOf(returnIdInt);
			}

			String returnType = (String) objArray[index++];

			if (returnType != null && returnType.equalsIgnoreCase("RTO")) {
				// We need to ignore RTO orders to be added in the list.
			} else {
				OrderExchangeEntity exchangeEntity = new OrderExchangeEntity(exchangeOrderId, shipmentId, itemId,
						returnId, returnType);
				orderExchangeEntityList.add(exchangeEntity);
			}
		}

		return orderExchangeEntityList;

	}

	@Override
	public List<OrderRtoEntity> getOrderRtoDetail(Long orderId) throws ERPServiceException {
		if (orderId == null) {
			throw new ERPServiceException("Order Id for RTO detail is null.", 0);
		}

		Query q = null;
		q = getEntityManager().createNamedQuery(CRMPortalServiceConstants.NAMED_QUERIES.RTO_BY_ORDERID);
		q.setParameter("orderId", orderId);

		List<Object[]> list = q.getResultList();

		List<OrderRtoEntity> orderRtoEntityList = new ArrayList<OrderRtoEntity>();

		for (Object[] objArray : list) {

			int index = 0;

			Long ordId = null;
			BigInteger ordIdBigInt = (BigInteger) objArray[index++];
			if (ordIdBigInt != null) {
				ordId = ordIdBigInt.longValue();
			}
			
			String rtoStatus = (String) objArray[index++];
			
			OrderRtoEntity orderRtoEntity = new OrderRtoEntity(ordId, rtoStatus);

			orderRtoEntityList.add(orderRtoEntity);
		}

		return orderRtoEntityList;
	}
}