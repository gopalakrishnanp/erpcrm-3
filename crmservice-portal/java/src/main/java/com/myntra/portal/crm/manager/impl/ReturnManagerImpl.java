package com.myntra.portal.crm.manager.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.myntra.commons.codes.StatusResponse;
import com.myntra.commons.exception.ERPServiceException;
import com.myntra.commons.manager.impl.BaseManagerImpl;
import com.myntra.commons.response.AbstractResponse;
import com.myntra.portal.crm.client.code.CRMPortalSuccessCodes;
import com.myntra.portal.crm.client.entry.ReturnCommentEntry;
import com.myntra.portal.crm.client.entry.ReturnEntry;
import com.myntra.portal.crm.client.response.ReturnResponse;
import com.myntra.portal.crm.dao.CustomerReturnDAO;
import com.myntra.portal.crm.entity.CustomerReturnCommentEntity;
import com.myntra.portal.crm.entity.CustomerReturnEntity;
import com.myntra.portal.crm.manager.ReturnManager;

/**
 * Manager implementation for customer return web service which integrates
 * detail of return and return comments
 * 
 * @author Arun Kumar
 */
public class ReturnManagerImpl extends BaseManagerImpl<ReturnResponse, ReturnEntry> implements ReturnManager {

	private static final Logger LOGGER = Logger.getLogger(ReturnManagerImpl.class);

	private CustomerReturnDAO customerReturnDAO;

	public CustomerReturnDAO getCustomerReturnDAO() {
		return customerReturnDAO;
	}

	public void setCustomerReturnDAO(CustomerReturnDAO customerReturnDAO) {
		this.customerReturnDAO = customerReturnDAO;
	}

	@Override
	public ReturnResponse getReturnDetail(Long returnId, Long orderId, String login) throws ERPServiceException {

		// Initialise customer return response and entry
		ReturnResponse response = new ReturnResponse();
		List<ReturnEntry> custReturnEntryList = new ArrayList<ReturnEntry>();

		try {

			List<CustomerReturnEntity> custReturnEntityList = (List<CustomerReturnEntity>) ((CustomerReturnDAO) getCustomerReturnDAO())
					.getCustomerReturnData(returnId, orderId, login);

			if (custReturnEntityList != null) {

				for (CustomerReturnEntity singleReturnEntity : custReturnEntityList) {
					ReturnEntry singleReturnEntry = toCustomerReturnEntry(singleReturnEntity);

					// add return comments
					List<CustomerReturnCommentEntity> customerReturnCommentEntityList = new ArrayList<CustomerReturnCommentEntity>();
					List<ReturnCommentEntry> customerReturnCommentEntryList = new ArrayList<ReturnCommentEntry>();
					customerReturnCommentEntityList = (List<CustomerReturnCommentEntity>) ((CustomerReturnDAO) getCustomerReturnDAO())
							.getCustomerReturnCommentData(singleReturnEntry.getReturnId());

					if (customerReturnCommentEntityList != null && customerReturnCommentEntityList.size() > 0) {
						for (CustomerReturnCommentEntity singleReturnCommentEntity : customerReturnCommentEntityList) {
							ReturnCommentEntry singleReturnCommentEntry = toCustomerReturnCommentEntry(singleReturnCommentEntity);
							customerReturnCommentEntryList.add(singleReturnCommentEntry);
						}
						singleReturnEntry.setReturnCommentEntry(customerReturnCommentEntryList);
					}

					custReturnEntryList.add(singleReturnEntry);
				}
			}
			
			response.setReturnEntryList(custReturnEntryList);

		} catch (ERPServiceException e) {
			LOGGER.error("Error validating/retreiving data", e);
			StatusResponse error = new StatusResponse(e.getCode(), e.getMessage(), StatusResponse.Type.ERROR);
			response.setStatus(error);
			throw new ERPServiceException(response);

		} catch (Exception e) {
			LOGGER.error("error", e);
			StatusResponse error = new StatusResponse(1, e.getMessage(), StatusResponse.Type.ERROR);
			response.setStatus(error);
			throw new ERPServiceException(response);
		}

		StatusResponse success = new StatusResponse(CRMPortalSuccessCodes.CUSTOMER_RETURN_RETRIEVED,
				StatusResponse.Type.SUCCESS, response.getReturnEntryList().size());
		response.setStatus(success);

		return response;
	}

	private ReturnEntry toCustomerReturnEntry(CustomerReturnEntity cre) {

		if (cre == null) {
			return null;
		}

		return new ReturnEntry(cre.getReturnId(), cre.getOrderId(), cre.getItemId(), cre.getSkuCode(), cre.getQuantity(), cre.getSize(),
				cre.getReturnStatus(), cre.getLogin(), cre.getCustomerName(), cre.getReturnAddress(), cre.getCity(),
				cre.getState(), cre.getCountry(), cre.getZipCode(), cre.getMobile(), cre.getReturnMode(),
				cre.getCourierService(), cre.getTrackingNumber(), cre.getReturnReason(), cre.getReturnDescription(),
				cre.getPickupCharge(), cre.getRefundAmount(), cre.getReturnCreatedDate(), cre.getDCCode(),
				cre.getIsRefunded(), cre.getRefundDate(), cre.getRefundModeCredit(), cre.getReturnStatusCode(),
				cre.getReturnStatusName(), cre.getReturnStatusDescription(), cre.getItemBarCode(), cre.getWareHouseId());
	}

	private ReturnCommentEntry toCustomerReturnCommentEntry(CustomerReturnCommentEntity crce) {

		if (crce == null) {
			return null;
		}

		return new ReturnCommentEntry(crce.getReturnComment(), crce.getReturnCommentBy(),
				crce.getReturnCommentDescription(), crce.getReturnCommentCreatedDate(), crce.getImageURL());

	}

	@Override
	public AbstractResponse search(int i, int i1, String string, String string1, String string2)
			throws ERPServiceException {
		throw new UnsupportedOperationException("Not supported yet. test by arun");
	}

	@Override
	public AbstractResponse findById(Long l) throws ERPServiceException {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	@Override
	public AbstractResponse create(ReturnEntry entry) throws ERPServiceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public AbstractResponse update(ReturnEntry entry, Long itemId) throws ERPServiceException {
		// TODO Auto-generated method stub
		return null;
	}
}