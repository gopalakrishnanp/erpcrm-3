/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myntra.portal.crm.service;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import com.myntra.commons.exception.ERPServiceException;
import com.myntra.commons.response.AbstractResponse;
import com.myntra.commons.service.BaseService;
import com.myntra.portal.crm.client.entry.CustomerProfileEntry;
import com.myntra.portal.crm.client.response.CustomerProfileResponse;

/**
 *
 * @author pravin
 */
@Path("/customerprofile/")
public interface CustomerProfileService extends BaseService<CustomerProfileResponse, CustomerProfileEntry>{

    @GET
    @Consumes({"application/xml", "application/json"})
    @Produces({"application/xml", "application/json"})
    @Path("/")
    AbstractResponse getCustomerProfileByLogin(
            @QueryParam("login") String login) throws ERPServiceException;
    
    @GET
    @Consumes({"application/xml", "application/json"})
    @Produces({"application/xml", "application/json"})
    @Path("/mobile")
    AbstractResponse getCustomerProfileByMobile(
            @QueryParam("mobile") String mobile) throws ERPServiceException;

    
    //@POST
    //@Consumes({"application/xml", "application/json"})
    //@Produces({"application/xml", "application/json"})
    //@Path("/my/create/")
    AbstractResponse createCustomerProfile() throws ERPServiceException;
}
