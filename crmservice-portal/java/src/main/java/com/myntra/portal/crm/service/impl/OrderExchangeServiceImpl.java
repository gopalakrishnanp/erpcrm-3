package com.myntra.portal.crm.service.impl;

import org.springframework.transaction.TransactionSystemException;

import com.myntra.commons.exception.ERPServiceException;
import com.myntra.commons.response.AbstractResponse;
import com.myntra.commons.service.impl.BaseServiceImpl;
import com.myntra.portal.crm.client.entry.OrderExchangeEntry;
import com.myntra.portal.crm.client.response.OrderExchangeResponse;
import com.myntra.portal.crm.manager.OrderExchangeManager;
import com.myntra.portal.crm.service.OrderExchangeService;

/**
 * Order Exchange Details Service
 * 
 * @author Arun Kumar
 */
public class OrderExchangeServiceImpl extends
		BaseServiceImpl<OrderExchangeResponse, OrderExchangeEntry> implements OrderExchangeService {

	
	/*
	 public AbstractResponse getOrderCommentLog(Long orderId) {
		try {
			return ((OrderCommentLogManager) getManager()).getOrderCommentLog(orderId);

		} catch (ERPServiceException e) {
			return e.getResponse();

		} catch (TransactionSystemException ex) {
			return ((ERPServiceException) ex.getApplicationException()).getResponse();
		}

	}
	*/

	@Override
	public AbstractResponse getExchangeDetail(Long orderId) {
		try {
			return ((OrderExchangeManager) getManager()).getExchangeDetail(orderId);
		} catch (ERPServiceException e) {
			return e.getResponse();

		} catch (TransactionSystemException ex) {
			return ((ERPServiceException) ex.getApplicationException()).getResponse();
		}
	}
}
