package com.myntra.portal.crm.dao.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Query;

import org.apache.log4j.Logger;

import com.myntra.commons.dao.impl.BaseDAOImpl;
import com.myntra.commons.exception.ERPServiceException;
import com.myntra.portal.crm.constant.CRMPortalServiceConstants;
import com.myntra.portal.crm.dao.CouponDAO;
import com.myntra.portal.crm.entity.CouponEntity;

public class CouponDAOImpl extends BaseDAOImpl<CouponEntity> implements CouponDAO {
	
	private static final Logger LOGGER = Logger.getLogger(CouponDAOImpl.class);

	@Override
	public List<CouponEntity> getCouponForLogin(String login) throws ERPServiceException {
		
		List<CouponEntity> listCoupons = new ArrayList<CouponEntity>();
		
		Query q = getEntityManager().createNamedQuery(CRMPortalServiceConstants.NAMED_QUERIES.COUPONS_VISIBLE_TO_CUSTOMER);
		q.setParameter("login", login);
		
		List<Object[]> list = q.getResultList();
		
		LOGGER.debug("Getting Coupons by Login function " + list.size());
		
		for (Object[] objArray : list) {
			int index = 0;
			String coupon = (String)objArray[index++];
			
			Double minimum = null;
			BigDecimal minimumDecimal = (BigDecimal)objArray[index++];
			if(minimumDecimal!=null) {
				minimum = Double.valueOf(minimumDecimal.doubleValue());
			}
			
			Integer times = (Integer)objArray[index++];
			String per_user = String.valueOf(objArray[index++]);
			Integer times_used = (Integer)objArray[index++];
			
			String couponType = (String)objArray[index++];

			Double mrpAmount = null;
			BigDecimal mrpAmountDecimal = (BigDecimal)objArray[index++];
			if(mrpAmountDecimal!=null) {
				mrpAmount = Double.valueOf(mrpAmountDecimal.doubleValue());
			}
			
			Double mrpPercentage = null;
			BigDecimal mrpPercentageDecimal = (BigDecimal)objArray[index++];
			if(mrpPercentageDecimal!=null) {
				mrpPercentage = Double.valueOf(mrpPercentageDecimal.doubleValue());
			}
			
			Date expire = (Date)objArray[index++];
			
			String status = String.valueOf(objArray[index++]);
			String styleid  = (String)objArray[index++];
			
			Date startdate = (Date)objArray[index++];
			
			String groupName = (String)objArray[index++];
			Boolean isInfinite = (Boolean)objArray[index++];
			Integer maxUsageByUser = (Integer)objArray[index++];
			Boolean isInfinitePerUser = (Boolean)objArray[index++];
			
			String couponStatus = (String)objArray[index++];
			Integer orderid = (Integer)objArray[index++];
			Date useddate = (Date)objArray[index++];
			
			CouponEntity entity = new CouponEntity(coupon, minimum, times, 
					per_user, times_used, couponType, mrpAmount, mrpPercentage,
					expire, status, styleid, startdate, groupName, isInfinite, 
					maxUsageByUser, isInfinitePerUser, couponStatus, orderid, useddate);
			
			listCoupons.add(entity);
			
			if(entity.getStatus().equals("U")) {
				Logger.getLogger(this.getClass()).debug("CheckCouponEntity : " + entity.toString());
			}
		}
		
		return listCoupons;
	}
}
