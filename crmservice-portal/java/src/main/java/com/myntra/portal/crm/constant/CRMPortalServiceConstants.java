package com.myntra.portal.crm.constant;

public interface CRMPortalServiceConstants {

	public interface NAMED_QUERIES {

		// Customer Profile
		public static final String CUSTOMER_PROFILE_BY_LOGIN = "CUSTOMER_PROFILE_BY_LOGIN";
		public static final String CUSTOMER_PROFILE_BY_MOBILE = "CUSTOMER_PROFILE_BY_MOBILE";

		// Coupon detail
		public static final String COUPONS_VISIBLE_TO_CUSTOMER = "COUPONS_VISIBLE_TO_CUSTOMER";

		// Customer return and comment
		public static final String CUSTOMER_RETURN_BY_LOGIN = "CUSTOMER_RETURN_BY_LOGIN";
		public static final String CUSTOMER_RETURN_BY_ORDERID = "CUSTOMER_RETURN_BY_ORDERID";
		public static final String CUSTOMER_RETURN_BY_RETURNID = "CUSTOMER_RETURN_BY_RETURNID";
		public static final String CUSTOMER_RETURN_COMMENT_BY_RETURNID = "CUSTOMER_RETURN_COMMENT_BY_RETURNID";
		
		// Customer order comment
		public static final String CUSTOMER_ORDER_COMMENT_BY_ORDERID = "CUSTOMER_ORDER_COMMENT_BY_ORDERID";		
		
		// order payment log
		public static final String ORDER_PAYMENT_LOG_BY_ORDERID = "ORDER_PAYMENT_LOG_BY_ORDERID";
		
		// order exchange details
		public static final String EXCHANGE_DETAIL_BY_ORDERID = "EXCHANGE_DETAIL_BY_ORDERID";
		
		// order cod oh reason log
		public static final String COD_ORDER_OH_LOG_BY_ORDERID = "COD_ORDER_OH_LOG_BY_ORDERID";
		
		// order RTO detail
		public static final String RTO_BY_ORDERID = "RTO_BY_ORDERID";
		
		// SR detail
		public static final String SR_BY_ORDERID = "SR_BY_ORDERID";
		public static final String SR_BY_SRID = "SR_BY_SRID";
		public static final String SR_BY_LOGIN = "SR_BY_LOGIN";
		public static final String SR_BY_MOBILE = "SR_BY_MOBILE";
	}

	public interface SQL_RESULT_SET_MAP {
		
		// Customer Profile
		public static final String MAP_CUSTOMER_PROFILE_BY_LOGIN = "MAP_CUSTOMER_PROFILE_BY_LOGIN";
		public static final String MAP_CUSTOMER_PROFILE_BY_MOBILE = "MAP_CUSTOMER_PROFILE_BY_MOBILE";

		// Coupon detail
		public static final String MAP_COUPONS_VISIBLE_TO_CUSTOMER = "MAP_COUPONS_VISIBLE_TO_CUSTOMER";

		// Customer return and comment
		public static final String MAP_CUSTOMER_RETURN = "MAP_CUSTOMER_RETURN";
		public static final String MAP_CUSTOMER_RETURN_COMMENT_BY_RETURNID = "MAP_CUSTOMER_RETURN_COMMENT_BY_RETURNID";
		
		// Customer order comment
		public static final String MAP_CUSTOMER_ORDER_COMMENT_BY_ORDERID = "MAP_CUSTOMER_ORDER_COMMENT_BY_ORDERID";
				
		// order payment log
		public static final String MAP_ORDER_PAYMENT_LOG_BY_ORDERID = "MAP_ORDER_PAYMENT_LOG_BY_ORDERID";
		
		// order exchange log
		public static final String MAP_EXCHANGE_DETAIL_BY_ORDERID = "MAP_EXCHANGE_DETAIL_BY_ORDERID";
		
		// order cod oh reason log
		public static final String MAP_COD_ORDER_OH_LOG_BY_ORDERID = "MAP_COD_ORDER_OH_LOG_BY_ORDERID";
		
		// Order RTO
		public static final String MAP_RTO_BY_ORDERID = "MAP_RTO_BY_ORDERID";
				
		// SR detail
		public static final String MAP_SR_DETAIL = "MAP_SR_DETAIL";
		
	}
}