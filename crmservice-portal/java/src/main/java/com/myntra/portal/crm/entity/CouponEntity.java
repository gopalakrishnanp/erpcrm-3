/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myntra.portal.crm.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.ColumnResult;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.SqlResultSetMappings;
import javax.persistence.Table;
import javax.persistence.Temporal;

import com.myntra.commons.entities.BaseEntity;
import com.myntra.portal.crm.constant.CRMPortalServiceConstants;

/**
 *
 * @author Pravin Mehta
 */
@Entity
@Table(name = "xcart_discount_coupons")
@NamedNativeQueries({
    @NamedNativeQuery(
        name = CRMPortalServiceConstants.NAMED_QUERIES.COUPONS_VISIBLE_TO_CUSTOMER,
        query = "SELECT " +
        		"   coupon, minimum, times, per_user, times_used, couponType, MRPAmount, MRPpercentage, " +
        		"   from_unixtime(expire) as expire, status, styleid, from_unixtime(startdate) as startdate, " +
        		"   groupName, isInfinite, maxUsageByUser, isInfinitePerUser, " +
        		"   (case(true) when status='A' and expire>unix_timestamp(now()) then 'Active' " +
       		    "             when status='A' and expire<=unix_timestamp(now()) then 'Expired' " +
        		"             when status='U' then 'Used' end) as couponStatus, " +
        		" (case true when status='U' then (select max(xo.orderid) from xcart_orders xo where xo.coupon=xdc.coupon and xo.login=:login) end) as orderid, " +
        		" (case true when status='U' then (select from_unixtime(max(xo.date)) from xcart_orders xo where xo.coupon=xdc.coupon and xo.login=:login) end) as useddate " +
       		    " from xcart_discount_coupons xdc " +
        		" where users=:login " +
       		    " and groupName!='CASHBACK' and showInMyMyntra=1 " +
       		    " order by status, expire",
        		
        resultSetMapping = CRMPortalServiceConstants.SQL_RESULT_SET_MAP.MAP_COUPONS_VISIBLE_TO_CUSTOMER
    )
})

@SqlResultSetMappings({
    @SqlResultSetMapping(name=CRMPortalServiceConstants.SQL_RESULT_SET_MAP.MAP_COUPONS_VISIBLE_TO_CUSTOMER,
        columns = {
    		@ColumnResult(name="coupon"),
            @ColumnResult(name="minimum"),
            @ColumnResult(name="times"),
            @ColumnResult(name="per_user"),
            @ColumnResult(name="times_used"),
            @ColumnResult(name="couponType"),
            @ColumnResult(name="MRPAmount"),
            @ColumnResult(name="MRPpercentage"),
            @ColumnResult(name="expire"),
            @ColumnResult(name="status"),
            @ColumnResult(name="styleid"),
            @ColumnResult(name="startdate"),
            @ColumnResult(name="groupName"),
            @ColumnResult(name="isInfinite"),
            @ColumnResult(name="maxUsageByUser"),
            @ColumnResult(name="isInfinitePerUser"),
            @ColumnResult(name="couponStatus"),
            @ColumnResult(name="orderid"),
            @ColumnResult(name="useddate")
        })
})

// @Loader(namedQuery=CRMConstants.NAMED_QUERIES.COUPONS_VISIBLE_TO_CUSTOMER)
public class CouponEntity extends BaseEntity {

    private static final long serialVersionUID = 3688398701763484215L;
    
    @Id
    @Column(name = "coupon")
    private String coupon;
    
    @Column(name = "minimum")
    private Double minimum;
    
    @Column(name = "times")
    private Integer times;
    
    @Column(name = "per_user")
    private String per_user;
    
    @Column(name = "times_used")
    private Integer times_used;
    
    @Column(name = "couponType")
    private String couponType;
    
    @Column(name = "MRPAmount")
    private Double mrpAmount;
    
    @Column(name = "MRPpercentage")
    private Double mrpPercentage;
    
    @Column(name = "expire")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date expire;
    
    @Column(name = "status")
    private String status;
    
    @Column(name = "styleid")
    private String styleid;

    @Column(name = "startdate")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date startdate;
    
    @Column(name = "groupName")
    private String groupName;
    
    @Column(name = "isInfinite")
    private Boolean isInfinite;

    @Column(name = "maxUsageByUser")
    private Integer maxUsageByUser;
    
    @Column(name = "isInfinitePerUser")
    private Boolean isInfinitePerUser;
    
    @Column(name = "couponStatus")
    private String couponStatus;
    
    @Column(name = "orderid")
    private Integer orderid;
    
    @Column(name = "useddate")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date useddate;
    
    public CouponEntity() {
    }

	public CouponEntity(String coupon, Double minimum, Integer times,
			String per_user, Integer times_used, String couponType,
			Double mrpAmount, Double mrpPercentage, Date expire, String status,
			String styleid, Date startdate, String groupName,
			Boolean isInfinite, Integer maxUsageByUser,
			Boolean isInfinitePerUser, String couponStatus, Integer orderid,
			Date useddate) {
		this.coupon = coupon;
		this.minimum = minimum;
		this.times = times;
		this.per_user = per_user;
		this.times_used = times_used;
		this.couponType = couponType;
		this.mrpAmount = mrpAmount;
		this.mrpPercentage = mrpPercentage;
		this.expire = expire;
		this.status = status;
		this.styleid = styleid;
		this.startdate = startdate;
		this.groupName = groupName;
		this.isInfinite = isInfinite;
		this.maxUsageByUser = maxUsageByUser;
		this.isInfinitePerUser = isInfinitePerUser;
		this.couponStatus = couponStatus;
		this.orderid = orderid;
		this.useddate = useddate;
	}

	public String getCoupon() {
		return coupon;
	}

	public void setCoupon(String coupon) {
		this.coupon = coupon;
	}

	public Double getMinimum() {
		return minimum;
	}

	public void setMinimum(Double minimum) {
		this.minimum = minimum;
	}

	public Integer getTimes() {
		return times;
	}

	public void setTimes(Integer times) {
		this.times = times;
	}

	public String getPer_user() {
		return per_user;
	}

	public void setPer_user(String per_user) {
		this.per_user = per_user;
	}

	public Integer getTimes_used() {
		return times_used;
	}

	public void setTimes_used(Integer times_used) {
		this.times_used = times_used;
	}

	public String getCouponType() {
		return couponType;
	}

	public void setCouponType(String couponType) {
		this.couponType = couponType;
	}

	public Double getMrpAmount() {
		return mrpAmount;
	}

	public void setMrpAmount(Double mrpAmount) {
		this.mrpAmount = mrpAmount;
	}

	public Double getMrpPercentage() {
		return mrpPercentage;
	}

	public void setMrpPercentage(Double mrpPercentage) {
		this.mrpPercentage = mrpPercentage;
	}

	public Date getExpire() {
		return expire;
	}

	public void setExpire(Date expire) {
		this.expire = expire;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getStyleid() {
		return styleid;
	}

	public void setStyleid(String styleid) {
		this.styleid = styleid;
	}

	public Date getStartdate() {
		return startdate;
	}

	public void setStartdate(Date startdate) {
		this.startdate = startdate;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public Boolean getIsInfinite() {
		return isInfinite;
	}

	public void setIsInfinite(Boolean isInfinite) {
		this.isInfinite = isInfinite;
	}

	public Integer getMaxUsageByUser() {
		return maxUsageByUser;
	}

	public void setMaxUsageByUser(Integer maxUsageByUser) {
		this.maxUsageByUser = maxUsageByUser;
	}

	public Boolean getIsInfinitePerUser() {
		return isInfinitePerUser;
	}

	public void setIsInfinitePerUser(Boolean isInfinitePerUser) {
		this.isInfinitePerUser = isInfinitePerUser;
	}

	public String getCouponStatus() {
		return couponStatus;
	}

	public void setCouponStatus(String couponStatus) {
		this.couponStatus = couponStatus;
	}

	public Integer getOrderid() {
		return orderid;
	}

	public void setOrderid(Integer orderid) {
		this.orderid = orderid;
	}

	public Date getUseddate() {
		return useddate;
	}

	public void setUseddate(Date useddate) {
		this.useddate = useddate;
	}

	@Override
	public String toString() {
		return "CouponEntity [coupon=" + coupon + ", minimum=" + minimum
				+ ", times=" + times + ", per_user=" + per_user
				+ ", times_used=" + times_used + ", couponType=" + couponType
				+ ", mrpAmount=" + mrpAmount + ", mrpPercentage="
				+ mrpPercentage + ", expire=" + expire + ", status=" + status
				+ ", styleid=" + styleid + ", startdate=" + startdate
				+ ", groupName=" + groupName + ", isInfinite=" + isInfinite
				+ ", maxUsageByUser=" + maxUsageByUser + ", isInfinitePerUser="
				+ isInfinitePerUser + ", couponStatus=" + couponStatus
				+ ", orderid=" + orderid + ", useddate=" + useddate + "]";
	}
}
