package com.myntra.portal.crm.service.impl;

import org.springframework.transaction.TransactionSystemException;

import com.myntra.commons.exception.ERPServiceException;
import com.myntra.commons.response.AbstractResponse;
import com.myntra.commons.service.impl.BaseServiceImpl;
import com.myntra.portal.crm.client.entry.OrderRtoEntry;
import com.myntra.portal.crm.client.response.OrderRtoResponse;
import com.myntra.portal.crm.manager.OrderRtoManager;
import com.myntra.portal.crm.service.OrderRtoService;

/**
 * Order RTO service interface to retrieve the detail of RTO
 * 
 * @author Arun Kumar
 */
public class OrderRtoServiceImpl extends BaseServiceImpl<OrderRtoResponse, OrderRtoEntry> implements OrderRtoService {

	@Override
	public AbstractResponse getOrderRtoDetail(Long orderId) {
		try {
			return ((OrderRtoManager) getManager()).getOrderRtoDetail(orderId);

		} catch (ERPServiceException e) {
			return e.getResponse();

		} catch (TransactionSystemException ex) {
			return ((ERPServiceException) ex.getApplicationException()).getResponse();
		}

	}
}
