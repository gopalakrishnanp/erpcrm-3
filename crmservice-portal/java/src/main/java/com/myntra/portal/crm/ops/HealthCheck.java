package com.myntra.portal.crm.ops;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.myntra.commons.exception.ERPServiceException;
import com.myntra.portal.crm.client.ProfileClient;
import com.myntra.portal.crm.client.response.CustomerProfileResponse;

/**
 * HttpServlet class for encapsulating logic for providing health check of the
 * service. The servlet needs to first check weather essential health indicators
 * of the service are running and then checks weather the application has been
 * manually pulled down. The second step is required for enabling manually
 * moving any application out of the LB
 */
public class HealthCheck extends HttpServlet {
    private static final long serialVersionUID = 1L;

    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {
    	try {
	    	CustomerProfileResponse profileResponse =
	    			ProfileClient.findByLogin("http://localhost:7092/crmservice-portal", "arun.kumar@myntra.com");
    			
    		if(profileResponse!=null && profileResponse.getStatus().getStatusType().equals("SUCCESS") 
    				&& profileResponse.getCustomerProfileEntry().getLogin().equals("arun.kumar@myntra.com")) {
    			response.getOutputStream().print("OK");
    		} else {
    			response.setStatus(HttpServletResponse.SC_NOT_FOUND);	
    		}
    	} catch (ERPServiceException e) {
    		response.setStatus(HttpServletResponse.SC_NOT_FOUND);
    	}
    }

}
