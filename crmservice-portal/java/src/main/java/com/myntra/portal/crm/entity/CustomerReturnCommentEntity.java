package com.myntra.portal.crm.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.ColumnResult;
import javax.persistence.Entity;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.SqlResultSetMappings;
import javax.persistence.Table;

import com.myntra.commons.entities.BaseEntity;
import com.myntra.portal.crm.constant.CRMPortalServiceConstants;

/**
 * Entity for return comments
 * @author Arun Kumar
 */
@Entity
@Table(name = "mk_returns_comments_log")
@NamedNativeQueries({	
@NamedNativeQuery(
	name = CRMPortalServiceConstants.NAMED_QUERIES.CUSTOMER_RETURN_COMMENT_BY_RETURNID, 
	query = "select"		
		+ " rcl.commenttitle as returnComment,rcl.addedby as returnCommentBy ,rcl.description as returnCommentDescription"
		+ " ,from_unixtime(rcl.adddate) as returnCommentCreatedDate"
		+ " ,image_url as imageURL"
		+ " from mk_returns_comments_log rcl where rcl.returnid=:returnid order by adddate desc", 
	resultSetMapping = CRMPortalServiceConstants.SQL_RESULT_SET_MAP.MAP_CUSTOMER_RETURN_COMMENT_BY_RETURNID
	)
})

@SqlResultSetMappings({
@SqlResultSetMapping(
	name = CRMPortalServiceConstants.SQL_RESULT_SET_MAP.MAP_CUSTOMER_RETURN_COMMENT_BY_RETURNID, 
	columns = {
		@ColumnResult(name = "returnComment"),
		@ColumnResult(name = "returnCommentBy"), 
		@ColumnResult(name = "returnCommentDescription"),
		@ColumnResult(name = "returnCommentCreatedDate"),
		@ColumnResult(name = "imageURL")
		}
	)
})

public class CustomerReturnCommentEntity extends BaseEntity {

	private static final long serialVersionUID = 3688398701763484215L;

	@Column(name = "returnComment")
	private String returnComment;

	@Column(name = "returnCommentBy")
	private String returnCommentBy;

	@Column(name = "returnCommentDescription")
	private String returnCommentDescription;

	//@Temporal(javax.persistence.TemporalType.DATE)
	@Column(name = "returnCommentCreatedDate")
	private Date returnCommentCreatedDate;

	@Column(name = "imageURL")
	private String imageURL;
	
	public CustomerReturnCommentEntity() {
	}
	
	public CustomerReturnCommentEntity(String returnComment, String returnCommentBy, String returnCommentDescription,
			Date returnCommentCreatedDate, String imageURL) {
		this.returnComment = returnComment;
		this.returnCommentBy = returnCommentBy;
		this.returnCommentDescription = returnCommentDescription;
		this.returnCommentCreatedDate = returnCommentCreatedDate;
		this.imageURL = imageURL;
	}

	public String getReturnComment() {
		return returnComment;
	}

	public void setReturnComment(String returnComment) {
		this.returnComment = returnComment;
	}

	public String getReturnCommentBy() {
		return returnCommentBy;
	}

	public void setReturnCommentBy(String returnCommentBy) {
		this.returnCommentBy = returnCommentBy;
	}

	public String getReturnCommentDescription() {
		return returnCommentDescription;
	}

	public void setReturnCommentDescription(String returnCommentDescription) {
		this.returnCommentDescription = returnCommentDescription;
	}

	public Date getReturnCommentCreatedDate() {
		return returnCommentCreatedDate;
	}

	public void setReturnCommentCreatedDate(Date returnCommentCreatedDate) {
		this.returnCommentCreatedDate = returnCommentCreatedDate;
	}

	public String getImageURL() {
		return imageURL;
	}

	public void setImageURL(String imageURL) {
		this.imageURL = imageURL;
	}

	@Override
	public String toString() {
		return "CustomerReturnCommentEntity [returnComment=" + returnComment + ", returnCommentBy=" + returnCommentBy
				+ ", returnCommentDescription=" + returnCommentDescription + ", returnCommentCreatedDate="
				+ returnCommentCreatedDate + ", imageURL=" + imageURL + "]";
	}

}