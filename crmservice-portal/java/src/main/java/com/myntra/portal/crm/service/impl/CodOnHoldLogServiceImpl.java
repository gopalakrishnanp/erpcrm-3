package com.myntra.portal.crm.service.impl;

import org.springframework.transaction.TransactionSystemException;

import com.myntra.commons.exception.ERPServiceException;
import com.myntra.commons.response.AbstractResponse;
import com.myntra.commons.service.impl.BaseServiceImpl;
import com.myntra.portal.crm.client.entry.CodOnHoldLogEntry;
import com.myntra.portal.crm.client.response.CodOnHoldLogResponse;
import com.myntra.portal.crm.manager.CodOnHoldLogManager;
import com.myntra.portal.crm.service.CodOnHoldLogService;

/**
 * COD Order on hold reason log service interface
 * 
 * @author Arun Kumar
 */
public class CodOnHoldLogServiceImpl extends BaseServiceImpl<CodOnHoldLogResponse, CodOnHoldLogEntry>
		implements CodOnHoldLogService {

	@Override
	public AbstractResponse getCodOnHoldLog(Long orderId) throws ERPServiceException {
		try {
			return ((CodOnHoldLogManager) getManager()).getCodOnHoldLog(orderId);

		} catch (ERPServiceException e) {
			return e.getResponse();

		} catch (TransactionSystemException ex) {
			return ((ERPServiceException) ex.getApplicationException()).getResponse();
		}
	}
}
