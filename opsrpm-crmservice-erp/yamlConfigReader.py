import getopt
import sys
import yaml
import os 
from pystache import Renderer
import pystache
import fnmatch
import datetime
import traceback

def parse_our_yaml(yamlFile, deploymentEnv, partials):
    fh = open(yamlFile, 'r')
    yamlDir = os.path.basename(yamlFile)
    renderer = pystache.Renderer()
    renderer = Renderer(partials=partials)    
    outfile = renderer.render(fh.read(), dict())
    try:
        yamldict = yaml.load(outfile)
        yamlAttributes = None
        if deploymentEnv in yamldict:
            yamlAttributes = yamldict[deploymentEnv]
        return yamlAttributes
    except:
        traceback.print_exc(file=sys.stdout)
        sys.exit(0)
        return None
    
def parse_yamldir(yamlDir, deploymentEnv):
    yamlAttributes = dict()
    partials = dict()
    for mustacheFile in os.listdir(yamlDir):
        if mustacheFile.endswith('.mustache'):
            fh = None
            try:
                fh = open(os.path.join(yamlDir,mustacheFile))
                fileNameOnly = os.path.splitext(mustacheFile)[0]
                partials[fileNameOnly] = fh.read()
            finally:
                if not fh is None:
                    fh.close()
    for yamlFile in os.listdir(yamlDir):
        if not yamlFile.endswith('.yml'):
            print "Ignoring file %s " % yamlFile
            continue
        try:
            currentAttributes = parse_our_yaml(os.path.join(yamlDir, yamlFile), deploymentEnv, partials)
            if currentAttributes is None:
                print 'Attributes None. %s environment not present in yaml file %s ' % ( deploymentEnv, yamlFile)
                continue
            commonset = set(currentAttributes.keys()).intersection(set(yamlAttributes.keys()))
            if len(commonset) > 0:
                print 'Keys already exist: %s ' % commonset
            yamlAttributes.update(currentAttributes)
        except:
            print 'Error parsing %s ' % yamlFile
            traceback.print_exc(file=sys.stdout)
            print 'Error parsing %s ' % yamlFile
    outAttributes = flatten_yaml_attributes(yamlAttributes)
    #print outAttributes
    return outAttributes 

def flatten_yaml_attributes(inAttributes):
    outAttributes = dict()
    errors= []
    for key in inAttributes:
        currentDict = outAttributes
        keys = key.split('.')
        for i in range(0, len(keys)):
            currentKey = keys[i]
            if i == len(keys) -1 :
                if currentKey in currentDict:
                    errors.append('Current Key already present in dictionary %s' % currentKey)
                else:
                    currentDict[currentKey] = inAttributes[key]
                # Leaf key
            else:
                if currentKey not in currentDict:
                    currentDict[currentKey] = dict()
                newDict = currentDict[currentKey]
                currentDict = newDict
    if len(errors) > 0:
        print errors
    return outAttributes


def process_template(templateFile, yamlAttributes):
    f = open(templateFile, 'r')
    outfile  = None
    try:
        infile = unicode(f.read())
        outfile = pystache.render(infile, yamlAttributes)
    finally:
        f.close()
    return outfile



def write_output(outputFile, outputFileContents):
    if os.path.exists(outputFile):
        now = datetime.datetime.now()
        outputFileTs = outputFile + '.'  + now.strftime("%Y%m%d_%H%M%S")
        print "File %s exists. Renamed as %s "  % (outputFile, outputFileTs)
        os.rename(outputFile, outputFileTs)
    fh  = None
    try:
        fh = open(outputFile, "w")
        fh.write(outputFileContents)
        print 'Written to %s ' % (outputFile)
    finally:
        if not fh is None:
            fh.close()


def processTemplateDirectory(templateDir, yamlAttributes, outputDir):
    for (path, dirs, files) in os.walk(templateDir):
        currentOutputDir = outputDir
        if currentOutputDir is None:
            currentOutputDir = path
        for templateFile in files:                     

            if fnmatch.fnmatch(templateFile, '*.template'):
                outputFile = process_template(os.path.join(path, templateFile), yamlAttributes)
                write_output( os.path.join(currentOutputDir, os.path.splitext(templateFile)[0]), outputFile)
    	
def process(deploymentEnv, templateDirs, yamlDir, outputDir):
    yamlAttributes = parse_yamldir(yamlDir, deploymentEnv)
    templateDirArray = templateDirs.split(',')
    for templateDir in templateDirArray:
        processTemplateDirectory(templateDir, yamlAttributes, outputDir)
    	
def parse_args(args):
    (options, remainder) = getopt.getopt(args, 'e:t:y:o:', ['env=', 
                                                         'tpldir=',
                                                         'yamldir=',
                                                         'outputdir='
                                                                   
           ])
    
    deploymentEnv = None
    templateDirs = None
    yamlDir = None
    outputDir = None
    for opt, arg in options:
        if opt in ('-e', '--env'):
            deploymentEnv = arg
        elif opt in ('-t', '--tpldir'):
            templateDirs = arg
        elif opt in ('-y', '--yamldir'):
            yamlDir = arg
        elif opt in ('-o', '--outputdir'):
            outputDir = arg

    status = True
    if templateDirs is None:
        print 'Error: Template Directory is None'
        status = False
    if yamlDir is None:
        print 'Error: Yaml Directory is None'
        status = False
    if deploymentEnv is None:
        print 'Error: Deployment environment is None'
        status = False
    
    return (status, deploymentEnv, templateDirs, yamlDir, outputDir)    

def process_parsing(args):
    (status, deploymentEnv, templateDirs, yamlDir, outputDir) = parse_args(args) 
    if status:
        process(deploymentEnv, templateDirs, yamlDir, outputDir) 

def test_dict():
    mydict = dict()
    mydict['catalina.home'] = '/opt/software/tomcat7'
    mydict['catalina.log'] = '/opt/software/tomcat7/log'
    mydict['catalina.base'] = '/opt/catalina/base'
    print mydict
    resultdict = flatten_yaml_attributes(mydict)
    print ''
    print resultdict
                   
if __name__ == "__main__":
    process_parsing(sys.argv[1:])
