/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myntra.rightnow.crm.taskwebapp.model;

import com.myntra.commons.exception.ERPServiceException;
import com.myntra.rightnow.crm.client.RightnowTaskClient;
import com.myntra.rightnow.crm.client.entry.RightnowFileAttachmentEntry;
import com.myntra.rightnow.crm.client.entry.RightnowNoteEntry;
import com.myntra.rightnow.crm.client.entry.RightnowReportColumnEntry;
import com.myntra.rightnow.crm.client.entry.RightnowReportRowEntry;
import com.myntra.rightnow.crm.client.entry.RightnowTaskEntry;
import com.myntra.rightnow.crm.client.entry.RightnowTaskIdEntry;
import com.myntra.rightnow.crm.client.response.RightnowTaskResponse;
import com.myntra.rightnow.crm.taskwebapp.utils.TaskwebappUtils;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.faces.model.ListDataModel;
import org.primefaces.model.SelectableDataModel;

/**
 *
 * @author pravin
 */
public class RightnowReportModel extends ListDataModel<RightnowReportRowEntry> implements SelectableDataModel<RightnowReportRowEntry>, Serializable {

    public List<RightnowReportColumnEntry> rightnowReportColumnEntryList;
    public List<RightnowReportRowEntry> rightnowReportRowEntryList;
    
    public HashMap<Long, List<RightnowNoteEntry>> taskNotes;
    private HashMap<Long, List<RightnowFileAttachmentEntry>> attachments;
    
    private int statusColumnIndex;

    public List<RightnowReportColumnEntry> getRightnowReportColumnEntryList() {
        return rightnowReportColumnEntryList;
    }

    public void setRightnowReportColumnEntryList(List<RightnowReportColumnEntry> rightnowReportColumnEntryList) {
        this.rightnowReportColumnEntryList = rightnowReportColumnEntryList;
        
        for(int i=0;i<rightnowReportColumnEntryList.size();i++) {
            if(rightnowReportColumnEntryList.get(i).getColumnName().equalsIgnoreCase("Task Status")) {
                setStatusColumnIndex(i);
                break;
            }
        }
    }

    public List<RightnowReportRowEntry> getRightnowReportRowEntryList() {
        return rightnowReportRowEntryList;
    }

    public void setRightnowReportRowEntryList(List<RightnowReportRowEntry> rightnowReportRowEntryList) {
        this.rightnowReportRowEntryList = rightnowReportRowEntryList;
    }

    public int getStatusColumnIndex() {
        return statusColumnIndex;
    }

    public void setStatusColumnIndex(int statusColumnIndex) {
        this.statusColumnIndex = statusColumnIndex;
    }
    
    @Override
    public Object getRowKey(RightnowReportRowEntry t) {
        return t;
    }

    @Override
    public RightnowReportRowEntry getRowData(String string) {
        final List<RightnowReportRowEntry> rows = (List<RightnowReportRowEntry>)getWrappedData();
        Long rowId = Long.valueOf(string);
        for (RightnowReportRowEntry row: rows) {
            if (row.getId().equals(rowId)) {
                return row;
            }
        }
        return null;
    }
    
    public List<RightnowNoteEntry> getTaskNotes(Long taskID) {
        if(taskNotes==null)
            return null;

        //String taskIDStr = rowEntry.getRightnowReportDataList().get(0);
        //Long taskID = Long.valueOf(taskIDStr);
        
        //return getTaskNotesFromId(taskID);
        return taskNotes.get(taskID);
    }
    
    public List<RightnowFileAttachmentEntry> getTaskAttachments(Long taskID) {
        if(attachments == null)
            return null;                
        
        return attachments.get(taskID);
    }
    
    public List<RightnowNoteEntry> getTaskDetailsFromId(Long taskID) {
        if(taskNotes==null) {
            taskNotes= new HashMap<Long, List<RightnowNoteEntry>>();
        }
        
        if(attachments == null) {
            attachments = new HashMap<Long, List<RightnowFileAttachmentEntry>>();
        }
        
        if(attachments.get(taskID) == null) {
            try {              
                RightnowTaskResponse response = RightnowTaskClient.getTaskDetails(null,taskID);
                if(TaskwebappUtils.isSuccessResponse(response)) {
                    List<RightnowTaskEntry> taskEntry = response.getRightnowTaskEntryList();
                    //taskNotes.put(taskID, taskEntry.get(0).getNoteList());
                    attachments.put(taskID, taskEntry.get(0).getFileList());
                }
            } catch(ERPServiceException e) {
                //TODO .. handle exception
            }
        }
        
        if(taskNotes.get(taskID) == null) {
            retrieveAllTaskNotesForCurrentReport();
        }
        
        return taskNotes.get(taskID);
    }
    
    public void retrieveAllTaskNotesForCurrentReport() {
        List<String> taskIds = new ArrayList<String>();
        for(RightnowReportRowEntry rowEntry : rightnowReportRowEntryList) {
            taskIds.add(rowEntry.getId().toString());
        }
        try {
            RightnowTaskResponse response = RightnowTaskClient.getRightnowTaskNotesFromReport(null, new RightnowTaskIdEntry(taskIds));
            if(TaskwebappUtils.isSuccessResponse(response)) {
                if(response.getRightnowTaskEntryList() != null && response.getRightnowTaskEntryList().size() > 0) {
                    taskNotes = new HashMap<Long, List<RightnowNoteEntry>>();
                    for(RightnowTaskEntry taskEntry : response.getRightnowTaskEntryList()) {
                        taskNotes.put(taskEntry.getId(), taskEntry.getNoteList());
                    }
                }                    
                //System.out.println("Fetching notes for all the task in the current report");
            }
        } catch(ERPServiceException e) {
            //TODO .. handle exception
        }
    }

    public void clearNotes(List<RightnowTaskEntry> taskEntries) {
        if(taskNotes != null)
            taskNotes.clear();
        taskNotes = null;
    }
    
    public void clearAttachments(List<RightnowTaskEntry> taskEntries) {
        attachments.clear();
        attachments = null;
    }
    
    public void refetchTaskDetails(Long taskId) {
        if(attachments == null) {
            attachments = new HashMap<Long, List<RightnowFileAttachmentEntry>>();
        }
        
        try {              
                RightnowTaskResponse response = RightnowTaskClient.getTaskDetails(null,taskId);
                if(TaskwebappUtils.isSuccessResponse(response)) {
                    List<RightnowTaskEntry> taskEntry = response.getRightnowTaskEntryList();
                    //taskNotes.put(taskID, taskEntry.get(0).getNoteList());
                    attachments.put(taskId, taskEntry.get(0).getFileList());
                }
        } catch(ERPServiceException e) {
            //TODO .. handle exception
        }
    }
}
