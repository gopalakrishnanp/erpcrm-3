/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myntra.rightnow.crm.taskwebapp.fileupload.exceptions;

import java.io.Serializable;

/**
 *
 * @author preetam
 */
public class FileUploadExceptionMessage implements Serializable {
    
    public static final String ERROR = "ERROR";
    public static final String INFO = "INFO";
    
    private String message;
    private String type;

    public FileUploadExceptionMessage() {
    }

    public FileUploadExceptionMessage(String message, String type) {
        this.message = message;
        this.type = type;
    }        

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
    
    
}
