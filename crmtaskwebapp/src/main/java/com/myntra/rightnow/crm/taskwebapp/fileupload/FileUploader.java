/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myntra.rightnow.crm.taskwebapp.fileupload;

import com.myntra.commons.excel.poiImpl.MyntraSheetReader;
import org.primefaces.event.FileUploadEvent;

/**
 *
 * @author preetam
 */
public interface FileUploader {
    
    public static final int MAX_NO_OF_EMPTY_ROWS_TO_STOP_PRCESSING = 3;        
    
    public void handleFileUpload(FileUploadEvent event);
    
    public void processEachExcelSheet(MyntraSheetReader sheet);
    
    public boolean validateColumnHeaders(MyntraSheetReader sheet);
    
    public void validateData(MyntraSheetReader sheet);
}
