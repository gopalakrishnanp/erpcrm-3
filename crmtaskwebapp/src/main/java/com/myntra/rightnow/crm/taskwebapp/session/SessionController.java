/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myntra.rightnow.crm.taskwebapp.session;

import java.io.IOException;
import java.io.Serializable;
//import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.enterprise.context.SessionScoped;
import org.apache.log4j.Logger;

import java.net.InetAddress;
import java.net.UnknownHostException;
import javax.faces.bean.ManagedBean;
//import java.util.logging.Logger;

/**
 *
 * @author vishal
 */
@ManagedBean(name = "sessionBean")
@SessionScoped
public class SessionController implements Serializable {
private static Logger log = Logger.getLogger(SessionController.class.getName());
    private String loggedInUser;
    private String serverDetails;
    private String sourcePage;

    public SessionController(){
        try {
            serverDetails = InetAddress.getLocalHost().getCanonicalHostName();
        } catch (UnknownHostException ex) {
            log.error(ex);
            //Logger.getLogger(SessionController.class.getName()).log(Level.SEVERE, null, ex);
            serverDetails = "";
        }
    }
    
    public String getSourcePage() {
        return sourcePage;
    }

    public void setSourcePage(String sourcePage) {
        this.sourcePage = sourcePage;
    }

    public String getLoggedInUser() {
        return loggedInUser;
    }

    public void setLoggedInUser(String loggedInUser) {
        this.loggedInUser = loggedInUser;
    }

    public String getServerDetails() {
        return serverDetails;
    }

    public void setServerDetails(String serverDetails) {
        this.serverDetails = serverDetails;
    }

    /*
     * This method sets the Get Parameters of SSO to the session.
     */
    public void setSessionVars() throws IOException {
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest req = (HttpServletRequest) context.getExternalContext().getRequest();
        HttpServletResponse res = (HttpServletResponse) context.getExternalContext().getResponse();
        
        // dont create a new session
        HttpSession session = req.getSession(true);
        
        // set all attributes to the session which can be used from the sessionScope map in facelet pages.
        session.setAttribute("loggedInUser", getLoggedInUser());
        log.info("++++++++++++++++++++++++++In Session Controller...logged in user is "+getLoggedInUser());
        session.setAttribute("serverDetails", getServerDetails());
        
        res.sendRedirect(sourcePage);
    }
    
    public void logout() throws IOException{                
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest req = (HttpServletRequest) context.getExternalContext().getRequest();
        HttpServletResponse res = (HttpServletResponse) context.getExternalContext().getResponse();
        
        HttpSession session = req.getSession(false);
        session.removeAttribute("loggedInUser");
        session.removeAttribute("userProfile");
        session.removeAttribute("token");
        
        session.invalidate();
        req.setAttribute("logout", true);
        res.sendRedirect(req.getContextPath() + "/?logout=true");
    }
}
