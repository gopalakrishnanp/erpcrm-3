package com.myntra.commons.excel.test;

import com.myntra.commons.excel.enums.CellColor;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.myntra.commons.excel.exceptions.ExcelPOIWrapperException;
import com.myntra.commons.excel.poiImpl.MyntraExcelWriter;
import com.myntra.commons.excel.poiImpl.MyntraSheetWriter;

public class WriteDate {

    public static void main(String[] args) throws IOException, ExcelPOIWrapperException {
        String file = "/home/paroksh/myntra/CMS/paroTesting/date.xlsx";
        FileOutputStream f = new FileOutputStream(file);
        MyntraExcelWriter excelWriter = new MyntraExcelWriter(f);
        writeAllTypes(excelWriter);
        //writeInMixedMode(excelWriter);
        //writeOriginalData(excelWriter);
        //excelWriter.save();
        /*MyntraSheetWriter sheetWriter = excelWriter.createSheetWriter();
         sheetWriter.writeCellComment("");
         sheetWriter.writeBoolean(true);
         sheetWriter.writeCellComment("my comment");
         String[] x = new String[]{null,null};
         sheetWriter.writeDropDown(x);
         String paroksh = sheetWriter.setNamedCell();
         sheetWriter.goToNamedCell(null);
         sheetWriter.writeText("paro");*/
         excelWriter.save();
        //f.close();
    }

    /**
     * Writes all types of data. In different mode. Protects sheets and few
     * cells. Writes comments. Named cells
     *
     * @param excelWriter
     * @throws ExcelPOIWrapperException
     */
    public static void writeAllTypes(MyntraExcelWriter excelWriter) throws ExcelPOIWrapperException {
        MyntraSheetWriter sheetWriter = excelWriter.createSheetWriter();
        //sheetWriter.setColumnMode();
        Boolean bool = true;
        String string = "paroksh";
        Date date = new Date();
        System.out.println(date);
        int numeric = 4;
        //sheetWriter.protectSheet("pas");
        //sheetWriter.setCellUnprotectedByDefault();
        String name = "myCell";
        sheetWriter.setNamedRange(name, 1, 1);
        //sheetWriter.goToNamedCell(null);
        sheetWriter.writeBoolean(bool);
        //sheetWriter.writeCellComment("this cell is manually protected");
        //sheetWriter.protectCell();
        sheetWriter.setBackgroundColor(java.awt.Color.BLUE);
        sheetWriter.writeText(string);
        //sheetWriter.writeCellComment("hi");
        sheetWriter.writeDate(date);
        //sheetWriter.setCellProtectedByDefault();
        //sheetWriter.writeCellComment("this cell is protected");
        sheetWriter.writeNumber((double) numeric);
        List<String> dropDown = new ArrayList<String>();
        dropDown.add("myname");
        dropDown.add("sup");
        dropDown.add("take me out");
        sheetWriter.goToNamedCell(name);
        sheetWriter.writeDropDown(dropDown, "nice");
    }

    public static void writeInMixedMode(MyntraExcelWriter excelWriter) throws ExcelPOIWrapperException {
        MyntraSheetWriter sheetWriter = excelWriter.createSheetWriter("mixMode");
        //sheetWriter.hideSheet();
        Boolean bool = true;
        String string = "paroksh";
        Date date = new Date();
        System.out.println(date);
        int numeric = 4;
        for (int i = 0; i < 3; i++) {
            sheetWriter.writeCellComment("changing my mode from here");
            sheetWriter.setRowMode();
            sheetWriter.writeBoolean(bool);
            sheetWriter.setColumnMode();
            sheetWriter.writeCellComment("changing mode from here");
            sheetWriter.writeText(string);
            sheetWriter.writeDate(date);
            sheetWriter.writeNumber((double) numeric);
        }
    }

    public static void writeOriginalData(MyntraExcelWriter excelWriter) throws ExcelPOIWrapperException {
        MyntraSheetWriter sheetWriter = excelWriter.createSheetWriter("Shoes");
        //writing 1st row
        sheetWriter.protectSheet("password");
        sheetWriter.writeText("StyleId");
        sheetWriter.writeText("Brand");
        sheetWriter.writeText("Enabled");
        sheetWriter.writeText("AgeGroup");
        //writing rows
        sheetWriter.setCellUnprotectedByDefault();
        for (int i = 0; i < 5; i++) {
            sheetWriter.nextRow();
            sheetWriter.protectCell();
            sheetWriter.writeNumber((double) 3000 + i);
            sheetWriter.writeText("Reebok");
            sheetWriter.writeBoolean(true);
            List<String> ageGroup = new ArrayList<String>();
            ageGroup.add("Male");
            ageGroup.add("Female");
            ageGroup.add("Unisex");
            ageGroup.add("Boy");
            ageGroup.add("Girl");
            sheetWriter.writeDropDown(ageGroup, ageGroup.get(i));
        }

    }
}
