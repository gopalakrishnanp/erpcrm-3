package com.myntra.commons.excel;

import com.myntra.commons.excel.enums.CellColor;
import com.myntra.commons.excel.exceptions.ExcelPOIWrapperException;
import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * @author paroksh
 * Every function dealing with current cell, first it checks if current row exists.If no, then create it.
 * If yes, then check if current cell exists.If not, then create it. Else use it
 */
public interface MyntraSheetWriterInterface {

    /**
     * variables to denote string "row" and "columns" to be used by
     * implementation class to check what is the current mode of reading
     */
    public static final String MODE_ROW = "row";
    public static final String MODE_COLUMN = "column";

    /**
     * This functions hides the current working sheet. Hidden sheet will not be
     * visible to user. But will be read by ExcelReader
     */
    public void hideSheet();

    /**
     * This functions un-hides a sheet.
     */
    public void unhideSheet();

    /**
     * This function protects the sheet with the given password. All rows
     * created before calling this function will also be protected(even if user
     * later sets all cells as unprotected by default by calling
     * setUnprotectedCellByDefault()). So call this function before writing a
     * row if you want to protect a sheet
     *
     * @param password password for the protected sheet
     * @throws ExcelPOIWrapperException if password is null
     */
    public void protectSheet(String password) throws ExcelPOIWrapperException;

    /**
     * Sets property(cellStyle) of a cell to be unprotected by default if sheet
     * is protected All rows created before calling this function will be
     * protected by default if sheet is protected. So call this function before
     * writing into a row else you will have no control on
     * protection/unprotection of cell created before this function call To
     * protect the cell, user needs to specifically call the function
     * protectCell()
     */
    public void setCellUnprotectedByDefault();

    /**
     * Sets property(cellStyle) of a cell to be protected by default if sheet is
     * protected To un-protect the cell, user needs to specifically call the
     * function unprotectCell()
     */
    public void setCellProtectedByDefault();

    /**
     * Protects the current cell of the sheet. This cell will be protected only
     * if sheet is protected.
     * Create row if not already present. Creates cell if not already present
     */
    public void protectCell();

    /**
     * UnProtects the current cell of the sheet.
     * Create row if not already present. Creates cell if not already present.
     */
    public void unprotectCell();

    /**
     * Moves to next row. 
     * Increase row number by 1 and sets column number as 0.
     * Doesn't create row or cell.
     *
     * @throws ExcelPOIWrapperException
     */
    
    /**
     * 
     * @param color : color of background for the current cell
     */
    public void setBackgroundColor(java.awt.Color color);
    
    /**
     * sets the sheet to be hidden.
     * But user can unhide the sheet via the user interface of excel
     */
    public void setSheetHidden();
    
    /**
     * sets the sheet to be very hidden.
     * That means user cannot unhide the sheet via user interface of excel
     */
    public void setSheetVeryHidden();
    
    public void nextRow() throws ExcelPOIWrapperException;

    /**
     * Moves to next column. 
     * Increase column number by 1 and sets row number as 0.
     * Doesn't create new cell or row
     *
     * @throws ExcelPOIWrapperException
     */
    public void nextColumn() throws ExcelPOIWrapperException;

    /**
     * Moves to next cell(depending upon row mode or column mode, it will move
     * right or downwards.
     * By default after writing a cell, it moves to next
     * cell. So this is required only when user sets writeAndMove property of
     * set-cell-value functions to false.
     *
     */
    public void nextCell();

    /**
     * Sets the reading mode of the sheet to column wise
     */
    public void setColumnMode();

    /**
     * Sets the reading mode of the sheet to row wise
     */
    public void setRowMode();

    /**
     * @return current row number
     */
    public int getRow();

    /**
     * @return current column number
     */
    public int getColumn();

    /**
     * Sets the current row to the given input
     *
     * @param rowNum row number to which we need to move
     */
    public void setRow(int rowNum);

    /**
     * Sets the current column to the given input
     *
     * @param colNum col number to which we need to move
     */
    public void setColumn(int colNum);

    /**
     * To set comment in the current cell
     *
     * @param message comment to set in the cell
     * @throws ExcelPOIWrapperException
     */
    public void writeCellComment(String message) throws ExcelPOIWrapperException;
    
    /**
     * This function assigns a range of cells to a name. This name should be unique
     * accross all MyntraSheetWriter created from a single MyntraExcelWriter
     * @param name : name of the range cells which will be used to refer to this area
     * @param horizontalLength : length in the 
     * @param verticalLength
     * @throws ExcelPOIWrapperException : if name already exists or index is out of bound
     */
    public void setNamedRange(String name,int horizontalLength,int verticalLength) throws ExcelPOIWrapperException;
    
//    /**
//     * Set the current cell to a named reference and assign that reference to
//     * string name
//     *
//     * @param name
//     */
//    public void setNamedCell(String name);

    /**
     * moves to the first cell of the namedCell region mapped to the input reference string
     * Now all operations will happen on referenced cell(previous cell location is lost)
     *
     * @param reference reference string (basically reference to the cell)
     * @throws ExcelPOIWrapperException
     */
    public void goToNamedCell(String reference) throws ExcelPOIWrapperException;
    
    /**
     * Get the default column width for the Sheet in terms of number of characters
     * @return 
     */
    public int getDefaultColumnWidth();
    
    /**
     * Set default column width for the sheet in terms of number of characters
     * @param width 
     */
    public void setDefaultColumnWidth(int width);
    
    /**
     * Get the column width in terms of number of characters for current sheet
     * @return 
     */
    public int getCurrentColumnWidth();
    /**
     * Sets the width of the current cell. It actually sets for the whole column of the current cell.
     * So if you call this function multiple times (even in different cells) in same row, final value will
     * be used to set width of the column
     * width should be typically given in terms of number of characters. Internally it gets multiplied by 255
     * @param width 
     */
    public void setCurrentColumnWidth(int width);
    
    
    
    
    /**
     * This makes the type of the current cell as CELL_BLANK. It is used when user is not sure about the 
     * type of value to be filled in the cell and for now, create it as blank cell
     * @param writeAndMoveParams variable argument(varargs : present in java
     * >1.5). Only the 1st argument is read and it denotes if cursor moves to
     * next cell after writing. If no arguments are provided by default it moves
     * to next cell after writing
     */
    public void writeBlank(Boolean... writeAndMoveParams) throws ExcelPOIWrapperException ;
    
    
    /**
     * Sets the value of the current cell to input double
     *
     * @param value value to set for the current cell
     * @param writeAndMoveParams variable argument(varargs : present in java
     * >1.5). Only the 1st argument is read and it denotes if cursor moves to
     * next cell after writing. If no arguments are provided by default it moves
     * to next cell after writing
     * @throws ExcelPOIWrapperException
     */
    public void writeNumber(Double value, Boolean... writeAndMoveParams) throws ExcelPOIWrapperException;

    /**
     * Sets the value of the current cell to input String
     *
     * @param value value to set for the current cell
     * @param writeAndMoveParams variable argument(varargs : present in java
     * >1.5). Only the 1st argument is read and it denotes if cursor moves to
     * next cell after writing. If no arguments are provided by default it moves
     * to next cell after writing
     * @throws ExcelPOIWrapperException
     */
    
    public void writeText(String value, Boolean... writeAndMoveParams) throws ExcelPOIWrapperException;

    /**
     * Sets the value of the current cell to input Bool
     *
     * @param value value to set for the current cell
     * @param writeAndMoveParams variable argument(varargs : present in java
     * >1.5). Only the 1st argument is read and it denotes if cursor moves to
     * next cell after writing. If no arguments are provided by default it moves
     * to next cell after writing
     * @throws ExcelPOIWrapperException
     */
    public void writeBoolean(Boolean value, Boolean... writeAndMoveParams) throws ExcelPOIWrapperException;

    /**
     * Sets the value of the current cell to input Date
     *
     * @param value value to set for the current cell
     * @param writeAndMoveParams variable argument(varargs : present in java
     * >1.5). Only the 1st argument is read and it denotes if cursor moves to
     * next cell after writing. If no arguments are provided by default it moves
     * to next cell after writing
     * @throws ExcelPOIWrapperException
     */
    public void writeDate(Date date, Boolean... writeAndMoveParams) throws ExcelPOIWrapperException;

    /**
     * Creates a drop down at the current cell
     *
     * @param value array of string which will appear in the drop down
     * @param writeAndMoveParams variable argument(varargs : present in java
     * >1.5). Only the 1st argument is read and it denotes if cursor moves to
     * next cell after writing. If no arguments are provided by default it moves
     * to next cell after writing
     * @throws ExcelPOIWrapperException
     */
    //public void writeDropDown(String[] values, String defaultValue, Boolean... writeAndMoveParams) throws ExcelPOIWrapperException;
    
    /**
     * 
     * @param value 
     * @param defaultValue 
     * @param rowFirst first row of the specified range of cells
     * @param rowLast last row of the specified range of cells
     * @param colFirst first column of the specified range of cells
     * @param colLast  last column of the specified range of cells
     * @param writeAndMoveParams 
     */
    /**
     * Creates a drop down at all the cells specified in the range.
     * @param values List of string which will appear in the drop down
     * @param defaultValue default text to be displayed in the cell
     * @param horizontalLength length of the rectangle in the horizontal direction which will have all its cells containing drop down
     * For drop down only in a column, pass horizontal length as 1
     * @param verticalLength length of the rectangle in the vertical direction which will have all its cells containing drop down
     * For drop down only in a row, pass vertical length as 1
     * @param writeAndMoveParams variable argument(varargs : present in java
     * >1.5). Only the 1st argument is read and it denotes if cursor moves to
     * next cell after writing. If no arguments are provided by default it moves
     * to next cell after writing. First it reaches the end of the rectangle by moving horizontal length forward (if horizontal length is
     * -ve, it will move backword) and vertical length downward (if vertical length is -ve, it will move upward).Now depending
     * upon row mode or column mode, it will move forward or downwards
     * 
     * @throws ExcelPOIWrapperException 
     */
    //public void writeDropDownCells(Collection<String> values, String defaultValue,int horizontalLength,int verticalLength,Boolean... writeAndMoveParams) throws ExcelPOIWrapperException;

    /**
     * Write Header Cell with header formatting, and with given column width.
     * If column width is passed as null and value contains more characters than in defaultColumnWidth, 
     * then the column width is set to be equal to the number of characters in the value
     * @param value
     * @param columnWidth
     * @param writeAndMoveParams
     * @throws ExcelPOIWrapperException 
     */
    public void writeHeader(String value, Integer columnWidth, Boolean highlightCell, Boolean... writeAndMoveParams) throws ExcelPOIWrapperException;
    
    /**
     * This writes the drop down values using the list provided. Reuse of this
     * list will not happen. If user wants to reuse this string, use 
     * {@link  #writeNamedDropDown(String namedCells,List<T> values, String defaultValue, Boolean... writeAndMoveParams) writeNamedDropDown} function
     * @param values : list of values which will appear in the drop down
     * @param defaultValue default text to be displayed in the cell
     * @param writeAndMoveParams variable argument(varargs : present in java
     * >1.5). Only the 1st argument is read and it denotes if cursor moves to
     * next cell after writing. If no arguments are provided by default it moves
     * to next cell after writing
     * @throws ExcelPOIWrapperException 
     */
    public void writeDropDown(Collection<String> values, String defaultValue, Boolean... writeAndMoveParams) throws ExcelPOIWrapperException;

    /**
     * This function creates a drop down at the current cell. 
     * Values to be filled in drop down are fetched from named cells reffered by string namedCells.
     * 
     * @param namedCells : name of the range of cells containing values
     * @param defaultValue : default value in the drop down
     * @param writeAndMoveParams : variable argument(varargs : present in java
     * >1.5). Only the 1st argument is read and it denotes if cursor moves to
     * next cell after writing. If no arguments are provided by default it moves
     * to next cell after writing
     * @throws ExcelPOIWrapperException : if cells range with given name does not exists, it gives error
     */
    public <T> void writeNamedDropDown(String namedCells,Collection<T> values, String defaultValue, Boolean... writeAndMoveParams) throws ExcelPOIWrapperException;

    /**
     * Create Freeze Pane
     * @param colSplit
     * @param rowSplit
     * @param leftmostColumn
     * @param topRow 
     * @see {@link HSSFSheet.createFreezePane}
     */
    public void createFreezePane(int colSplit, int rowSplit, int leftmostColumn, int topRow);
    
    /**
     * Create Freeze Pane
     * @param colSplit
     * @param rowSplit 
     * @see {@link HSSFSheet.createFreezePane}
     */
    public void createFreezePane(int colSplit, int rowSplit);
}
