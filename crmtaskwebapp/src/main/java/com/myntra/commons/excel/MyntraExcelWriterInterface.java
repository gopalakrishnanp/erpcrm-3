package com.myntra.commons.excel;

import com.myntra.commons.excel.exceptions.ExcelPOIWrapperException;
import com.myntra.commons.excel.poiImpl.MyntraSheetWriter;
import java.io.IOException;
import java.util.Collection;
import org.apache.poi.ss.usermodel.Name;

public interface MyntraExcelWriterInterface {

    /**
     * @author paroksh creates an object of class MyntraSheetWriter creates a
     * sheet in the workbook object present in the class MyntraExcelWriter
     * Assign this sheet to MyntraSheetWriter object
     * @param name Name of the sheet created
     * @return object of class MyntraSheetWriter
     * @throws ExcelPOIWrapperException
     */
    public MyntraSheetWriter createSheetWriter(String name) throws ExcelPOIWrapperException;

    /**
     * @author paroksh creates an object of class MyntraSheetWriter creates a
     * sheet in the workbook object present in the class MyntraExcelWriter
     * Assign this sheet to MyntraSheetWriter object
     * @return object of class MyntraSheetWriter
     * @throws ExcelPOIWrapperException
     */
    public MyntraSheetWriter createSheetWriter() throws ExcelPOIWrapperException;

    /**
     * This function is to write the content which will be referred later as named cells 
     * into a hidden sheet. It is mainly called from writeNamedDropDown function from MyntraSheetWriter object
     * to write the given list in a hidden sheet, assign it the given name and store it in a map so that this
     * name can be used later to fetch the namedCells containing these values.
     * It checks if hidden sheet exists. If not, then it creates a hidden sheet and assign it to its variable
     * hiddenSheetWriter and makes this sheet as hidden. Now it writes in a column, all these values and assign
     * those cells a name and map that namedCells to name provided by the user. It moves to the new column, to write
     * in new column in case this function is called again to write some other lists.
     * @param <T> : Type of the content of the list.
     * @param name : name to be associated with namedCells
     * @param values : list which needs to be written in the namedCells
     * @throws ExcelPOIWrapperException 
     */
    public <T> void writeDropDownContent(String name, Collection<T> values) throws ExcelPOIWrapperException;
    
    /**
     * This creates a map entry with key as string name and value as Name namedCell.
     * Now this name can be used anywhere in the sheets created by this excel writer
     * to refer to these namedCells
     * @param name : String name 
     * @param namedCell : Name object
     * @throws ExcelPOIWrapperException : if this name is already assigned to some other
     * named cells, an exception is thrown
     */
    public void setNamedCell(String name, Name namedCell) throws ExcelPOIWrapperException;
    
    /**
     * This fetches the namedCells corresponding to this String name.
     * If no such entry exists, it will return a null value.
     * @param name
     * @return Name object mapped with this string name. Null if no such Name object exists
     */
    public Name getNamedCell(String name);
    
    /**
     * @author paroksh writes all the sheets present in the workbook to the
     * output file
     * @throws IOException
     */
    public void save() throws IOException;
}
