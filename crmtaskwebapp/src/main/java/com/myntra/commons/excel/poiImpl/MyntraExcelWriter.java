package com.myntra.commons.excel.poiImpl;

import com.myntra.commons.excel.MyntraExcelWriterInterface;
import com.myntra.commons.excel.exceptions.ExcelPOIWrapperException;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.apache.log4j.Logger;
import org.apache.commons.collections.CollectionUtils;
import org.apache.poi.ss.usermodel.Name;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 * @author paroksh Class that will help user to write in an excel sheet It can
 * create sheets and write sheets to file specified by user
 */
public class MyntraExcelWriter implements MyntraExcelWriterInterface {

    /**
     * File output stream to which excel data will be written
     */
    private FileOutputStream file;
    /**
     * workbook which is used to create excel sheets and save data in excel
     * file.
     */
    private Workbook workbook;
    /**
     * To log data.
     */
    private static Logger log = Logger.getLogger(MyntraExcelWriter.class.getName());
    /**
     * A Myntra sheet writer object which is used to write hidden data. This is
     * primarily used for writing drop down data and creating named cells out of
     * it which will be used by other sheet writers to write drop down using
     * these named cells instead of themselves passing data list of drop down
     * every time. This considerable reduces excel file size in which many drop
     * downs are reffering to same data content.
     */
    private MyntraSheetWriter hiddenSheetWriter = null;
    /**
     * to store the mapping of named string to its reference.
     */
    private Map<String, Name> namedCellMap;

    Workbook getWorkbook(){
        return workbook;
    }
    
    /**
     * Constructor
     *
     * @param file output file to which sheets created by this class should be
     * written
     */
    public MyntraExcelWriter(FileOutputStream file) throws ExcelPOIWrapperException {
        if (file == null) {
            throw new ExcelPOIWrapperException("Output file stream is null");
        }
        this.file = file;
        workbook = new XSSFWorkbook();
        namedCellMap = new HashMap<String, Name>();
    }

    /**
     * Constructor. If readFile in null or throws some exception, create a new
     * workbook instead of creating from already existing file
     *
     * @param file output file to which sheets created by this class should be
     * written
     * @param readFile input file which should be read in the workbook. Over
     * this new sheets will be added
     */
    public MyntraExcelWriter(FileOutputStream file, FileInputStream readFile) throws ExcelPOIWrapperException {
        if (file == null) {
            throw new ExcelPOIWrapperException("Output file stream is null");
        }
        this.file = file;
        try {
            if (readFile == null) {
                workbook = new XSSFWorkbook();
            } else {
                workbook = new XSSFWorkbook(readFile);
            }

        } catch (IOException e) {
            workbook = new XSSFWorkbook();
        }
        namedCellMap = new HashMap<String, Name>();
    }

    @Override
    public MyntraSheetWriter createSheetWriter(String name) throws ExcelPOIWrapperException {
        if (name == null) {
            return createSheetWriter();
        } else {
            MyntraSheetWriter myntraSheetWriter = new MyntraSheetWriter(name, workbook, this);
            return myntraSheetWriter;
        }
    }

    @Override
    public MyntraSheetWriter createSheetWriter() throws ExcelPOIWrapperException {
        MyntraSheetWriter myntraSheetWriter = new MyntraSheetWriter(workbook, this);
        return myntraSheetWriter;
    }

    @Override
    public <T> void writeDropDownContent(String name, Collection<T> values) throws ExcelPOIWrapperException {
        if (hiddenSheetWriter == null) {
            hiddenSheetWriter = this.createSheetWriter("__hidden__");
            hiddenSheetWriter.setSheetHidden();
            hiddenSheetWriter.setColumnMode();
        }
        if (CollectionUtils.isNotEmpty(values)) {
            int len = values.size();
            Iterator<T> tempItr = values.iterator();
            //if(itr.hasNext()){
            T inst = tempItr.next();
            //}
            //T inst = values.get(0);

            //Depending upon type of values present in the list, call appropriate write functions
            if (inst instanceof String) {
                Iterator<T> iter = values.iterator();
                String value = null;
                while (iter.hasNext()) {
                    value = iter.next().toString();
                    if (!iter.hasNext()) {
                        break; //if currently writing last entry, skipping while loop and write outside
                    }
                    hiddenSheetWriter.writeText(value);
                }
                hiddenSheetWriter.writeText(value, false);
//                for(int i=0;i<len-1;i++){
//                    hiddenSheetWriter.writeText(values.get(i).toString());
//                }
                //Don't move to next cell after writing last cell
                //hiddenSheetWriter.writeText(values.get(len-1).toString(),false);
            } else if (inst instanceof Double) {
                Iterator<T> iter = values.iterator();
                String value=null;
                while (iter.hasNext()) {
                    value = iter.next().toString();
                    if (!iter.hasNext()) {
                        break; //if currently writing last entry, skipping while loop and write outside
                    }
                    hiddenSheetWriter.writeNumber(Double.valueOf(value));
                }
                hiddenSheetWriter.writeNumber(Double.valueOf(value), false);

//                for(int i=0;i<len-1;i++){
//                    hiddenSheetWriter.writeNumber(Double.valueOf(values.get(i).toString()));// . values.get(i));
//                }
//                //Don't move to next cell after writing last cell
//                hiddenSheetWriter.writeNumber(Double.valueOf(values.get(len -1).toString()),false);
            } else if (inst instanceof Boolean) {
                Iterator<T> iter = values.iterator();
                String value=null;
                while (iter.hasNext()) {
                    value = iter.next().toString();
                    if (!iter.hasNext()) {
                        break; //if currently writing last entry, skipping while loop and write outside
                    }
                    hiddenSheetWriter.writeBoolean(Boolean.getBoolean(value));
                }
                hiddenSheetWriter.writeBoolean(Boolean.getBoolean(value), false);


//                for(int i=0;i<len-1;i++){
//                    hiddenSheetWriter.writeBoolean(Boolean.getBoolean(values.get(i).toString()));
//                }
//                //Don't move to next cell after writing last cell
//                hiddenSheetWriter.writeBoolean(Boolean.getBoolean(values.get(len-1).toString()),false);
            } else if (inst instanceof Date) {
                Iterator<T> iter = values.iterator();
                String value = null;
                while (iter.hasNext()) {
                    value = iter.next().toString();
                    if (!iter.hasNext()) {
                        break; //if currently writing last entry, skipping while loop and write outside
                    }
                    try {
                        DateFormat dateFormat = new SimpleDateFormat("dd-MM-YYY");
                        hiddenSheetWriter.writeDate(dateFormat.parse(value));
                    } catch (ParseException ex) {
                        log.debug(ex);
                    }
                }
                    //Don't move to next cell after writing last cell
                    DateFormat dateFormat = new SimpleDateFormat("dd-MM-YYY");
                    try {
                        hiddenSheetWriter.writeDate(dateFormat.parse(value), false);
                    } catch (ParseException ex) {
                        log.debug(ex);
                    }
//                for(int i=0;i<len-1;i++){
//                    try {
//                        DateFormat dateFormat = new SimpleDateFormat("dd-MM-YYY");
//                        hiddenSheetWriter.writeDate(dateFormat.parse(values.get(i).toString()));
//                    } catch (ParseException ex) {
//                        log.debug(ex);
//                    }
//                }
//                //Don't move to next cell after writing last cell
//                DateFormat dateFormat = new SimpleDateFormat("dd-MM-YYY");
//                try {
//                    hiddenSheetWriter.writeDate(dateFormat.parse(values.get(len -1).toString()),false);
//                } catch (ParseException ex) {
//                    log.debug(ex);
                }

            
            hiddenSheetWriter.setNamedRange(name, 1, -len);
            hiddenSheetWriter.nextColumn();
        }

    }

    @Override
    public void setNamedCell(String name, Name namedCell) throws ExcelPOIWrapperException {
        Name ref = namedCellMap.get(name);
        if (ref != null) {
            throw new ExcelPOIWrapperException("Name \"" + name + "\" is already used to name a cell/range of cells");
        }
        namedCellMap.put(name, namedCell);
    }

    @Override
    public Name getNamedCell(String name) {
        Name ref = namedCellMap.get(name);
        return ref;
    }

    @Override
    public void save() throws IOException {
        workbook.write(file);
    }
}
