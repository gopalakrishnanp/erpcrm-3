<?php
$url="http://prism.mynt.myntra.com";

$start = microtime(true);

$ch = curl_init();
curl_setopt($ch, CURLOPT_HEADER, 0);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible;)");
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_HTTPGET, true);
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);
curl_setopt($ch, CURLOPT_TIMEOUT, 30);

/*
$authheaders = 'Authorization: Basic '.base64_encode("Heartbeat User"."~"."dummy_heartbeat". ':' ."dummy_heartbeat_pass");
$accept = 'Accept: application/json';
$contentType = 'Content-Type: application/json';
curl_setopt($ch, CURLOPT_HTTPHEADER, array("$authheaders","$accept","$contentType"));
*/
$response = curl_exec($ch);
$info = curl_getinfo($ch);

$end = microtime(true);
$time = number_format(($end - $start), 2);
echo 'Prism Loaded in ', $time, ' seconds';

print_r($response);
print_r($info);

curl_close($ch);
?>

