package com.myntra.erp.crm.manager;

import com.myntra.commons.exception.ERPServiceException;
import com.myntra.commons.manager.BaseManager;
import com.myntra.erp.crm.client.entry.CustomerOrderSummaryEntry;
import com.myntra.erp.crm.client.response.CustomerOrderSummaryResponse;

/**
 * @author Pravin Mehta
 */
public interface CustomerOrderSummaryManager extends BaseManager<CustomerOrderSummaryResponse, CustomerOrderSummaryEntry> {

	public CustomerOrderSummaryResponse getOrderSummary(String login) throws ERPServiceException;
}