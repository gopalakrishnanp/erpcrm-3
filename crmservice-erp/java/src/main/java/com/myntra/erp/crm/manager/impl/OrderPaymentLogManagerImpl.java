/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myntra.erp.crm.manager.impl;

import com.myntra.commons.exception.ERPServiceException;
import com.myntra.commons.manager.impl.BaseManagerImpl;
import com.myntra.commons.response.AbstractResponse;
import com.myntra.erp.crm.manager.OrderPaymentLogManager;
import com.myntra.portal.crm.client.OrderPortalClient;
import com.myntra.portal.crm.client.entry.OrderPaymentLogEntry;
import com.myntra.portal.crm.client.response.OrderPaymentLogResponse;

/**
 * @author Pravin Mehta
 */
public class OrderPaymentLogManagerImpl extends BaseManagerImpl<OrderPaymentLogResponse, OrderPaymentLogEntry> implements OrderPaymentLogManager {

	
	@Override
	public OrderPaymentLogResponse getOrderPaymentLog(Long orderId) throws ERPServiceException {
		// Call the portal client and fetch payment data
		return OrderPortalClient.getPaymentLog(null, orderId);
	}
	
	@Override
	public AbstractResponse findById(Long id) throws ERPServiceException {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	@Override
	public AbstractResponse create(OrderPaymentLogEntry entry) throws ERPServiceException {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	@Override
	public AbstractResponse update(OrderPaymentLogEntry entry, Long itemId) throws ERPServiceException {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	@Override
	public AbstractResponse search(int start, int fetchSize, String sortBy, String sortOrder, String searchTerms)
			throws ERPServiceException {
		throw new UnsupportedOperationException("Not supported yet.");
	}
}
