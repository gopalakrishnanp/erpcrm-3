package com.myntra.erp.crm.service.impl;

import org.springframework.transaction.TransactionSystemException;

import com.myntra.commons.exception.ERPServiceException;
import com.myntra.commons.response.AbstractResponse;
import com.myntra.commons.service.impl.BaseServiceImpl;
import com.myntra.erp.crm.client.entry.CustomerOrderSummaryEntry;
import com.myntra.erp.crm.client.response.CustomerOrderSummaryResponse;
import com.myntra.erp.crm.manager.CustomerOrderSummaryManager;
import com.myntra.erp.crm.service.CustomerOrderSummaryService;

public class CustomerOrderSummaryServiceImpl extends BaseServiceImpl<CustomerOrderSummaryResponse, CustomerOrderSummaryEntry> implements CustomerOrderSummaryService {

    @Override
    public AbstractResponse getOrderSummary(String login) throws ERPServiceException {
        try {
            return ((CustomerOrderSummaryManager) getManager()).getOrderSummary(login);
        } catch (ERPServiceException e) {
            return e.getResponse();
        } catch (TransactionSystemException ex) {
            // Spring encapsulates Application Exception into TransactionSystemException
            return ((ERPServiceException) ex.getApplicationException()).getResponse();
        }
    }
}
