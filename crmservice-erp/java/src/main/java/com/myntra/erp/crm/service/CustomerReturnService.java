package com.myntra.erp.crm.service;

import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import com.myntra.commons.exception.ERPServiceException;
import com.myntra.commons.response.AbstractResponse;
import com.myntra.commons.service.BaseService;
import com.myntra.erp.crm.client.entry.CustomerReturnEntry;
import com.myntra.erp.crm.client.response.CustomerReturnResponse;

/**
 * Customer return web service interface(abstract) which integrates detail
 * of return comments, skus, tracking, trip detail etc..
 * 
 * @author Arun Kumar
 */
@Path("/return/")
public interface CustomerReturnService extends BaseService<CustomerReturnResponse, CustomerReturnEntry> {

	@GET
	@Produces({ "application/xml", "application/json" })
	@Consumes({ "application/xml", "application/json" })
	AbstractResponse getReturnDetail(@QueryParam("returnId") Long returnId, @QueryParam("orderId") Long orderId,
			@QueryParam("login") String login,
			@DefaultValue("true") @QueryParam("isLogisticDetailNeeded") boolean isLogisticDetailNeeded) throws ERPServiceException;
}
