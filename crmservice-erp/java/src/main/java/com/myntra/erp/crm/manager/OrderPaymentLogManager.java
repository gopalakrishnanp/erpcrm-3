package com.myntra.erp.crm.manager;

import com.myntra.commons.exception.ERPServiceException;
import com.myntra.commons.manager.BaseManager;
import com.myntra.portal.crm.client.entry.OrderPaymentLogEntry;
import com.myntra.portal.crm.client.response.OrderPaymentLogResponse;

/**
 * Manager interface(abstract) for payment logs.
 * 
 * @author Pravin Mehta
 */
public interface OrderPaymentLogManager extends BaseManager<OrderPaymentLogResponse, OrderPaymentLogEntry> {
	public OrderPaymentLogResponse getOrderPaymentLog(Long orderID) throws ERPServiceException;
}