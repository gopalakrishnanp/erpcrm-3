$(document).ready(function() {

    // This is to enable opening panel on clicking header
    $('.ui-widget').on('mouseup', 'div.ui-panel-titlebar', function(event) {
        event.stopPropagation();
        // added both class to make it compatible for FF and chrome
        // dont toggle on click of refresh icon on panel header bar
        if (!$(event.target).hasClass("ui-icon-refresh") && !$(event.target).hasClass("ui-button")) {
            if (!$(event.target).is("a.ui-panel-titlebar-icon > span")) {
                if (!$(event.target).is('a.ui-panel-titlebar-icon')) {
                    $(this).find('a .ui-icon-minusthick, a .ui-icon-plusthick').trigger('click');
                }
            }
        }

    });

    $('.ui-widget-content').on('click', 'tr', function(event) {
        if ($(event.target).is(".ui-row-toggler")) {
            return;
        }
        var obj = $(this).find('td:eq(0) div');

        if (obj.hasClass('ui-row-toggler')) {
            obj.trigger("click");
            event.stopPropagation();
        }
    });
    
    var i = $('#mainForm\\:orderHistoryDataTable .ui-row-toggler.ui-icon-circle-triangle-e').length;
    if(i == 1){
        $('#mainForm\\:orderHistoryDataTable .ui-row-toggler.ui-icon-circle-triangle-e').trigger('click');
    }
    
    var j = $('#mainForm\\:return_data_table .ui-row-toggler.ui-icon-circle-triangle-e').length;
    if(j == 1){
        $('#mainForm\\:return_data_table .ui-row-toggler.ui-icon-circle-triangle-e').trigger('click');
    }
    
    //var hashurl = $('#mainForm\\:searchterm').val();
    //if(typeof hashurl === "undefined") {
    //} else {
      //  location.hash=hashurl;
    //}
    
});
