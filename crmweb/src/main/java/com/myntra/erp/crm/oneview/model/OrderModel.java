package com.myntra.erp.crm.oneview.model;

import com.myntra.commons.exception.ERPServiceException;
import com.myntra.erp.crm.client.CustomerOrderClient;
import com.myntra.erp.crm.client.CustomerReturnClient;
import com.myntra.erp.crm.client.TripDeliveryStaffClient;
import com.myntra.erp.crm.client.entry.CustomerOrderEntry;
import com.myntra.erp.crm.client.entry.CustomerOrderItemEntry;
import com.myntra.erp.crm.client.entry.CustomerOrderShipmentEntry;
import com.myntra.erp.crm.client.entry.CustomerOrderTripAssignmentEntry;
import com.myntra.erp.crm.client.entry.CustomerReturnEntry;
import com.myntra.erp.crm.client.entry.TripDeliveryStaffEntry;
import com.myntra.erp.crm.client.response.CustomerOrderResponse;
import com.myntra.erp.crm.client.response.CustomerReturnResponse;
import com.myntra.erp.crm.client.response.TripDeliveryStaffResponse;
import com.myntra.portal.crm.client.OrderPortalClient;
import com.myntra.portal.crm.client.entry.CodOnHoldLogEntry;
import com.myntra.portal.crm.client.entry.CustomerOrderCommentEntry;
import com.myntra.portal.crm.client.response.CodOnHoldLogResponse;
import com.myntra.portal.crm.client.response.CustomerOrderCommentResponse;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author pravin
 */
public class OrderModel implements Serializable {

    private List<CustomerOrderEntry> customerOrderEntryList;
    private List<Long> shipmentIdList;
    private HashMap<Long, List<CustomerReturnEntry>> itemToReturnEntryMap;
    private HashMap<Long, List<CustomerOrderCommentEntry>> customerOrderCommentMap;
    private HashMap<Long, List<CodOnHoldLogEntry>> orderOnHoldLogsMap;
    private HashMap<Long, TripDeliveryStaffEntry> shipmentToStaffEntryMap;
    private HashMap<Long, Long> markForProcessMap;

    public OrderModel() {
    }

    public List<CustomerOrderEntry> getCustomerOrderEntryList() {
        return customerOrderEntryList;
    }

    public void setCustomerOrderEntryList(List<CustomerOrderEntry> customerOrderEntryList) {
        this.customerOrderEntryList = customerOrderEntryList;
        doOrderEnhancements();
    }

    public List<Long> getShipmentIdList() {
        return shipmentIdList;
    }

    public List<CustomerReturnEntry> getReturn(Long itemId) {
        return itemToReturnEntryMap.get(itemId);        
    }

    public boolean isReturned(Long itemId) {
        if(itemToReturnEntryMap.get(itemId) != null){
            return (itemToReturnEntryMap.get(itemId).size()>0);
        }
        return false;        
    }
    
    public int getTotalReturnForItem(Long itemId){
        if(itemToReturnEntryMap.get(itemId) != null){
            List<CustomerReturnEntry> returnEntryList = itemToReturnEntryMap.get(itemId);
            Integer totalQty = 0;
            for(CustomerReturnEntry returnEntry : returnEntryList){
                totalQty = totalQty + returnEntry.getQuantity();
            }
            return totalQty;
        }
        return 0;
    }
    
    public TripDeliveryStaffEntry getStaffDetail(Long shipmentId) {
        return shipmentToStaffEntryMap.get(shipmentId);
    }

    private void doOrderEnhancements() {
        addExchangeOrders();
        checkZeroPay();
        fixOnHoldStatus();
        createShipmentIdList();
        getReturnIDForShipmentItem();
        getDeliveryStaffDetail();
    }

    public List<CustomerOrderShipmentEntry> getShipmentEntryList(CustomerOrderEntry orderEntry) {
        return orderEntry.getOrderShipments();
    }

    public HashMap<Long, List<CustomerOrderCommentEntry>> getCustomerOrderCommentMap() {
        return customerOrderCommentMap;
    }

    public void setCustomerOrderCommentMap(HashMap<Long, List<CustomerOrderCommentEntry>> customerOrderCommentMap) {
        this.customerOrderCommentMap = customerOrderCommentMap;
    }

    public HashMap<Long, List<CodOnHoldLogEntry>> getOrderOnHoldLogsMap() {
        return orderOnHoldLogsMap;
    }

    public void setOrderOnHoldLogsMap(HashMap<Long, List<CodOnHoldLogEntry>> customerOrderOnHoldCommentMap) {
        this.orderOnHoldLogsMap = customerOrderOnHoldCommentMap;
    }

    public void getShipmentComments(Long shipmentID) {
        if (customerOrderCommentMap == null) {
            customerOrderCommentMap = new HashMap<Long, List<CustomerOrderCommentEntry>>();
        }

        try {
            CustomerOrderCommentResponse response = OrderPortalClient.getCommentLog(null, shipmentID);
            if (response != null && response.getStatus().getStatusType().equals("SUCCESS")) {
                customerOrderCommentMap.put(shipmentID, response.getCustomerOrderCommentEntryList());
                return;
            }
        } catch (ERPServiceException e) {
        }
        customerOrderCommentMap.put(shipmentID, null);
    }

    public void getShipmentOnHoldComments(Long shipmentID) {
        if (orderOnHoldLogsMap == null) {
            orderOnHoldLogsMap = new HashMap<Long, List<CodOnHoldLogEntry>>();
        }

        try {
            CodOnHoldLogResponse response = OrderPortalClient.getCodOnHoldLog(null, shipmentID);
            if (response != null && response.getStatus().getStatusType().equals("SUCCESS") && response.getCodOnHoldLogEntryList().size() > 0) {
                orderOnHoldLogsMap.put(shipmentID, response.getCodOnHoldLogEntryList());
                return;
            }
        } catch (ERPServiceException e) {
        }

        orderOnHoldLogsMap.put(shipmentID, null);

    }

    private void addExchangeOrders() {
        if (customerOrderEntryList == null) {
            return;
        }

        List<Long> exchangeIDList = new ArrayList<Long>();
        List<Long> existingOrderIDs = new ArrayList<Long>();

        List<CustomerOrderEntry> exchangeOrderEntryList = new ArrayList<CustomerOrderEntry>();

        for (CustomerOrderEntry customerOrderEntry : customerOrderEntryList) {
            List<CustomerOrderShipmentEntry> shipmentEntryList = customerOrderEntry.getOrderShipments();
            for (CustomerOrderShipmentEntry shipmentEntry : shipmentEntryList) {
                existingOrderIDs.add(shipmentEntry.getShipmentId());
            }
        }

        for (CustomerOrderEntry customerOrderEntry : customerOrderEntryList) {
            if (customerOrderEntry.getOrderType() != null
                    && customerOrderEntry.getOrderType().equals("ex")) {
                Long exchangeID = customerOrderEntry.getOrderShipments().get(0).getExchangeReleaseId();
                if (exchangeID != null && !existingOrderIDs.contains(exchangeID)
                        && !exchangeIDList.contains(exchangeID)) {
                    exchangeIDList.add(exchangeID);
                }
            }
            // Get OrderIDs from exchange items list as well.
            List<CustomerOrderShipmentEntry> shipmentList = customerOrderEntry.getOrderShipments();
            for (CustomerOrderShipmentEntry shipmentEntry : shipmentList) {
                List<CustomerOrderItemEntry> orderItemList = shipmentEntry.getOrderItems();
                for (CustomerOrderItemEntry itemEntry : orderItemList) {
                    Long exchangeOrderId = itemEntry.getExchangeOrderId();
                    if (exchangeOrderId != null && !existingOrderIDs.contains(exchangeOrderId)
                            && !exchangeIDList.contains(exchangeOrderId)) {
                        exchangeIDList.add(exchangeOrderId);
                    }
                }
            }
        }

        for (int i = 0; i < exchangeIDList.size(); i++) {
            try {
                CustomerOrderResponse response = CustomerOrderClient.getCustomerOrderDetails(null, exchangeIDList.get(i).toString(), 1, true);
                if (response != null && response.getStatus().getStatusType().equals("SUCCESS")) {
                    customerOrderEntryList.addAll(response.getCustomerOrderEntryList());
                }
            } catch (ERPServiceException e) {
            }
        }

        if (exchangeIDList.size() > 0) {
            addExchangeOrders();
        } else {
            if (customerOrderEntryList.size() > 1) {
                Collections.sort(customerOrderEntryList, new Comparator<CustomerOrderEntry>() {
                    public int compare(CustomerOrderEntry o1, CustomerOrderEntry o2) {
                        // Sort descending order of orderId
                        return o2.getOrderId().compareTo(o1.getOrderId());
                    }
                });
            }
        }
    }

    private void checkZeroPay() {
        if (customerOrderEntryList == null) {
            return;
        }

        for (CustomerOrderEntry customerOrderEntry : customerOrderEntryList) {
            if (customerOrderEntry.getPaymentMethod().equals("on")) {

                if (customerOrderEntry.getPaymentMethodCardBankName() != null
                        && customerOrderEntry.getPaymentMethodCardBankName().equalsIgnoreCase("cashback")) {

                    customerOrderEntry.setPaymentMethodDisplay("cashback / zeropay");
                    customerOrderEntry.setPaymentMethodCardBankName(null);


                } else if (customerOrderEntry.getCashRedeemed() != null
                        && customerOrderEntry.getCashRedeemed() > 0) {

                    if (customerOrderEntry.getFinalAmount() == null || customerOrderEntry.getFinalAmount() == 0) {
                        customerOrderEntry.setPaymentMethodDisplay("cashback / zeropay");
                        customerOrderEntry.setPaymentMethodCardBankName(null);
                    }
                }
            }
        }
    }

    public void orderDataChange(Long orderId) {
        updateOrder(orderId);
        doOrderEnhancements();
    }

    public void updateOrder(Long orderId) {
        if (orderId == null || markForProcessMap == null || !markForProcessMap.containsKey(orderId)) {
            return;
        }

        markForProcessMap.remove(orderId);

        String orderIDStr = orderId.toString();
        try {
            CustomerOrderResponse response = CustomerOrderClient.getCustomerOrderDetails(null, orderIDStr, 1, true);
            CustomerOrderEntry newEntry = response.getCustomerOrderEntryList().get(0);

            for (int i = 0; i < customerOrderEntryList.size(); i++) {
                CustomerOrderEntry entry = customerOrderEntryList.get(i);
                if (entry.getOrderId().equals(orderId)) {
                    customerOrderEntryList.set(i, newEntry);
                    break;
                }
            }
        } catch (ERPServiceException ex) {
        }


    }

    public void orderDataMarkForProcess(Long orderId) {
        if (markForProcessMap == null) {
            markForProcessMap = new HashMap<Long, Long>();
        }

        markForProcessMap.put(orderId, orderId);
    }

    public String getlastNOrders() {
        int orders = customerOrderEntryList.size();
        String lastNOrders = "(Last " + orders + " Order";
        if (orders > 1) {
            lastNOrders += "s";
        }
        lastNOrders += ")";
        return lastNOrders;
    }

    private void fixOnHoldStatus() {
        for (CustomerOrderEntry customerOrderEntry : customerOrderEntryList) {
            if (customerOrderEntry.getOnHold()) {
                List<CustomerOrderShipmentEntry> shipmentEntryList = customerOrderEntry.getOrderShipments();
                for (CustomerOrderShipmentEntry shipmentEntry : shipmentEntryList) {
                    shipmentEntry.setStatus("OH");
                    shipmentEntry.setStatusDisplayName("On Hold");

                    if (customerOrderEntry.getOnHoldReason() != null
                            && !customerOrderEntry.getOnHoldReason().equals("")) {

                        shipmentEntry.setStatusDisplayName(shipmentEntry.getStatusDisplayName() + " - " + customerOrderEntry.getOnHoldReason());
                    }
                }
            }
        }
    }

    public void createShipmentIdList() {
        shipmentIdList = new ArrayList<Long>();
        for (CustomerOrderEntry customerOrderEntry : customerOrderEntryList) {
            List<CustomerOrderShipmentEntry> shipments = customerOrderEntry.getOrderShipments();
            for (CustomerOrderShipmentEntry shipmentEntry : shipments) {
                shipmentIdList.add(shipmentEntry.getShipmentId());
            }
        }
    }

    public void getReturnIDForShipmentItem() {

        if (itemToReturnEntryMap == null) {
            itemToReturnEntryMap = new HashMap<Long, List<CustomerReturnEntry>>();
        }

        for (CustomerOrderEntry customerOrderEntry : customerOrderEntryList) {
            List<CustomerOrderShipmentEntry> shipments = customerOrderEntry.getOrderShipments();            
            for (CustomerOrderShipmentEntry shipmentEntry : shipments) {
                Long shipmentID = shipmentEntry.getShipmentId();
                List<CustomerOrderItemEntry> orderItemEntryList = shipmentEntry.getOrderItems();
                
                try {
                    CustomerReturnResponse response = CustomerReturnClient.getReturnByShipmentId(null, shipmentID);                    
                    if (response != null && response.getStatus().getStatusType().equals("SUCCESS")) {
                        List<CustomerReturnEntry> returnEntrylist = response.getCustomerReturnEntryList();                        
                        for (CustomerReturnEntry returnEntry : returnEntrylist) {                            
                            if (returnEntry.getReturnStatus().equals("RRD")) {
                                // Ignore Return Request Declined Cases.
                                continue;
                            }                            
                            
                            List<CustomerOrderItemEntry> appendOrderItemEntryList = new ArrayList<CustomerOrderItemEntry>();
                            
                            for (CustomerOrderItemEntry orderItemEntry : orderItemEntryList) {
                                // order item exists in return detail, show this item in grid
                                if (returnEntry.getItemId().equals(orderItemEntry.getId())) {                                
                                    // append return detail for the order item
                                    appendItemToReturnEntryMap( orderItemEntry, returnEntry);                                 
                                    
                                // if partially returned item added in order item, consider this item to show in grid
                                }else if(!returnEntry.getItemId().equals(orderItemEntry.getId()) 
                                            && returnEntry.getOrderItemEntry().getSkuId().equals(orderItemEntry.getSkuId())
                                            && orderItemEntry.getStatus().equals("derived-RT")){
                                    
                                    // modify quantity and refactor price for derived-RT item(created return order item)
                                    modifyCreatedReturnedOrderItem(orderItemEntry, returnEntry);
                                    
                                    // find base order item of the derived-RT item(loop order item entry list)
                                    for (CustomerOrderItemEntry baseItem : orderItemEntryList){
                                        if(!orderItemEntry.getId().equals(baseItem.getId())
                                            && orderItemEntry.getSkuId().equals(baseItem.getSkuId())
                                            && baseItem.getStatus().equals("D")){
                                            
                                            // modify base(for which there is a partial return and is listed in OMS service) 
                                            // returned order item
                                            modifyBaseReturnedOrderItem(baseItem, returnEntry);
                                            break;
                                        }
                                    }
                                    
                                    // append return entry to map
                                    appendItemToReturnEntryMap( orderItemEntry, returnEntry);
                                    
                                // TODO remove this part of elseif once OMS service provides returned(partially returned) item detail
                                } else if (!returnEntry.getItemId().equals(orderItemEntry.getId()) 
                                            && returnEntry.getOrderItemEntry().getSkuId().equals(orderItemEntry.getSkuId())
                                            && (orderItemEntry.getStatus().equals("D") || orderItemEntry.getStatus().equals("RT"))) {
                                    
                                    // check for base order item exists in map, if yes append return entry                                    
                                    if(itemToReturnEntryMap.containsKey(orderItemEntry.getId())){
                                        appendItemToReturnEntryMap( orderItemEntry, returnEntry);                                        
                                    } else {
                                        
                                        boolean isOrderItemExistsInReturnList = false;
                                        // order item exists in return items (check in loop for order item in return entry list)
                                        for(CustomerReturnEntry returnedItem : returnEntrylist){
                                            if(orderItemEntry.getId().equals(returnedItem.getReturnId())){
                                                appendItemToReturnEntryMap( orderItemEntry, returnEntry);
                                                isOrderItemExistsInReturnList = true;
                                                break;
                                            }
                                        }
                                        
                                        // create and append partially returned item to order items
                                        if(!isOrderItemExistsInReturnList){                                            
                                            // creating and appending the partial returned item from returned item detail 
                                            // and similar(in sku) order item to order item list
                                            CustomerOrderItemEntry appendOrderItemEntry = new CustomerOrderItemEntry();                                    
                                            createPartialReturnedOrderItem(appendOrderItemEntry, returnEntry, orderItemEntry);
                                            
                                            // modify base(for which there is a partial return and is listed in OMS service) returned order item
                                            modifyBaseReturnedOrderItem(orderItemEntry, returnEntry);
                                            
                                            if(appendOrderItemEntry.getSkuId()!=null) {
                                                // Resolving for error in orderid: 10026587 where qty is zero
                                                appendOrderItemEntryList.add(appendOrderItemEntry);
                                                appendItemToReturnEntryMap( appendOrderItemEntry, returnEntry);
                                            }
                                        }
                                    }
                                }
                            }
                            
                            if(appendOrderItemEntryList.size()>0){
                                orderItemEntryList.addAll(appendOrderItemEntryList);
                            }
                        }
                    }
                } catch (ERPServiceException e) {
                }
                
                // sort item entry list
                Collections.sort(orderItemEntryList, new Comparator<CustomerOrderItemEntry>() {
                    public int compare(CustomerOrderItemEntry item1, CustomerOrderItemEntry item2) {
                            int flag;
                            if(item1.getStyleName() == null || item2.getStyleName() == null){
                                flag=0;
                                
                            }else{
                                flag = item1.getStyleName().compareToIgnoreCase(item2.getStyleName());
                            }
                            
                            if (flag == 0) {
                                return item1.getSkuId().compareTo(item2.getSkuId());
                            } else {
                                return flag;
                            }
                    }
                });
            }
        }
        
        
        
    }

    public void getDeliveryStaffDetail() {

        if (shipmentToStaffEntryMap == null) {
            shipmentToStaffEntryMap = new HashMap<Long, TripDeliveryStaffEntry>();
        }

        for (CustomerOrderEntry customerOrderEntry : customerOrderEntryList) {
            List<CustomerOrderShipmentEntry> shipments = customerOrderEntry.getOrderShipments();
            for (CustomerOrderShipmentEntry shipmentEntry : shipments) {
                Long shipmentID = shipmentEntry.getShipmentId();
                List<CustomerOrderTripAssignmentEntry> tripAssignmentEntryList = shipmentEntry.getOrderTripAssignmentEntry();

                if (tripAssignmentEntryList != null && tripAssignmentEntryList.size() > 0) {

                    Long deliveryStaffId = tripAssignmentEntryList.get(0).getDeliveryStaffId();

                    try {
                        if (deliveryStaffId != null) {
                            TripDeliveryStaffResponse response = TripDeliveryStaffClient.getTripDeliveryStaffDetail(null, deliveryStaffId, true);
                            if (response != null && response.getStatus().getStatusType().equals("SUCCESS")) {
                                shipmentToStaffEntryMap.put(shipmentID, response.getTripDeliveryStaffEntry());

                            }
                        }

                    } catch (ERPServiceException ex) {
                    }
                }
            }
        }
    }

    private void createPartialReturnedOrderItem(CustomerOrderItemEntry appendOrderItemEntry, 
            CustomerReturnEntry returnEntry, CustomerOrderItemEntry orderItemEntry) {
        Integer oldItemQty = orderItemEntry.getQuantity();
        
        if(oldItemQty==null || oldItemQty.equals(0)) {
            return;
        }
        
        // set values for appendable order item entry
        appendOrderItemEntry.setId(returnEntry.getItemId());                                    
        appendOrderItemEntry.setQuantity(Integer.valueOf(returnEntry.getQuantity()));
        // to calculate the item price according to the new qty set
        Double qtyFactor = (1.0 * appendOrderItemEntry.getQuantity()) / oldItemQty;
        appendOrderItemEntry.setStatus("derived-RT");
        appendOrderItemEntry.setStatusDisplayName("Returned");
        appendOrderItemEntry.setIsReturnableProduct(false);
        
        // these fields are needed for sorting by Collection.sort() method
        appendOrderItemEntry.setSkuId(orderItemEntry.getSkuId());
        appendOrderItemEntry.setStyleName(orderItemEntry.getStyleName());
        appendOrderItemEntry.setStyleId(orderItemEntry.getStyleId());
        
        //appendOrderItemEntry.setItemBarcodes(orderItemEntry.getItemBarcodes());
        appendOrderItemEntry.setSearchImagezURL(orderItemEntry.getSearchImagezURL());
        appendOrderItemEntry.setSize(orderItemEntry.getSize());
        appendOrderItemEntry.setUnitPrice(orderItemEntry.getUnitPrice()*qtyFactor);
        appendOrderItemEntry.setTaxAmount(orderItemEntry.getTaxAmount()*qtyFactor);
        appendOrderItemEntry.setLoyaltyAmountUsed(orderItemEntry.getLoyaltyAmountUsed()*qtyFactor);
        appendOrderItemEntry.setDiscount(orderItemEntry.getDiscount()*qtyFactor);
        appendOrderItemEntry.setCartDiscount(orderItemEntry.getCartDiscount()*qtyFactor);
        appendOrderItemEntry.setCouponDiscount(orderItemEntry.getCouponDiscount()*qtyFactor);
        appendOrderItemEntry.setCashRedeemed(orderItemEntry.getCashRedeemed()*qtyFactor);
        appendOrderItemEntry.setFinalAmount(orderItemEntry.getFinalAmount()*qtyFactor);
    }

    private void appendItemToReturnEntryMap(CustomerOrderItemEntry orderItemEntry, CustomerReturnEntry returnEntry) {
        List<CustomerReturnEntry> mappedReturnList = itemToReturnEntryMap.get(orderItemEntry.getId());
        if(mappedReturnList == null){
            mappedReturnList = new ArrayList<CustomerReturnEntry>();
        }
        mappedReturnList.add(returnEntry);
        itemToReturnEntryMap.put(orderItemEntry.getId(), mappedReturnList);
    }

    private void modifyBaseReturnedOrderItem(CustomerOrderItemEntry orderItemEntry, CustomerReturnEntry returnEntry) {
        Integer oldItemQty = orderItemEntry.getQuantity();
        if (oldItemQty==null || oldItemQty.equals(0)) {
            return;
        }
        // set values for existing order item for which partial return is made
        // subtract partially returned qty from order item
        orderItemEntry.setQuantity(orderItemEntry.getQuantity()- Integer.valueOf(returnEntry.getQuantity()));
        // to calculate the item price according to the new qty set
        Double oQtyFactor = (1.0 * orderItemEntry.getQuantity()) / oldItemQty;
        orderItemEntry.setUnitPrice(orderItemEntry.getUnitPrice()*oQtyFactor);
        orderItemEntry.setTaxAmount(orderItemEntry.getTaxAmount()*oQtyFactor);
        orderItemEntry.setLoyaltyAmountUsed(orderItemEntry.getLoyaltyAmountUsed()*oQtyFactor);
        orderItemEntry.setDiscount(orderItemEntry.getDiscount()*oQtyFactor);
        orderItemEntry.setCartDiscount(orderItemEntry.getCartDiscount()*oQtyFactor);
        orderItemEntry.setCouponDiscount(orderItemEntry.getCouponDiscount()*oQtyFactor);
        orderItemEntry.setCashRedeemed(orderItemEntry.getCashRedeemed()*oQtyFactor);
        orderItemEntry.setFinalAmount(orderItemEntry.getFinalAmount()*oQtyFactor);
    }

    private void modifyCreatedReturnedOrderItem(CustomerOrderItemEntry orderItemEntry, CustomerReturnEntry returnEntry) {
        
        Integer oldItemQty = orderItemEntry.getQuantity();
        if (oldItemQty==null || oldItemQty.equals(0)) {
            return;
        }
        orderItemEntry.setQuantity(orderItemEntry.getQuantity()+ Integer.valueOf(returnEntry.getQuantity()));
        // to calculate the item price according to the new qty set
        Double qtyFactor = (1.0 * orderItemEntry.getQuantity()) / oldItemQty;
        orderItemEntry.setUnitPrice(orderItemEntry.getUnitPrice()*qtyFactor);
        orderItemEntry.setTaxAmount(orderItemEntry.getTaxAmount()*qtyFactor);
        orderItemEntry.setLoyaltyAmountUsed(orderItemEntry.getLoyaltyAmountUsed()*qtyFactor);
        orderItemEntry.setDiscount(orderItemEntry.getDiscount()*qtyFactor);
        orderItemEntry.setCartDiscount(orderItemEntry.getCartDiscount()*qtyFactor);
        orderItemEntry.setCouponDiscount(orderItemEntry.getCouponDiscount()*qtyFactor);
        orderItemEntry.setCashRedeemed(orderItemEntry.getCashRedeemed()*qtyFactor);
        orderItemEntry.setFinalAmount(orderItemEntry.getFinalAmount()*qtyFactor);
    }
    
    public boolean isMarketPlaceShipment(CustomerOrderShipmentEntry shipment){        
        if(shipment != null && shipment.getOrderItems().size()>0){
            if(shipment.getOrderItems().get(0).getSupplyType() != null){
                return (shipment.getOrderItems().get(0).getSupplyType().equals("JUST_IN_TIME"))? true : false;    
            }
        }
        return false;
    }
}