package com.myntra.erp.crm.oneview.model;

import com.myntra.portal.crm.client.entry.CouponEntry;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author pravin
 */
public class CouponModel implements Serializable {
    
    /** coupon entry
    private String coupon;
    private Double minimum;
    private Integer times;
    private String per_user;
    private Integer times_used;
    private String couponType;
    private Double mrpAmount;
    private Double mrpPercentage;
    private Date expire;
    private String status;
    private String styleid;
    private Date startdate;
    private String groupName;
    private Boolean isInfinite;
    private Integer maxUsageByUser;
    private Boolean isInfinitePerUser;
    private String couponStatus;
    */
    
    //private List<CouponEntry> couponEntryList;
    
    private List<CouponEntry> couponEntryListActive;
    private List<CouponEntry> couponEntryListUsed;
    private List<CouponEntry> couponEntryListExpired;
    
    private static String COUPON_ACTIVE = "Active";
    private static String COUPON_USED = "Used";
    private static String COUPON_EXPIRED = "Expired";
    
    public List<CouponEntry> getCouponEntryListActive() {
        return couponEntryListActive;
    }

    public void setCouponEntryListActive(List<CouponEntry> couponEntryListActive) {
        this.couponEntryListActive = couponEntryListActive;
    }

    public List<CouponEntry> getCouponEntryListUsed() {
        return couponEntryListUsed;
    }

    public void setCouponEntryListUsed(List<CouponEntry> couponEntryListUsed) {
        this.couponEntryListUsed = couponEntryListUsed;
    }

    public List<CouponEntry> getCouponEntryListExpired() {
        return couponEntryListExpired;
    }

    public void setCouponEntryListExpired(List<CouponEntry> couponEntryListExpired) {
        this.couponEntryListExpired = couponEntryListExpired;
    }

    public void setCouponEntryList(List<CouponEntry> couponEntryList) {
        this.couponEntryListActive = new ArrayList<CouponEntry>();
        this.couponEntryListExpired = new ArrayList<CouponEntry>();
        this.couponEntryListUsed = new ArrayList<CouponEntry>();
        
        Iterator<CouponEntry> iter = couponEntryList.iterator();
        while(iter.hasNext()) {
            CouponEntry couponEntry = (CouponEntry)iter.next();
            if(couponEntry.getCouponStatus()==null) {
                // Do nothing
            } else if(couponEntry.getCouponStatus().equals(COUPON_ACTIVE)) {
                this.couponEntryListActive.add(couponEntry);
            } else if (couponEntry.getCouponStatus().equals(COUPON_EXPIRED)) {
                this.couponEntryListExpired.add(couponEntry);
            } else if (couponEntry.getCouponStatus().equals(COUPON_USED)) {
                this.couponEntryListUsed.add(couponEntry);
            }
        }
    }
}