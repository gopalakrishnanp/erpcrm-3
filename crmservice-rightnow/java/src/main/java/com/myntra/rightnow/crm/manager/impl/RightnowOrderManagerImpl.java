package com.myntra.rightnow.crm.manager.impl;

import java.util.ArrayList;
import java.util.List;

import com.myntra.commons.exception.ERPServiceException;
import com.myntra.commons.manager.impl.BaseManagerImpl;
import com.myntra.commons.response.AbstractResponse;
import com.myntra.erp.crm.client.CustomerOrderClient;
import com.myntra.erp.crm.client.entry.CustomerOrderEntry;
import com.myntra.erp.crm.client.entry.CustomerOrderShipmentEntry;
import com.myntra.erp.crm.client.response.CustomerOrderResponse;
import com.myntra.rightnow.crm.manager.RightnowOrderManager;

public class RightnowOrderManagerImpl extends
		BaseManagerImpl<CustomerOrderResponse, CustomerOrderEntry> implements
		RightnowOrderManager {

	@Override
	public CustomerOrderResponse getOrderDetails(long orderid) throws ERPServiceException {

		String orderidString = Long.valueOf(orderid).toString();
		// Call OMS service to get order response
		CustomerOrderResponse orderResponse = CustomerOrderClient.getCustomerOrderDetails(null, orderidString, 1, true);
		return orderResponse;			
	}
	
	@Override
	public CustomerOrderResponse getShipmentDetails(long shipmentid) throws ERPServiceException {

		String shipmentidString = Long.valueOf(shipmentid).toString();
		// Call OMS service to get order response
		CustomerOrderResponse orderResponse = CustomerOrderClient.getCustomerOrderDetails(null, shipmentidString, 1, true);
		
		if(orderResponse.getStatus().getStatusType().equals("SUCCESS")
				&& orderResponse.getStatus().getTotalCount()>0
				&& orderResponse.getCustomerOrderEntryList().get(0).getOrderShipments().size()>1) {
			
			List<CustomerOrderShipmentEntry> check = orderResponse.getCustomerOrderEntryList().get(0).getOrderShipments();
			
			List<CustomerOrderShipmentEntry> newShipmentList = new ArrayList<CustomerOrderShipmentEntry>();
			
			for(CustomerOrderShipmentEntry shipmentEntry: check) {
				if(shipmentEntry.getShipmentId().longValue()==shipmentid) {
					newShipmentList.add(shipmentEntry);
					orderResponse.getCustomerOrderEntryList().get(0).setOrderShipments(newShipmentList);
					break;
				}
			}
		}
		
		return orderResponse;			
	}

	@Override
	public AbstractResponse create(CustomerOrderEntry e)
			throws ERPServiceException {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	@Override
	public AbstractResponse update(CustomerOrderEntry e, Long l)
			throws ERPServiceException {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	@Override
	public AbstractResponse search(int i, int i1, String string,
			String string1, String string2) throws ERPServiceException {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	@Override
	public AbstractResponse findById(Long l) throws ERPServiceException {
		throw new UnsupportedOperationException("Not supported yet.");
	}
}
