package com.myntra.rightnow.crm.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.axis2.AxisFault;

import com.myntra.commons.exception.ERPServiceException;
import com.myntra.rightnow.crm.base.BasicRunAnalyticsReport;
import com.myntra.rightnow.crm.client.entry.RightnowReportData;
import com.myntra.rightnow.crm.client.entry.RightnowReportFilterEntry;
import com.myntra.rightnow.crm.constants.RightnowConstants;
import com.rightnow.ws.base.ActionEnum;
import com.rightnow.ws.base.ChainDestinationID;
import com.rightnow.ws.base.ID;
import com.rightnow.ws.base.NamedID;
import com.rightnow.ws.base.RNObject;
import com.rightnow.ws.generic.DataTypeEnum;
import com.rightnow.ws.generic.DataValue;
import com.rightnow.ws.generic.GenericField;
import com.rightnow.ws.generic.GenericObject;
import com.rightnow.ws.generic.RNObjectType;
import com.rightnow.ws.messages.BatchRequestItem;
import com.rightnow.ws.messages.BatchRequestItemChoice_type0;
import com.rightnow.ws.messages.CSVRow;
import com.rightnow.ws.messages.CSVTable;
import com.rightnow.ws.messages.CreateMsg;
import com.rightnow.ws.messages.CreateProcessingOptions;
import com.rightnow.ws.messages.UpdateMsg;
import com.rightnow.ws.messages.UpdateProcessingOptions;
import com.rightnow.ws.objects.Note;
import com.rightnow.ws.objects.NoteList;
import com.rightnow.ws.objects.Task;

public class TaskyUtils {
	
	public static Map<String, String> CATEGORY_TODEPARTMENT_MAP = new HashMap<String, String>();
	
	static {
		CATEGORY_TODEPARTMENT_MAP.put("proactive ob:order confirmation:promise date crossed", "CC-Outbound");
		CATEGORY_TODEPARTMENT_MAP.put("proactive ob:ud calling:courier partner unable to deliver", "CC-Outbound");
		CATEGORY_TODEPARTMENT_MAP.put("proactive ob:oos calling:item oos, pmm issues in delhi warehouse", "CC-Outbound");
		CATEGORY_TODEPARTMENT_MAP.put("proactive ob:pmm calling:pmm issues in bangalore warehouse", "CC-Outbound");
		CATEGORY_TODEPARTMENT_MAP.put("proactive ob:telesales on hold:any telesale order where cb has been used", "CC-Outbound");
		CATEGORY_TODEPARTMENT_MAP.put("proactive ob:pickup fail:customer/courier issue", "CC-Outbound");
		CATEGORY_TODEPARTMENT_MAP.put("proactive ob:seized by sales tax:orders seized by sales tax", "CC-Outbound");
		CATEGORY_TODEPARTMENT_MAP.put("proactive ob:pp to q:order in pre processed state", "CC-Outbound");
		CATEGORY_TODEPARTMENT_MAP.put("proactive ob:online rto cancellation:informing customer on rto status", "CC-Outbound");
		CATEGORY_TODEPARTMENT_MAP.put("proactive ob:mrp mismatch:difference in price on website and product", "CC-Outbound");
		CATEGORY_TODEPARTMENT_MAP.put("proactive ob:complete address/contact number:confirm addess and contact number", "CC-Outbound");
		CATEGORY_TODEPARTMENT_MAP.put("proactive ob:free gift policy:educated the customer about free gift policy", "CC-Outbound");
		CATEGORY_TODEPARTMENT_MAP.put("proactive ob:on hold orders:cancel the order", "CC-Outbound");
		CATEGORY_TODEPARTMENT_MAP.put("proactive ob:order lost:order lost in transit", "CC-Outbound");
		CATEGORY_TODEPARTMENT_MAP.put("proactive ob:order confirmation:check if cm wants order or not", "CC-Outbound");
		CATEGORY_TODEPARTMENT_MAP.put("proactive ob:detractor calling:dissatisfied customer", "CC-Outbound");
		CATEGORY_TODEPARTMENT_MAP.put("proactive ob:welcome calling:first time customer", "CC-Outbound");
	}
	
	public static String getDepartmentFromCategory(String category) {
		return CATEGORY_TODEPARTMENT_MAP.get(category);
	}

	public static String getTimeInDayHourMinuteFormat(String timeInSeconds, boolean isDayFormat) {
        StringBuffer formattedTime = new StringBuffer();
        if(timeInSeconds == null || timeInSeconds.equals("")) 
        	return "";
        int time = Integer.parseInt(timeInSeconds);

        int minInSecs = 60;
        int hourInSecs = minInSecs * 60;
        int dayInSecs = hourInSecs * 24;

        int days = time / dayInSecs;
        
        //if it is only day format, return the integer value
        if(isDayFormat)
        	return Integer.toString(days);
        
        time = time % dayInSecs;

        int hours = time / hourInSecs;

        if(days >= 0) {
        	formattedTime.append((days < 10 ? "0" : "") + days + " Days ");
        }

        if(hours > 0) {
        	formattedTime.append(":" + (hours < 10 ? "0" : "") + hours + " Hours");
        }

        if(formattedTime.toString().isEmpty()) {
        	formattedTime.append("00 Hours");
        }

        return formattedTime.toString();

	}
	
	public static boolean validateEmail(String email) {
		String regex = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(email);
		return matcher.matches();
	}
	
	public static Date convertStringToDate(String dateText) {
		Date result = null;
		//remove leading and trailing apostrophes (') if any
		dateText = dateText.startsWith("'") ? dateText.substring(1) : dateText;
		dateText = dateText.endsWith("'") ? dateText.substring(0, dateText.length() - 1) : dateText;
		
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
		try {
			result = dateFormat.parse(dateText);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return result;
	}
	
	/**
	 * Create a GenericObject with the given GenericField array and the Type
	 * It will be used for dealing with saving/updating of custom fields/objects.
	 * @param fields
	 * @param type
	 * @return
	 */
	public static GenericObject createCustomObject(GenericField[] fields, String type) {
		GenericObject genericObject = new GenericObject();
	    RNObjectType objType = new RNObjectType();
	    objType.setTypeName(type);                
	    genericObject.setGenericFields(fields);
	    genericObject.setObjectType(objType);	    
	    return genericObject;
	}
	
	/**
	 * Created a custom field with the given field name & value. Also performs NamedID creation (both ID & Name).
	 * @param fieldName
	 * @param value
	 * @param isNamedID
	 * @return
	 */
	public static GenericField addCustomFields(String fieldName, Object value, boolean isNamedID, boolean isDateTime) {
		GenericField genericCustomField = new GenericField();
		DataValue dv = new DataValue();
		if(isNamedID) {
			genericCustomField.setDataType(DataTypeEnum.NAMED_ID);
			NamedID ni = new NamedID();		    
		    if(value instanceof Long) {
		    	ID niID = new ID();
		    	niID.setId((Long) value);
		    	ni.setID(niID);
		    } else if(value instanceof ChainDestinationID) {
		    	ni.setID((ChainDestinationID) value);
		    } else {
		    	ni.setName((String) value);
		    }
		    dv.setNamedIDValue(ni);
		} else if(value instanceof String) {
			genericCustomField.setDataType(DataTypeEnum.STRING);			
			dv.setStringValue((String) value);
		} else if(value instanceof Date) {
			if(isDateTime) {
				genericCustomField.setDataType(DataTypeEnum.DATETIME);
				Calendar calendar = Calendar.getInstance();
				calendar.setTime((Date) value);
				dv.setDateTimeValue(calendar);
			} else {
				genericCustomField.setDataType(DataTypeEnum.DATE);
				dv.setDateValue((Date) value);
			}
		} else if(value instanceof GenericObject) {
			genericCustomField.setDataType(DataTypeEnum.OBJECT);
			dv.setObjectValue((GenericObject) value);
		}
		genericCustomField.setName(fieldName);
	    genericCustomField.setDataValue(dv);         
	    return genericCustomField;
	}
	
	/**
	 * Create a batch request item from an RNObject array
	 * @param objects
	 * @return
	 */
	public static BatchRequestItem getBatchRequestItem(RNObject[] objects) {
		BatchRequestItem createItem = new BatchRequestItem();
		BatchRequestItemChoice_type0 batchItemCreate = new BatchRequestItemChoice_type0();
		
		CreateMsg createMsg = new CreateMsg();
		CreateProcessingOptions createProcessingOptions = new CreateProcessingOptions();
		createProcessingOptions.setSuppressExternalEvents(false);
		createProcessingOptions.setSuppressRules(false);
		createMsg.setProcessingOptions(createProcessingOptions);
		
		createMsg.setRNObjects(objects);
		
		batchItemCreate.setCreateMsg(createMsg);
		createItem.setBatchRequestItemChoice_type0(batchItemCreate);
		return createItem;
	}
	
	/**
	 * Create a batch request item from an RNObject array
	 * @param objects
	 * @return
	 */
	public static BatchRequestItem getBatchRequestItemForUpdate(RNObject[] objects) {
		BatchRequestItem updateItem = new BatchRequestItem();
		BatchRequestItemChoice_type0 batchItemUpdate = new BatchRequestItemChoice_type0();
		
		UpdateMsg updateMsg = new UpdateMsg();
		UpdateProcessingOptions updateProcessingOptions = new UpdateProcessingOptions();
		updateProcessingOptions.setSuppressExternalEvents(false);
		updateProcessingOptions.setSuppressRules(false);
		updateMsg.setProcessingOptions(updateProcessingOptions);
		
		updateMsg.setRNObjects(objects);
		
		batchItemUpdate.setUpdateMsg(updateMsg);
		updateItem.setBatchRequestItemChoice_type0(batchItemUpdate);
		return updateItem;
	}
	
	
	public static String convertListToString(List<String> emails) {
		StringBuilder result = new StringBuilder();
		for(String email : emails) {
			result.append("'").append(email).append("',");
		}
		//System.out.println(result.length());
		//System.out.println(result.toString().substring(0,result.length()-1));
		return result.toString().substring(0, result.length()-1);
	}
	
	public static RightnowReportData getReportData(String reportName, List<RightnowReportFilterEntry> filterEntries) throws ERPServiceException {
		BasicRunAnalyticsReport report;
		try {
			report = new BasicRunAnalyticsReport();
		} catch (AxisFault e) {
			throw new ERPServiceException(e.getMessage(), 1);
		}
		
		CSVTable table;
		//System.out.println("Fetching categories for tasks");
		try {
			table = report.runAnalyticsReport(reportName, filterEntries, 0, 0);
		} catch (ERPServiceException e) {						
			throw e;
		}
		
		CSVRow rows = table.getRows();
		String[] rowData = rows.getRow();
		String[] columnData = table.getColumns().split(RightnowConstants.RIGHTNOW_REPORT_DELIMITER_SPLIT_ESCAPE);
		RightnowReportData reportData = new RightnowReportData(columnData, rowData);
		return reportData;
	}
	
	public static void addNoteToTask(Task task, String noteText) {
		if(noteText != null && !noteText.trim().isEmpty()) {
			NoteList noteList = new NoteList();
			Note note = new Note();
			note.setAction(ActionEnum.add);
			note.setText(noteText);
			noteList.addNoteList(note);
			task.setNotes(noteList);
		}
	}
	
	public static String stripQuotes(String input) {
        String str = input;
        if (str.startsWith("\"")) {
            str = str.substring(1, str.length());
        }
        if (str.endsWith("\"")) {
            str = str.substring(0, str.length() - 1);
        }
        return str;
    }
	
	public static Calendar getCurrentTimeForRightnow() {
		Calendar c = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
		long gmtTime = c.getTime().getTime();
		long timezoneAlteredTime = gmtTime - (330*60*1000);
		Calendar c1 = Calendar.getInstance(TimeZone.getTimeZone("Asia/Calcutta"));
		c1.setTimeInMillis(timezoneAlteredTime);
		return c1;
	}
	
	public static Date getCurrentDateTimeForRightnow() {
		Calendar c = getCurrentTimeForRightnow();
		return c.getTime();
	}
}
