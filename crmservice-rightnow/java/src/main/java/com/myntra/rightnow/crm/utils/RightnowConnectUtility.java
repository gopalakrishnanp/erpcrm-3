package com.myntra.rightnow.crm.utils;

import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.impl.OMNamespaceImpl;
import org.apache.axiom.om.impl.llom.OMElementImpl;
import org.apache.axiom.soap.impl.llom.soap11.SOAP11Factory;
import org.apache.axis2.AxisFault;
import org.apache.axis2.client.ServiceClient;
import org.apache.ws.security.WSConstants;

import com.rightnow.ws.wsdl.RightNowSyncService;
import com.rightnow.ws.wsdl.RightNowSyncServiceStub;

public class RightnowConnectUtility {

	private static RightnowConnectUtility rightnowConnectUtility = null;

	private RightNowSyncService service;

	public RightNowSyncService getService() {
		return service;
	}

	public void setService(RightNowSyncService service) {
		this.service = service;
	}

	public RightnowConnectUtility() throws AxisFault {
		service = new RightNowSyncServiceStub();
		ServiceClient serviceClient = ((org.apache.axis2.client.Stub) service)
				._getServiceClient();
		serviceClient.addHeader(createSecurityHeader("task.webapp", "tasky"));
	}

	private OMElement createSecurityHeader(String username, String password) {

		OMNamespaceImpl wsseNS = new OMNamespaceImpl(WSConstants.WSSE_NS, WSConstants.WSSE_PREFIX);
		OMFactory factory = new SOAP11Factory();
		OMElementImpl securityHeader;
		OMElementImpl usernameTokenElement;
		OMElementImpl usernameElement;
		OMElementImpl passwordElement;

		// create the Security header block
		securityHeader = new OMElementImpl("Security", wsseNS, factory);
		securityHeader.addAttribute("mustUnderstand", "1", null);

		// nest the UsernameToken in the Security header
		usernameTokenElement = new OMElementImpl(WSConstants.USERNAME_TOKEN_LN,
				wsseNS, securityHeader, factory);

		// nest the Username and Password elements
		usernameElement = new OMElementImpl(WSConstants.USERNAME_LN, wsseNS,
				usernameTokenElement, factory);
		usernameElement.setText(username);

		passwordElement = new OMElementImpl(WSConstants.PASSWORD_LN, wsseNS,
				usernameTokenElement, factory);
		passwordElement.setText(password);
		passwordElement.addAttribute(WSConstants.PASSWORD_TYPE_ATTR,
				WSConstants.PASSWORD_TEXT, null);

		return securityHeader;
	}

	public static RightnowConnectUtility getInstance() {
		if (rightnowConnectUtility == null) {
			try {
				rightnowConnectUtility = new RightnowConnectUtility();
			} catch (AxisFault af) {
				af.printStackTrace();
			}
		}
		return rightnowConnectUtility;
	}
}