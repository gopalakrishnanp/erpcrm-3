package com.myntra.rightnow.crm.manager.impl;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;

import org.apache.log4j.Logger;

import com.myntra.commons.exception.ERPServiceException;
import com.myntra.commons.utils.Misc;
import com.myntra.rightnow.crm.client.code.RightnowErrorCodes;
import com.myntra.rightnow.crm.manager.RightnowContactManager;
import com.myntra.rightnow.crm.utils.RightnowConnectUtility;
import com.myntra.rightnow.crm.utils.TaskyUtils;
import com.rightnow.ws.base.ActionEnum;
import com.rightnow.ws.base.ID;
import com.rightnow.ws.base.NamedID;
import com.rightnow.ws.base.RNObject;
import com.rightnow.ws.generic.DataTypeEnum;
import com.rightnow.ws.generic.DataValue;
import com.rightnow.ws.generic.GenericField;
import com.rightnow.ws.generic.GenericObject;
import com.rightnow.ws.generic.RNObjectType;
import com.rightnow.ws.messages.ClientInfoHeader;
import com.rightnow.ws.messages.CreateProcessingOptions;
import com.rightnow.ws.messages.RNObjectsResult;
import com.rightnow.ws.messages.UpdateProcessingOptions;
import com.rightnow.ws.messages.UpdateResponseMsg;
import com.rightnow.ws.objects.Contact;
import com.rightnow.ws.objects.Email;
import com.rightnow.ws.objects.EmailList;
import com.rightnow.ws.objects.PersonName;
import com.rightnow.ws.objects.Phone;
import com.rightnow.ws.objects.PhoneList;
import com.rightnow.ws.wsdl.RequestErrorFault;
import com.rightnow.ws.wsdl.ServerErrorFault;
import com.rightnow.ws.wsdl.UnexpectedErrorFault;

public class RightnowContactManagerImpl implements RightnowContactManager {
	
	private static final Logger LOGGER = Logger.getLogger(RightnowContactManagerImpl.class);

	
	private String camelCase(String strdata) {
		
		StringBuffer strbufCamelCase = new StringBuffer();
		StringTokenizer st = new StringTokenizer(strdata);
		if(null == strdata || strdata.length() == 0){
			return "";
		}
		
		while(st.hasMoreTokens()) {
			String strWord = st.nextToken();
			strbufCamelCase.append(strWord.substring(0,1).toUpperCase());
			
			if(strWord.length()>1) {
				strbufCamelCase.append(strWord.substring(1).toLowerCase());
			}
			if(st.hasMoreTokens())
				strbufCamelCase.append(" ");
		}
		return strbufCamelCase.toString();
	}
	
	@Override
	public boolean createContact(String firstName, String lastName, String gender, String email, String login, String mobile, String phone, Date dob) 
			throws ERPServiceException {
		boolean result = false;
		
		//Set the processing options
		CreateProcessingOptions options = new CreateProcessingOptions();
		options.setSuppressExternalEvents(false);
		options.setSuppressRules(false);		
	
		ClientInfoHeader clientInfoHeader = new ClientInfoHeader();
		clientInfoHeader.setAppID("Basic Create");
		
		try {
			Contact contact = buildRightnowContactObject(firstName, lastName, gender, email, login, mobile, phone, dob);
		    
			if(contact != null) {
			    RNObject[] rnObjects = new RNObject[]{contact};
			    RNObjectsResult results = RightnowConnectUtility.getInstance().getService().create(rnObjects, options, clientInfoHeader);
				
			    LOGGER.debug("Contact created with ID: " + results.getRNObjects()[0].getID().getId());
				result = true;
			}			
		} catch (RemoteException e) {
			LOGGER.error("RemoteException while creating contact: " + e.getMessage(), e);
			throw Misc.createERPServiceException(RightnowErrorCodes.NO_CONTACT_CREATED, new String[]{email, e.getMessage()});
		} catch (ServerErrorFault e) {
			LOGGER.error("ServerErrorFault while updating contact: " + e.getMessage(), e);
			throw Misc.createERPServiceException(RightnowErrorCodes.NO_CONTACT_CREATED, new String[]{email, e.getMessage()});
		} catch (RequestErrorFault e) {
			LOGGER.error("RequestErrorFault while creating contact: " + e.getMessage(), e);
			throw Misc.createERPServiceException(RightnowErrorCodes.NO_CONTACT_CREATED, new String[]{email, e.getMessage()});
		} catch (UnexpectedErrorFault e) {
			LOGGER.error("UnexpectedErrorFault while updating Tasks: " + e.getMessage(), e);
			throw Misc.createERPServiceException(RightnowErrorCodes.NO_CONTACT_CREATED, new String[]{email, e.getMessage()});
		} catch(Exception e) {
			LOGGER.error("Exception while updating Tasks: " + e.getMessage(), e);
			throw Misc.createERPServiceException(RightnowErrorCodes.NO_CONTACT_CREATED, new String[]{email, e.getMessage()});
		}
		
		return result;
	}
	
	@Override
	public Contact buildRightnowContactObject(String firstName, String lastName, String gender, String email, String login, String mobile, String phone, Date dob) {
		Contact contact = null;
		//save the contact only if email is valid
		if(email == null)
			email = "";
		
		if(TaskyUtils.validateEmail(email)) {
			contact = new Contact();

			if(firstName!=null) {
				if(firstName.isEmpty()) {
					firstName=null;
				} else {
					firstName = firstName.replaceAll("[^a-zA-Z. ]", " ");
					firstName = firstName.replaceAll("[.]", ". ");
					firstName = firstName.replaceAll(" +", " ");
					firstName = firstName.trim();
					firstName = camelCase(firstName);
				}
			}
				
			if(lastName!=null) {
				if(lastName.isEmpty()) {
					lastName=null;
				} else {
					lastName = lastName.replaceAll("[^a-zA-Z ]", "");
					lastName = lastName.replaceAll("[.]", ". ");
					lastName = lastName.replaceAll(" +", " ");
					lastName = lastName.trim();
					lastName = camelCase(lastName);
				}
			}
			
			if(firstName==null && lastName!=null) {
				firstName = lastName;
				lastName = null;
			}
			
			// If first name is there and has spaces & last name does not exist, 
			// split the first name in two pieces and store as first & last name
			if(firstName != null && lastName == null) {
				//split the first name
				int lastIndex = firstName.lastIndexOf(" ");
				if(lastIndex>0) {
					lastName = firstName.substring(lastIndex+1);
					firstName = firstName.substring(0, lastIndex);
				}
				
			}
				
			PersonName personName = new PersonName();
			
			if(firstName != null) {
				personName.setFirst(firstName);
			}
			
			if(lastName != null) {
				personName.setLast(lastName);
			}
	
			if(firstName != null || lastName != null)
				contact.setName(personName);
			
			//set email			
			EmailList emailList = new EmailList();
			Email[] emailArray = new Email[1];
			emailArray[0] = new Email();
			emailArray[0].setAction(ActionEnum.add);
			emailArray[0].setAddress(email.trim().toLowerCase());
			NamedID emailType = new NamedID();
			ID emailTypeID = new ID();
			emailTypeID.setId(0);
			emailType.setID(emailTypeID);
			emailArray[0].setAddressType(emailType);
			emailList.setEmailList(emailArray);
			contact.setEmails(emailList);
			
			//Do not set login field in rightnow !!!
			//-- contact.setLogin(login);
			
			//set mobile & phone
			PhoneList phoneList = new PhoneList();
			List<Phone> phones = new ArrayList<Phone>();
			//set mobile
			if(mobile != null && !mobile.trim().isEmpty()) {
				Phone mobilePhone = new Phone();
				mobilePhone.setAction(ActionEnum.add);
				mobilePhone.setNumber(mobile.trim());
				NamedID phoneType = new NamedID();
				phoneType.setName("Mobile Phone");						
				mobilePhone.setPhoneType(phoneType);
				phones.add(mobilePhone);
			}
			
			//set phone
			if(phone != null && !phone.trim().isEmpty()) {
				Phone homePhone = new Phone();
				homePhone.setAction(ActionEnum.add);
				homePhone.setNumber(phone.toString());
				NamedID phoneType = new NamedID();
				phoneType.setName("Home Phone");
				homePhone.setPhoneType(phoneType);
				phones.add(homePhone);
			}
			
			if(phones.size() > 0) {
				phoneList.setPhoneList(phones.toArray(new Phone[phones.size()]));
				contact.setPhones(phoneList);
			}
			
			//set custom fields
			GenericObject genericCustomObject = new GenericObject();
		    RNObjectType customObjType = new RNObjectType();
		    customObjType.setTypeName("TaskCustomFieldsc");
		    List<GenericField> fields = new ArrayList<GenericField>();
		    
		    Date rightnowDateLimit = new Date(0);
		    //set DOB
		    if(dob != null && dob.after(rightnowDateLimit)) {
			    GenericField genericCustomField = new GenericField();
			    genericCustomField.setDataType(DataTypeEnum.DATE);
			    genericCustomField.setName("dob");	    
				DataValue dv = new DataValue();
				dv.setDateValue(dob);
				genericCustomField.setDataValue(dv);         
			    fields.add(genericCustomField);
		    }
		    
		    //set Gender
		    if(gender != null && !gender.trim().isEmpty()) {
			    GenericField genericCustomField1 = new GenericField();
			    genericCustomField1.setDataType(DataTypeEnum.NAMED_ID);
			    genericCustomField1.setName("gender");
			    NamedID idGender = new NamedID();
			    gender = gender.trim().substring(0,1);
			    if("M".equalsIgnoreCase(gender)) {
			    	idGender.setName("Male");
			    } else if("F".equalsIgnoreCase(gender)) {
			    	idGender.setName("Female");
			    }
			    DataValue dv1 = new DataValue();
				dv1.setNamedIDValue(idGender);
				genericCustomField1.setDataValue(dv1); 
				if(idGender.getName() != null) { 
					fields.add(genericCustomField1);
				}
		    }
		    
		    if(fields.size() > 0) {
			    genericCustomObject.setGenericFields(fields.toArray(new GenericField[fields.size()]));
			    genericCustomObject.setObjectType(customObjType);
			    
			    //set the Custom Field
			    GenericObject genericObject = new GenericObject();
			    RNObjectType objType = new RNObjectType();
			    objType.setTypeName("TaskCustomFields");
			    GenericField [] genericFieldsArray = new GenericField[1];
			    GenericField genericField = new GenericField();            
			    genericField.setName("c");
			    genericField.setDataType(DataTypeEnum.OBJECT); 
			    DataValue dvCustom = new DataValue();
			    dvCustom.setObjectValue(genericCustomObject);
			    genericField.setDataValue(dvCustom);
			    genericFieldsArray[0] = genericField;                 
			    genericObject.setGenericFields(genericFieldsArray);
			    genericObject.setObjectType(objType);
			    contact.setCustomFields(genericObject);
		    }
		}
		
		return contact;
	}
	
	private long fetchRNContactByLogin(String login) {
		Contact contact = new Contact();
		//set email
		EmailList emailList = new EmailList();
		Email[] emailArray = new Email[1];
		emailArray[0] = new Email();
		emailArray[0].setAction(ActionEnum.add);
		emailArray[0].setAddress(login);
		NamedID emailType = new NamedID();
		ID emailTypeID = new ID();
		emailTypeID.setId(0);
		emailType.setID(emailTypeID);
		emailArray[0].setAddressType(emailType);
		emailList.setEmailList(emailArray);
		contact.setEmails(emailList);
		
		PersonName personName = new PersonName();
		personName.setFirst("Preetam");
		personName.setLast("Thakur");
		contact.setName(personName);
		
		//Set the processing options
		UpdateProcessingOptions options = new UpdateProcessingOptions();
		options.setSuppressExternalEvents(false);
		options.setSuppressRules(false);		
	
		ClientInfoHeader clientInfoHeader = new ClientInfoHeader();
		clientInfoHeader.setAppID("Basic Update");
		
		RNObject[] rnObjects = new RNObject[]{contact};
		UpdateResponseMsg results;
		try {
			results = RightnowConnectUtility.getInstance().getService().update(rnObjects, options, clientInfoHeader);
		
		
			System.out.println("Contact created with ID: " + results);
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ServerErrorFault e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (RequestErrorFault e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnexpectedErrorFault e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;
	}
	
	public static void main(String args[]) throws ERPServiceException {
		RightnowContactManagerImpl rm = new RightnowContactManagerImpl();
		Calendar c = Calendar.getInstance();
		c.set(1983, 5, 22);
		rm.createContact("preetam", "thakur", "M", "preetam.kumar@compassitesinc.coms", "", "9916484815 ", " 9480584815", c.getTime());
	}

}
