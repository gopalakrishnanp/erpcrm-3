package com.myntra.rightnow.crm.manager;

import com.myntra.commons.exception.ERPServiceException;
import com.myntra.commons.manager.BaseManager;
import com.myntra.portal.crm.client.entry.CustomerProfileEntry;
import com.myntra.portal.crm.client.response.CustomerProfileResponse;

/**
 * Manager interface(abstract) for customer profile service to be used by rightnow
 * 
 */
public interface RightnowCustomerProfileManager extends BaseManager<CustomerProfileResponse, CustomerProfileEntry> {

	public CustomerProfileResponse getCustomerProfileByLogin(String login) throws ERPServiceException;

	public CustomerProfileResponse createCustomerProfileInRightnow(String email) throws ERPServiceException;

}