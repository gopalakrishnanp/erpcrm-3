package com.myntra.rightnow.crm.manager.impl;

import com.myntra.commons.codes.StatusResponse;
import com.myntra.commons.exception.ERPServiceException;
import com.myntra.commons.manager.impl.BaseManagerImpl;
import com.myntra.commons.response.AbstractResponse;
import com.myntra.portal.crm.client.ProfileClient;
import com.myntra.portal.crm.client.entry.CustomerProfileEntry;
import com.myntra.portal.crm.client.response.CustomerProfileResponse;
import com.myntra.rightnow.crm.manager.RightnowContactManager;
import com.myntra.rightnow.crm.manager.RightnowCustomerProfileManager;

public class RightnowCustomerProfileManagerImpl extends
		BaseManagerImpl<CustomerProfileResponse, CustomerProfileEntry> implements
		RightnowCustomerProfileManager {
	
	private RightnowContactManager rightnowContactManager;
	
	public RightnowContactManager getRightnowContactManager() {
		return rightnowContactManager;
	}

	public void setRightnowContactManager(RightnowContactManager rightnowContactManager) {
		this.rightnowContactManager = rightnowContactManager;
	}

	@Override
	public CustomerProfileResponse getCustomerProfileByLogin(String login)
			throws ERPServiceException {
		return ProfileClient.findByLogin(null, login);
	}
	
	@Override
	public CustomerProfileResponse createCustomerProfileInRightnow(String email)
			throws ERPServiceException {
		CustomerProfileResponse profileResponse = ProfileClient.findByLogin(null,  email);
		
		if(profileResponse.getStatus().getStatusType().equals("SUCCESS")
				&& profileResponse.getStatus().getTotalCount()==1
				&& profileResponse.getCustomerProfileEntry()!=null) {
			
			CustomerProfileEntry profileEntry = profileResponse.getCustomerProfileEntry();
			
			boolean checkCreate;
			
			try {
				checkCreate =
					getRightnowContactManager().createContact(profileEntry.getFirstName(), profileEntry.getLastName(),
							profileEntry.getGender(), email, null, profileEntry.getMobile(), 
							profileEntry.getPhone(), profileEntry.getDob());
				
				profileResponse.setCreatedRN(checkCreate);
			} catch (ERPServiceException e) {
				StatusResponse error = new StatusResponse(e.getCode(), e.getMessage(), StatusResponse.Type.ERROR);
				profileResponse.setStatus(error);
				profileResponse.setCustomerProfileEntry(null);
				return profileResponse;
			}
		}
		
		return profileResponse;
	}
	

	@Override
	public AbstractResponse findById(Long id) throws ERPServiceException {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	@Override
	public AbstractResponse create(CustomerProfileEntry entry)
			throws ERPServiceException {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	@Override
	public AbstractResponse update(CustomerProfileEntry entry, Long itemId)
			throws ERPServiceException {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	@Override
	public AbstractResponse search(int start, int fetchSize, String sortBy,
			String sortOrder, String searchTerms) throws ERPServiceException {
		throw new UnsupportedOperationException("Not supported yet.");
	}
}
