package com.myntra.rightnow.crm.dao.impl;

import java.math.BigInteger;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.apache.log4j.Logger;

import com.myntra.commons.dao.impl.BaseDAOImpl;
import com.myntra.commons.exception.ERPServiceException;
import com.myntra.rightnow.crm.constant.RightnowConstants;
import com.myntra.rightnow.crm.dao.WelcomeCallTaskLogDAO;
import com.myntra.rightnow.crm.entity.WelcomeCallTaskLogEntity;

/**
 * DAO for welcome call log data
 * 
 * @author Arun Kumar
 */
public class WelcomeCallTaskLogDAOImpl extends BaseDAOImpl<WelcomeCallTaskLogEntity> implements WelcomeCallTaskLogDAO {

	private static final Logger LOGGER = Logger.getLogger(WelcomeCallTaskLogDAOImpl.class);

	@Override
	public WelcomeCallTaskLogEntity getWelcomeCallTaskLog(Long orderReleaseId) throws ERPServiceException {

		if (orderReleaseId == null) {
			throw new ERPServiceException("Order release id to fetch welcome call task log is null", 0);
		}

		Object[] obj;
		try {
			Query q = null;
			q = getEntityManager().createNamedQuery(RightnowConstants.NAMED_QUERIES.WELCOME_CALL_LOG_BY_RELEASE);
			q.setParameter("orderReleaseId", orderReleaseId);

			obj = (Object[]) q.getSingleResult();
		} catch (NoResultException nre) {
			LOGGER.debug("No welcome call task created for order release");
			return null;
		}

		int index = 0;

		Long releaseId = null;
		Long taskId = null;

		BigInteger releaseIdBigInt = (BigInteger) obj[index++];
		if (releaseIdBigInt != null) {
			releaseId = releaseIdBigInt.longValue();
		}

		BigInteger taskIdBigInt = (BigInteger) obj[index++];
		if (taskIdBigInt != null) {
			taskId = taskIdBigInt.longValue();
		}

		WelcomeCallTaskLogEntity welcomeCallLogEntity = new WelcomeCallTaskLogEntity(releaseId, taskId, null);
		return welcomeCallLogEntity;

	}

	@Override
	public Integer getTotalWelcomeCallTask(Date startDate, Date endDate) throws ERPServiceException {

		Integer totalTask = null;

		try {

			Query q = getEntityManager(false).createNamedQuery(
					WelcomeCallTaskLogEntity.TOTAL_WELCOME_CALL_TASK_BY_CREATE_DATE);

			/*SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String startDateString = "2014-02-01 12:00:00";
			String endDateString = "2014-03-04 12:45:00";
			Date startDate = null, endDate = null;
			
			try {
				startDate = formatter.parse(startDateString);
				endDate = formatter.parse(endDateString);

			} catch (ParseException e) {
				e.printStackTrace();
			}*/

			q.setParameter("startDate", startDate);
			q.setParameter("endDate", endDate);

			Long totalTaskLong = (Long) q.getSingleResult();
			if (totalTaskLong != null) {
				totalTask = totalTaskLong.intValue();
			}

		} catch (NoResultException nre) {
			LOGGER.debug("No welcome call created on this date range");
			return 0;
		}

		return totalTask;
	}
}