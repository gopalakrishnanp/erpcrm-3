/**
 * 
 */
package com.myntra.rightnow.crm.manager.impl;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.log4j.Logger;

import com.myntra.commons.codes.StatusResponse;
import com.myntra.commons.exception.ERPServiceException;
import com.myntra.commons.manager.impl.BaseManagerImpl;
import com.myntra.commons.response.AbstractResponse;
import com.myntra.commons.utils.Misc;
import com.myntra.erp.crm.client.entry.CustomerOrderEntry;
import com.myntra.erp.crm.client.entry.CustomerOrderShipmentEntry;
import com.myntra.rightnow.crm.client.code.RightnowErrorCodes;
import com.myntra.rightnow.crm.client.code.RightnowSuccessCodes;
import com.myntra.rightnow.crm.client.entry.RightnowBaseTaskEntry;
import com.myntra.rightnow.crm.client.entry.RightnowBaseTaskListEntry;
import com.myntra.rightnow.crm.client.entry.RightnowReportData;
import com.myntra.rightnow.crm.client.entry.RightnowReportFilterEntry;
import com.myntra.rightnow.crm.client.entry.RightnowTaskDetractorCallingEntry;
import com.myntra.rightnow.crm.client.entry.RightnowTaskEntry;
import com.myntra.rightnow.crm.client.entry.RightnowTaskOrderEntry;
import com.myntra.rightnow.crm.client.entry.RightnowTaskUDCallingEntry;
import com.myntra.rightnow.crm.client.entry.RightnowTaskWelcomeCallingEntry;
import com.myntra.rightnow.crm.client.response.RightnowBaseTaskResponse;
import com.myntra.rightnow.crm.client.response.RightnowTaskResponse;
import com.myntra.rightnow.crm.constants.RightnowConstants;
import com.myntra.rightnow.crm.constants.RightnowReportColumnConstants;
import com.myntra.rightnow.crm.manager.RightnowProactiveTaskManager;
import com.myntra.rightnow.crm.utils.GetOrderDetails;
import com.myntra.rightnow.crm.utils.RightnowConnectUtility;
import com.myntra.rightnow.crm.utils.TaskyUtils;
import com.rightnow.ws.base.ChainDestinationID;
import com.rightnow.ws.base.ChainSourceID;
import com.rightnow.ws.base.ID;
import com.rightnow.ws.base.NamedID;
import com.rightnow.ws.base.NamedIDHierarchy;
import com.rightnow.ws.base.RNObject;
import com.rightnow.ws.generic.GenericField;
import com.rightnow.ws.generic.GenericObject;
import com.rightnow.ws.generic.RNObjectType;
import com.rightnow.ws.messages.BatchRequestItem;
import com.rightnow.ws.messages.BatchResponseItem;
import com.rightnow.ws.messages.ClientInfoHeader;
import com.rightnow.ws.messages.CreateResponseMsg;
import com.rightnow.ws.objects.Contact;
import com.rightnow.ws.objects.Incident;
import com.rightnow.ws.objects.IncidentContact;
import com.rightnow.ws.objects.Task;
import com.rightnow.ws.objects.TaskServiceSettings;
import com.rightnow.ws.wsdl.RequestErrorFault;
import com.rightnow.ws.wsdl.ServerErrorFault;
import com.rightnow.ws.wsdl.UnexpectedErrorFault;

/**
 * @author preetam
 * 
 */
public class RightnowProactiveTaskManagerImpl extends BaseManagerImpl<RightnowTaskResponse, RightnowTaskEntry>
		implements RightnowProactiveTaskManager {

	private static final Logger LOGGER = Logger.getLogger(RightnowProactiveTaskManagerImpl.class);

	private static HashMap<String, Long> categoryMap;
	private ChainSourceID chainIncidentID;
	private String contactEmail;
	private ChainSourceID chainProactiveOBID;
	private Map<Long, CustomerOrderEntry> orderMap = null;
	private Map<String, Long> contactMap = null;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.myntra.taskwebapp.crm.manager.RightnowProactiveTaskManager#createTask
	 * (com.myntra.taskwebapp.crm.client.entry.RightnowTaskListEntry)
	 */
	@Override
	public RightnowBaseTaskResponse createTasks(List<RightnowBaseTaskEntry> taskList) throws ERPServiceException {
		List<Task> taskObjectList = new ArrayList<Task>();
		List<Incident> incidentObjectList = new ArrayList<Incident>();
		List<GenericObject> proactiveOBList = new ArrayList<GenericObject>();
		// List<Contact> contactObjectList = new ArrayList<Contact>();
		List<RightnowBaseTaskEntry> failedTasks = new ArrayList<RightnowBaseTaskEntry>();

		RightnowBaseTaskResponse response = new RightnowBaseTaskResponse();
		response.setRightnowBaseTaskEntryList(null);

		ClientInfoHeader clientInfoHeader = new ClientInfoHeader();
		clientInfoHeader.setAppID("Chainer");
		try {

			// get order details
			getCustomerOrderDetails(taskList);

			// loop through each task to get order id and details and create a
			// list of task object
			int count = 0;
			for (RightnowBaseTaskEntry baseTaskEntry : taskList) {				
				RightnowTaskOrderEntry taskEntry = null;
				String udReason = null;
				Date udQueuedDate = null;
				Task task = new Task();
				//check whether the task is for UD/Detractor/Welcome Calling
				if(baseTaskEntry instanceof RightnowTaskUDCallingEntry) {
					RightnowTaskUDCallingEntry udEntry = (RightnowTaskUDCallingEntry) baseTaskEntry;
					udReason = udEntry.getUdReason();
					udQueuedDate = udEntry.getUdQueuedDate();
					taskEntry = udEntry;
				} else if(baseTaskEntry instanceof RightnowTaskDetractorCallingEntry){
					taskEntry = (RightnowTaskDetractorCallingEntry) baseTaskEntry;
					task.setDueTime(TaskyUtils.getCurrentTimeForRightnow());
				} else {
					taskEntry = (RightnowTaskWelcomeCallingEntry) baseTaskEntry;
					task.setDueTime(TaskyUtils.getCurrentTimeForRightnow());
				}
				
				long orderId = taskEntry.getOrderId();
				
				String taskName = taskEntry.getMainCategory() + ":" + taskEntry.getSubCategory() + ":"
						+ taskEntry.getSubSubCategory();
				TaskyUtils.addNoteToTask(task, taskEntry.getComments());
				setTaskPriority(task);
				GenericObject proactiveOB = createProactiveOB(baseTaskEntry, orderId, count);
				boolean success = setCustomFieldsForTask(task, taskName, orderId, udReason, udQueuedDate);

				if (success) {
					// set name
					task.setName(taskName);

					// create an incident for the task
					Incident incident = createIncidentForTask(baseTaskEntry, contactEmail, Long.toString(orderId), count);					

					if (incident == null) {
						failedTasks.add(taskEntry);
					} else {
						//set queue on Incident if the task is of type welcome calling
						if(baseTaskEntry instanceof RightnowTaskWelcomeCallingEntry) {
							NamedID queueID = new NamedID();
							queueID.setName("OB Welcome");
							incident.setQueue(queueID);
						}
						
						// add the incident to the task
						NamedID incidentNamedID = new NamedID();
						ChainDestinationID chainDestId = new ChainDestinationID();
						chainDestId.setVariableName(chainIncidentID.getVariableName());
						incidentNamedID.setID(chainDestId);
						TaskServiceSettings serviceSettings = new TaskServiceSettings();
						serviceSettings.setIncident(incidentNamedID);
						task.setServiceSettings(serviceSettings);
						
						//add contact to task
						task.setContact(incident.getPrimaryContact().getContact());
						

						taskObjectList.add(task);
						incidentObjectList.add(incident);
						if(proactiveOB != null)
							proactiveOBList.add(proactiveOB);
					}
				} else {
					failedTasks.add(taskEntry);
				}
				count++;
			}

			// if any of the task entries does not have right data, do not
			// create any tasks
			if (failedTasks.size() > 0) {
				response.setRightnowBaseTaskEntryList(failedTasks);
				StatusResponse error = new StatusResponse(RightnowErrorCodes.TASK_CREATION_ABORTED,
						StatusResponse.Type.SUCCESS, failedTasks.size());
				response.setStatus(error);
				return response;
			} else if (taskObjectList.size() > 0) {
				List<RNObject> objectList = new ArrayList<RNObject>();
				if(!proactiveOBList.isEmpty())
					objectList.addAll(proactiveOBList);
				objectList.addAll(incidentObjectList);
				objectList.addAll(taskObjectList);				
				RNObject[] objects = objectList.toArray(new RNObject[objectList.size()]);
				
				List<BatchRequestItem> batchRequestItems = new ArrayList<BatchRequestItem>();
				
				batchRequestItems.add(TaskyUtils.getBatchRequestItem(objects));

				BatchResponseItem[] responseItems = RightnowConnectUtility
						.getInstance()
						.getService()
						.batch(batchRequestItems.toArray(new BatchRequestItem[batchRequestItems.size()]),
								clientInfoHeader);

				String errorMessage = "";
				List<RightnowBaseTaskEntry> createdTasks = new ArrayList<RightnowBaseTaskEntry>();
				for (BatchResponseItem responseItem : responseItems) {
					if (responseItem.isCreateResponseMsgSpecified()) {
						CreateResponseMsg createResponseMsg = responseItem.getCreateResponseMsg();
						if (createResponseMsg != null) {
							RNObject[] createdObj = createResponseMsg.getRNObjectsResult().getRNObjects();
							for (RNObject object : createdObj) {
								String objectType = "GenericObject";
								if (object instanceof Contact) {
									objectType = "Contact";
								} else if (object instanceof Incident) {
									objectType = "Incident";
								} else if (object instanceof Task) {
									objectType = "Task";
									RightnowBaseTaskEntry taskEntry = new RightnowBaseTaskEntry();
									taskEntry.setId(object.getID().getId());
									createdTasks.add(taskEntry);
								}
								LOGGER.debug("New " + objectType + " has ID: " + object.getID().getId());
								//System.out.println("New " + objectType + " has ID: " + object.getID().getId());
							}
						}
					} else if (responseItem.isRequestErrorFaultSpecified()) {
						LOGGER.error(responseItem.getRequestErrorFault().getExceptionMessage());
						errorMessage += responseItem.getRequestErrorFault().getExceptionMessage();
					}
				}

				if (!errorMessage.isEmpty()) {
					StatusResponse error = new StatusResponse(RightnowErrorCodes.NO_TASK_SAVED_IN_RIGHTNOW,
							StatusResponse.Type.SUCCESS, 0);
					error.setStatusMessage(errorMessage);
					response.setStatus(error);
					return response;
				} else {
					StatusResponse success = new StatusResponse(RightnowSuccessCodes.RIGHTNOW_TASKS_SAVED,
							StatusResponse.Type.SUCCESS, taskList.size());
					response.setStatus(success);
					response.setRightnowBaseTaskEntryList(createdTasks);
					return response;
				}
			}

			return response;

		} catch (RemoteException e) {
			LOGGER.error("RemoteException while updating Tasks: " + e.getMessage(), e);
			throw Misc.createERPServiceException(RightnowErrorCodes.NO_TASK_FETCHED, new String[] { e.getMessage() });
		} catch (ServerErrorFault e) {
			LOGGER.error("ServerErrorFault while updating Tasks: " + e.getMessage(), e);
			throw Misc.createERPServiceException(RightnowErrorCodes.NO_TASK_FETCHED, new String[] { e.getMessage() });
		} catch (RequestErrorFault e) {
			e.printStackTrace();
			LOGGER.error("RequestErrorFault while updating Tasks: " + e.getMessage(), e);
			throw Misc.createERPServiceException(RightnowErrorCodes.NO_TASK_FETCHED, new String[] { e.getMessage() });
		} catch (UnexpectedErrorFault e) {
			LOGGER.error("UnexpectedErrorFault while updating Tasks: " + e.getMessage(), e);
			throw Misc.createERPServiceException(RightnowErrorCodes.NO_TASK_FETCHED, new String[] { e.getMessage() });
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error("Exception while updating Tasks: " + e.getMessage(), e);
			throw Misc.createERPServiceException(RightnowErrorCodes.NO_TASK_FETCHED, new String[] { e.getMessage() });
		}
	}

	private void getCustomerOrderDetails(List<RightnowBaseTaskEntry> taskList) {
		List<String> contactEmails = new ArrayList<String>();
		List<Long> orderIdList = new ArrayList<Long>();
		List<Long> releaseIdList = new ArrayList<Long>();
		orderMap = new HashMap<Long, CustomerOrderEntry>();
		
		//store all the order ids in a list
		for (RightnowBaseTaskEntry baseTaskEntry : taskList) {
			RightnowTaskOrderEntry taskEntry = (RightnowTaskOrderEntry) baseTaskEntry;
			orderIdList.add(taskEntry.getOrderId());
		}		
		
		try {
			fetchOrderDetails(orderIdList, GetOrderDetails.ORDER, 100);
			
			//check for how many order ids we did not get any response. all such order ids are actually release id (shipment id).
			//call the order release service to fetch their details
			for(Long orderId : orderIdList) {
				if(orderMap.get(orderId) == null) {
					releaseIdList.add(orderId);
				}
			}
			
			fetchOrderDetails(releaseIdList, GetOrderDetails.RELEASE, 1);
			
			//loop through the order map get the email id associated with each order entry
			Iterator<Long> itr = orderMap.keySet().iterator();
			while (itr.hasNext()) {
				contactEmails.add(orderMap.get(itr.next()).getLogin());
			}
			
			if(!contactEmails.isEmpty()) {
				String inputEmails = TaskyUtils.convertListToString(contactEmails);
				getContactIDByEmailFromReport(inputEmails);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void fetchOrderDetails(List<Long> idList, String type, int batchSize) {
		//int batchSize = 100;
		
		//if no ids are there, no need of further processing
		if(idList == null || idList.size() == 0)
			return;
		
		//calculate how many batches of  ids we have to create
		int batchCount = (idList.size() + batchSize - 1) / batchSize;
		
		String[] orderIdArr = new String[batchCount];
		
		try {
			// loop through task list to create comma separated order ids
			// create a lot of 100 order ids
			int i = 0;
			for (Long id : idList) {
				
				if (orderIdArr[i/batchSize] == null)
					orderIdArr[i/batchSize] = "" + id;
				else
				    orderIdArr[i/batchSize] += "," + id;

				i++;
			}
			
			//create future tasks to fetch order/release details batch wise
			List<FutureTask<Map<Long, CustomerOrderEntry>>> futureTaskList = new ArrayList<FutureTask<Map<Long, CustomerOrderEntry>>>();
			for (String orderIds : orderIdArr) {
				if (orderIds != null && !orderIds.isEmpty()) {
					GetOrderDetails orderDetails = new GetOrderDetails(orderIds, type);
					FutureTask<Map<Long, CustomerOrderEntry>> futureTask = new FutureTask<Map<Long, CustomerOrderEntry>>(
							orderDetails);
					futureTaskList.add(futureTask);
				}
			}
			ExecutorService executor = Executors.newFixedThreadPool(futureTaskList.size());
			for (i = 0; i < futureTaskList.size(); i++) {
				executor.execute(futureTaskList.get(i));
			}

			// TODO need to look at the logic here to avoid infinite loop
			try {
				for (FutureTask<Map<Long, CustomerOrderEntry>> task : futureTaskList) {
					orderMap.putAll(task.get());
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			} catch(ExecutionException e) {
				e.printStackTrace();
			}
			
			executor.shutdown();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void getContactIDByEmailFromReport(String inputEmails) throws ERPServiceException {
		contactMap = new HashMap<String, Long>();

		// create filters
		List<RightnowReportFilterEntry> filterList = new ArrayList<RightnowReportFilterEntry>();
		RightnowReportFilterEntry filter = new RightnowReportFilterEntry();
		filter.setProperty("Email ID");
		filter.setValues(new String[] { inputEmails });
		filter.setDataType(5);
		filter.setOperator(10);
		filterList.add(filter);

		//String[] rowData = TaskyUtils.getReportData("engg_contact_email", filterList);
		RightnowReportData reportData = TaskyUtils.getReportData("engg_contact_email", filterList);
		int contactIdIndex = reportData.getIndexOfColumn(RightnowReportColumnConstants.CONTACT_ID_COLUMN);
		int emailAddrIndex = reportData.getIndexOfColumn(RightnowReportColumnConstants.EMAIL_ADDRESS_COLUMN);
		String[] rowData = reportData.getRowData();
		if (rowData != null && contactIdIndex > -1 && emailAddrIndex > -1) {
			for (String row : rowData) {
				String[] data = row.split(RightnowConstants.RIGHTNOW_REPORT_DELIMITER_SPLIT_ESCAPE, -1);
				if (data != null) {
					Long id = Long.parseLong(data[contactIdIndex]);
					String email = data[emailAddrIndex] == null ? "" : data[emailAddrIndex];
					//System.out.println(email + " --->>> " + id);
					contactMap.put(email.toLowerCase(), id);
				}
			}
		}
	}

	private boolean setCustomFieldsForTask(Task task, String taskName, long orderId, String reason, Date queuedDate) throws ERPServiceException {
		List<GenericField> genericFieldList = new ArrayList<GenericField>();

		// set the custom fields for task
		GenericObject customField = createCustomFieldsFromOrderDetails(orderId, taskName, reason, queuedDate);
		if (customField == null)
			return false;
		genericFieldList.add(TaskyUtils.addCustomFields("c", customField, false, false));

		// set the custom objects for task
		genericFieldList.add(TaskyUtils.addCustomFields("CO", createCustomCategory(taskName), false, false));

		// add the custom array to the task object
		GenericField[] genericFieldsArray = genericFieldList.toArray(new GenericField[genericFieldList.size()]);
		task.setCustomFields(TaskyUtils.createCustomObject(genericFieldsArray, "TaskCustomFields"));
		return true;
	}

	private GenericObject createCustomCategory(String taskName) throws ERPServiceException {
		List<GenericField> genericFields = new ArrayList<GenericField>();
		genericFields.add(TaskyUtils.addCustomFields("Category", getCategoriesForTasks().get(taskName.toLowerCase()),
				true, false));
		
		if(taskName.toLowerCase().contains("ud calling") || taskName.toLowerCase().contains("welcome calling")) {
			ChainDestinationID chainDestId = new ChainDestinationID();
			chainDestId.setVariableName(chainProactiveOBID.getVariableName());
			genericFields.add(TaskyUtils.addCustomFields("ProactiveOB", chainDestId, true, false));
		}
		
		GenericObject genericCustomCategory = TaskyUtils.createCustomObject(genericFields.toArray(new GenericField[genericFields.size()]), "TaskCustomFieldsco");		
		return genericCustomCategory;
	}
	
	private GenericObject createProactiveOB(RightnowBaseTaskEntry baseTaskEntry, long orderId, int count) {
		GenericObject go = null;
		if(baseTaskEntry != null) {
			go = new GenericObject();
			RNObjectType ot = new RNObjectType();
			ot.setNamespace("CO");
			ot.setTypeName("ProactiveOB");
			go.setObjectType(ot);
			
			chainProactiveOBID = new ChainSourceID();
			chainProactiveOBID.setVariableName("NewProactiveOBFor-" + orderId + "-" + count);
			go.setID(chainProactiveOBID);
			
			//if UD calling, set UD Queued Date
			if(baseTaskEntry instanceof RightnowTaskUDCallingEntry) {
				RightnowTaskUDCallingEntry udEntry = (RightnowTaskUDCallingEntry) baseTaskEntry;
				GenericField[] pbGF = new GenericField[1];
				pbGF[0] = TaskyUtils.addCustomFields("UDQueuedDate", udEntry.getUdQueuedDate(), false, false);
				go.setGenericFields(pbGF);
			} else {//if Welcome calling		
				GenericField[] pbGF = new GenericField[2];
				pbGF[0] = TaskyUtils.addCustomFields("UDQueuedDate", new Date(), false, false);//dummy field. we need a Proactive OB blank object
				go.setGenericFields(pbGF);
			}
		}
		return go;
	}

	private GenericObject createCustomFieldsFromOrderDetails(long orderId, String taskName, String reason, Date queuedDate) {
		CustomerOrderEntry orderEntry = orderMap.get(new Long(orderId));
		boolean isReleaseId = false;
		if (orderEntry != null) {
			String warehouse = "";
			String courierPartnerName = "";
			Date shippedDate = null;
			String trackingNumber = "";
			String orderStatus = "";
			String shippingPhoneNumber = "";
			contactEmail = orderEntry.getLogin();
			if(orderEntry.getOrderId() != orderId)
				isReleaseId = true;
			for (CustomerOrderShipmentEntry shipmentEntry : orderEntry.getOrderShipments()) {
				orderStatus = shipmentEntry.getStatusDisplayName();
				warehouse = shipmentEntry.getWarehouseName();
				courierPartnerName = shipmentEntry.getCourierDisplayName();
				shippedDate = shipmentEntry.getShippedOn();
				trackingNumber = shipmentEntry.getTrackingNo();
				shippingPhoneNumber = shipmentEntry.getMobile();
				if(shippingPhoneNumber!=null && shippingPhoneNumber.length()>15) {
					if(shippingPhoneNumber.startsWith("&#x2b;")) {
						shippingPhoneNumber = StringEscapeUtils.unescapeHtml(shippingPhoneNumber);
						LOGGER.debug("ShippingPhoneErrorFixed " + shippingPhoneNumber + " .. shipmentID: " + shipmentEntry.getShipmentId());
					}
				}
				if(isReleaseId && shipmentEntry.getShipmentId() == orderId)
					break;
			}

			List<GenericField> genericFieldList = new ArrayList<GenericField>();

			// set order id
			genericFieldList.add(TaskyUtils.addCustomFields("order_id", Long.toString(orderId), false, false));
			
			if(taskName.toLowerCase().contains("ud calling")) {//UD Calling
				// set tracking number
				genericFieldList.add(TaskyUtils.addCustomFields("tracking_number", trackingNumber, false, false));
				// set order status
				genericFieldList.add(TaskyUtils.addCustomFields("order_status", orderStatus, false, false));
				// set payment method
				genericFieldList.add(TaskyUtils.addCustomFields("payment_method", orderEntry.getPaymentMethod(), false, false));
				// set warehouse
				genericFieldList.add(TaskyUtils.addCustomFields("wh", warehouse, false, false));
				// set order created date
				genericFieldList.add(TaskyUtils.addCustomFields("order_date", orderEntry.getCreatedOn(), false, true));
				// set courier partner name
				genericFieldList.add(TaskyUtils.addCustomFields("courier_partner", courierPartnerName, false, false));
				// set shipped date
				// TODO
			} else if(taskName.toLowerCase().contains("detractor calling")) {//Detractor Calling
				//set email
				genericFieldList.add(TaskyUtils.addCustomFields("user_login_id", contactEmail, false, false));
				//set shipping phone number
				genericFieldList.add(TaskyUtils.addCustomFields("shipping_phone_number", shippingPhoneNumber, false, false));
				//set TAT
				genericFieldList.add(TaskyUtils.addCustomFields("tat", "1 Business Day", true, false));
			} else {//Welcome Calling
				//set email
				genericFieldList.add(TaskyUtils.addCustomFields("user_login_id", contactEmail, false, false));
				//set shipping phone number
				genericFieldList.add(TaskyUtils.addCustomFields("shipping_phone_number", shippingPhoneNumber, false, false));
				//set TAT
				genericFieldList.add(TaskyUtils.addCustomFields("tat", "2 Hours", true, false));
			}
			// set task status to open
			genericFieldList.add(TaskyUtils.addCustomFields("sr_status", "Open", true, false));
			// set department
			genericFieldList.add(TaskyUtils.addCustomFields("department",
					TaskyUtils.getDepartmentFromCategory(taskName.toLowerCase()), true, false));
			//set UD Reason
			if(reason != null && !reason.isEmpty()) {
				genericFieldList.add(TaskyUtils.addCustomFields("reason", reason, false, false));
			}
			

			GenericField[] genericCustomFieldsArray = genericFieldList
					.toArray(new GenericField[genericFieldList.size()]);
			GenericObject genericCustomObject = TaskyUtils.createCustomObject(genericCustomFieldsArray,
					"TaskCustomFieldsc");
			return genericCustomObject;
		}

		return null;
	}

	private Incident createIncidentForTask(RightnowBaseTaskEntry baseTaskEntry, String login, String orderId, int count) {
		Incident incident = new Incident();

		// create a chain source id for the incident
		chainIncidentID = new ChainSourceID();
		chainIncidentID.setVariableName("NewIncidentFor-" + orderId + "-" + count);
		incident.setID(chainIncidentID);
		
		// set primary contact for the incident
		// Create an IncidentContact to add as the Primary Contact on the new
		// Incident
		IncidentContact incContact = new IncidentContact();
		NamedID contactNamedID = new NamedID();

		Long contactId = contactMap.get(login.toLowerCase());// findRightnowContact(login);
		// if contact does not exist (creating new contact by chaining), set the
		// chain destination contact id into the incident
		if (contactId == null) {
			return null;
		} else {
			ID contactID = new ID();
			contactID.setId(contactId.longValue());
			contactNamedID.setID(contactID);
		}
		incContact.setContact(contactNamedID);
		incident.setPrimaryContact(incContact);

		//set category for incident
		NamedIDHierarchy catNamedIdH = new NamedIDHierarchy();
		catNamedIdH.setName("OB");
		incident.setCategory(catNamedIdH);
		
		//GenericField[] genericCustomFieldsArray = new GenericField[1];
		List<GenericField> genericCustomFieldList = new ArrayList<GenericField>();
		//genericCustomFieldsArray[0] = TaskyUtils.addCustomFields("orderid", orderId, false, false);
		genericCustomFieldList.add(TaskyUtils.addCustomFields("orderid", orderId, false, false));
		if(baseTaskEntry instanceof RightnowTaskWelcomeCallingEntry) {
			genericCustomFieldList.add(TaskyUtils.addCustomFields("cust_last_updated", TaskyUtils.getCurrentDateTimeForRightnow(), false, true));
		}
		//GenericObject genericCustomObject = TaskyUtils.createCustomObject(genericCustomFieldsArray, "IncidentCustomFieldsc");
		GenericObject genericCustomObject = TaskyUtils.createCustomObject(genericCustomFieldList.toArray(new GenericField[genericCustomFieldList.size()]),
				"IncidentCustomFieldsc");

		GenericField[] genericFieldParents = new GenericField[1];
		genericFieldParents[0] = TaskyUtils.addCustomFields("c", genericCustomObject, false, false);
		incident.setCustomFields(TaskyUtils.createCustomObject(genericFieldParents, "IncidentCustomField"));
		return incident;
	}

	public static HashMap<String, Long> getCategoriesForTasks() throws ERPServiceException {
		if (categoryMap != null)
			return categoryMap;
		RightnowReportData reportData = TaskyUtils.getReportData("engg_category", null);
		int categoryIndex = reportData.getIndexOfColumn(RightnowReportColumnConstants.CATEGORY_COLUMN);
		int catLvl1Index = reportData.getIndexOfColumn(RightnowReportColumnConstants.CATEGORY_LEVEL_1_COLUMN);
		int catLvl2Index = reportData.getIndexOfColumn(RightnowReportColumnConstants.CATEGORY_LEVEL_2_COLUMN);
		int catLvl3Index = reportData.getIndexOfColumn(RightnowReportColumnConstants.CATEGORY_LEVEL_3_COLUMN);
		String[] rowData = reportData.getRowData();
		if (rowData != null && categoryIndex > -1 && catLvl1Index > -1 && catLvl2Index > -1 && catLvl3Index > -1) {
			categoryMap = new HashMap<String, Long>();
			for (String row : rowData) {
				String[] data = row.split(RightnowConstants.RIGHTNOW_REPORT_DELIMITER_SPLIT_ESCAPE, -1);
				if (data != null) {
					Long id = Long.parseLong(data[categoryIndex]);
					String category = data[catLvl1Index] == null ? "" : data[catLvl1Index];
					if (data[catLvl2Index] != null && !data[catLvl2Index].isEmpty())
						category += ":" + data[catLvl2Index];
					if (data[catLvl3Index] != null && !data[catLvl3Index].isEmpty())
						category += ":" + data[catLvl3Index];
					categoryMap.put(category.toLowerCase(), id);
				}
			}
		}
		return categoryMap;
	}
	
	private void setTaskPriority(Task task) {
		//set task priority to low
		NamedID priorityNamedId = new NamedID();
		priorityNamedId.setName("Low");
		task.setPriority(priorityNamedId);
	}

	public static void main(String args[]) throws ERPServiceException {
		RightnowProactiveTaskManagerImpl rtml = new RightnowProactiveTaskManagerImpl();
		RightnowBaseTaskListEntry taskListEntry = new RightnowBaseTaskListEntry();
		List<RightnowBaseTaskEntry> taskEntries = new ArrayList<RightnowBaseTaskEntry>();
		Long[] ordrArray = new Long[] //{9645600L,9317234L,9277342L,9205334L,9200330L,9199374L,9034719L,8722490L,8580083L,8474884L};
		//{8474789L,8450280L,7855146L,7828922L,7761322L,7761193L,7719712L,7714648L,7642816L,7459481L};//
		//{7443429L,7401333L,7388108L,7371672L,7371085L,7370329L,7111336L,7080252L,7063824L,7063636L};
		//{6843673L,6488853L,6488378L,6487861L,10769079L,8778012L,8711461L,8664223L};//6417361L,5403412L,
		//{8559324L,8531736L,8474850L,8054703L,7796800L,7676865L,7676278L,7405782L,7310470L,7081010L};
				//{12460286L};
				{11441644L};
		for (int i = 0; i < ordrArray.length; i++) {
			//long orderId = 12201211;//9307330 
			RightnowTaskWelcomeCallingEntry taskEntry = new RightnowTaskWelcomeCallingEntry();
			taskEntry.setOrderId(ordrArray[i]);
			
			// welcome calling params
			//taskEntry.setMainCategory("Proactive OB");
			//taskEntry.setSubCategory("Welcome Calling");
			//taskEntry.setSubSubCategory("First Time Customer");
			
			// detractor calling params
			taskEntry.setMainCategory("Proactive OB");
			taskEntry.setSubCategory("Detractor Calling");			
			taskEntry.setSubSubCategory("Dissatisfied Customer");

			//taskEntry.setComments("Test notes welcome calling " + i);
			taskEntry.setComments("Jo dikta he wo nahi milta" + i);			

			//taskEntry.setUdReason("Sample Reason");
			//taskEntry.setUdQueuedDate(new Date());
			taskEntries.add(taskEntry);			
		}
		taskListEntry.setTaskEntryList(taskEntries);		
		rtml.createTasks(taskEntries);
	}

	@Override
	public AbstractResponse findById(Long id) throws ERPServiceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public AbstractResponse create(RightnowTaskEntry entry) throws ERPServiceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public AbstractResponse update(RightnowTaskEntry entry, Long itemId) throws ERPServiceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public AbstractResponse search(int start, int fetchSize, String sortBy, String sortOrder, String searchTerms)
			throws ERPServiceException {
		// TODO Auto-generated method stub
		return null;
	}

}
