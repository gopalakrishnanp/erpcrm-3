package com.myntra.drishti.crm.constants;

public class DrishtiContants {

	public static final String BEING_PROCESSED = "BEING_PROCESSED";
	public static final String SHIPPED = "SHIPPED";
	public static final String DELIVERED = "DELIVERED";
	
	public static final String OUT_FOR_DELIVERY = "OUT_FOR_DELIVERY";
	
	public static final String CANCELLED = "CANCELLED";
	
	public static final String REDIRECT_CC = "REDIRECT_CC";
	
	public static enum ORDER_STATUS {
		Q, WP, RFR, PK, OH, PV, D, F, PP, L, SH, RTO, C, DL
	};
}
