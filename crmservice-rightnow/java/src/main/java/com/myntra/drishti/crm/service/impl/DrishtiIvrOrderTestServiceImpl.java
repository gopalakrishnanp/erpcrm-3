package com.myntra.drishti.crm.service.impl;

import org.springframework.transaction.TransactionSystemException;

import com.myntra.commons.exception.ERPServiceException;
import com.myntra.commons.response.AbstractResponse;
import com.myntra.commons.service.impl.BaseServiceImpl;
import com.myntra.drishti.crm.client.entry.DrishtiOrderEntry;
import com.myntra.drishti.crm.client.response.DrishtiIvrOrderTestResponse;
import com.myntra.drishti.crm.manager.DrishtiIvrOrderTestManager;
import com.myntra.drishti.crm.service.DrishtiIvrOrderTestService;

public class DrishtiIvrOrderTestServiceImpl extends
		BaseServiceImpl<DrishtiIvrOrderTestResponse, DrishtiOrderEntry> implements DrishtiIvrOrderTestService {

	@Override
	public AbstractResponse getIvrOrderTestDetailsForDrishti(int count, String date) throws ERPServiceException {
		try {
			return ((DrishtiIvrOrderTestManager) getManager()).getIvrOrderTestDetailsForDrishti(count, date);

		} catch (ERPServiceException e) {
			return e.getResponse();

		} catch (TransactionSystemException ex) {
			return ((ERPServiceException) ex.getApplicationException()).getResponse();
		}
	}
}
