package com.myntra.drishti.crm.service.impl;

import org.springframework.transaction.TransactionSystemException;

import com.myntra.commons.exception.ERPServiceException;
import com.myntra.commons.response.AbstractResponse;
import com.myntra.commons.service.impl.BaseServiceImpl;
import com.myntra.drishti.crm.client.entry.DrishtiShipmentEntry;
import com.myntra.drishti.crm.client.response.DrishtiOrderResponse;
import com.myntra.drishti.crm.manager.DrishtiOrderManager;
import com.myntra.drishti.crm.service.DrishtiOrderService;

public class DrishtiOrderServiceImpl extends BaseServiceImpl<DrishtiOrderResponse, DrishtiShipmentEntry> implements
		DrishtiOrderService {

	@Override
	public AbstractResponse getOrderDetailsForDrishti(long orderid) {
		try {
			return ((DrishtiOrderManager) getManager()).getOrderDetailsForDrishti(orderid);

		} catch (ERPServiceException e) {
			return e.getResponse();

		} catch (TransactionSystemException ex) {
			return ((ERPServiceException) ex.getApplicationException())
					.getResponse();
		}

	}

	@Override
	public AbstractResponse getLastNopenOrdersForDrishti(String userContactNo, int fetchSize)
			throws ERPServiceException {
		try {
			return ((DrishtiOrderManager) getManager()).getLastNopenOrdersForDrishti(userContactNo, fetchSize);

		} catch (ERPServiceException e) {
			return e.getResponse();

		} catch (TransactionSystemException ex) {
			return ((ERPServiceException) ex.getApplicationException())
					.getResponse();
		}
	}
}
