package com.myntra.rightnow.crm.client.entry;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import com.myntra.commons.entries.BaseEntry;

/**
 * @author Pravin Mehta
 */
@XmlRootElement(name = "rightnowReportRowEntry")
public class RightnowReportRowEntry extends BaseEntry {
	
	private static final long serialVersionUID = 1;
	
	private List<String> rightnowReportDataList;
	
	public RightnowReportRowEntry() {
	}

	@XmlElementWrapper(name = "RightnowReportDataList")
	@XmlElement(name = "RightnowReportDataEntry")
	public List<String> getRightnowReportDataList() {
		return rightnowReportDataList;
	}

	public void setRightnowReportDataList(List<String> rightnowReportDataList) {
		this.rightnowReportDataList = rightnowReportDataList;
	}
}
