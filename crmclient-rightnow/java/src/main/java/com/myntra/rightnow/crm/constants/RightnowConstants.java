package com.myntra.rightnow.crm.constants;

public final class RightnowConstants {
	
	public static final String CRM_RIGHTNOW_URL = "crmRightnowURL";
	
	public static final String TASK_STATUS_COLUMN = "Task Status";
	
	public static final String RIGHTNOW_USERNAME = "task.webapp";
	public static final String RIGHTNOW_PASSWORD = "tasky";
	
	public static final String RIGHTNOW_REPORT_DELIMITER = "|";
	public static final String RIGHTNOW_REPORT_DELIMITER_SPLIT_ESCAPE = "\\|";
	
	public static final String[] RIGHTNOW_REPORT_FILTERS = {"Courier Name", "Warehouse"};

}
