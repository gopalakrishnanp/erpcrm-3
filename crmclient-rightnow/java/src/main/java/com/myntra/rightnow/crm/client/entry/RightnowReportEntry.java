package com.myntra.rightnow.crm.client.entry;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import com.myntra.commons.entries.BaseEntry;

/**
 * @author Pravin Mehta
 */
@XmlRootElement(name = "rightnowReportEntry")
public class RightnowReportEntry extends BaseEntry {
	
	private static final long serialVersionUID = 1;
	
	private List<RightnowReportColumnEntry> rightnowReportColumnEntryList;
	private List<RightnowReportRowEntry> rightnowReportRowEntryList;
	
	public RightnowReportEntry() {
	}

	@XmlElementWrapper(name = "RightnowReportColumnEntryList")
	@XmlElement(name = "RightnowReportColumnEntry")
	public List<RightnowReportColumnEntry> getRightnowReportColumnEntryList() {
		return rightnowReportColumnEntryList;
	}

	public void setRightnowReportColumn(List<RightnowReportColumnEntry> rightnowReportColumnEntryList) {
		this.rightnowReportColumnEntryList = rightnowReportColumnEntryList;
	}

	@XmlElementWrapper(name = "RightnowReportRowEntryList")
	@XmlElement(name = "RightnowReportRowEntry")
	public List<RightnowReportRowEntry> getRightnowReportRowEntryList() {
		return rightnowReportRowEntryList;
	}

	public void setRightnowReportRowEntryList(List<RightnowReportRowEntry> rightnowReportRowEntryList) {
		this.rightnowReportRowEntryList = rightnowReportRowEntryList;
	}
}
