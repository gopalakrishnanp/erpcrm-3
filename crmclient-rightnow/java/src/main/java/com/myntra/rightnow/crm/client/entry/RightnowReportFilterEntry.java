package com.myntra.rightnow.crm.client.entry;

import java.util.Arrays;

import javax.xml.bind.annotation.XmlRootElement;

import com.myntra.commons.entries.BaseEntry;

@XmlRootElement(name = "rightnowReportFilterEntry")
public class RightnowReportFilterEntry extends BaseEntry {

	private static final long serialVersionUID = 1L;

	private String property;
	private String[] values;
	private long dataType;
	private long operator;

	public String getProperty() {
		return property;
	}

	public void setProperty(String property) {
		this.property = property;
	}

	public String[] getValues() {
		return values;
	}

	public void setValues(String[] values) {
		this.values = values;
	}

	public long getDataType() {
		return dataType;
	}

	public void setDataType(long dataType) {
		this.dataType = dataType;
	}

	public long getOperator() {
		return operator;
	}

	public void setOperator(long operator) {
		this.operator = operator;
	}

	@Override
	public String toString() {
		return "RightnowReportFilterEntry [property=" + property + ", values=" + Arrays.toString(values)
				+ ", dataType=" + dataType + ", operator=" + operator + "]";
	}

}
