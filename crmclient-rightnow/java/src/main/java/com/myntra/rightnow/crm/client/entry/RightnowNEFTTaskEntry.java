/**
 * 
 */
package com.myntra.rightnow.crm.client.entry;

/**
 * @author preetam
 *
 */
public class RightnowNEFTTaskEntry extends RightnowTaskOrderEntry {

	private static final long serialVersionUID = 1L;
	
	
	private String customerName;
	private String bankName;
	private String accountNo;
	private String ifscCode;
	private String accountType;
	private double amount;
	
	
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getBankName() {
		return bankName;
	}
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	public String getAccountNo() {
		return accountNo;
	}
	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}
	public String getIfscCode() {
		return ifscCode;
	}
	public void setIfscCode(String ifscCode) {
		this.ifscCode = ifscCode;
	}
	public String getAccountType() {
		return accountType;
	}
	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	
	
	@Override
	public String toString() {
		return "RightnowNEFTTaskEntry [customerName=" + customerName
				+ ", bankName=" + bankName + ", accountNo=" + accountNo
				+ ", ifscCode=" + ifscCode + ", accountType=" + accountType
				+ ", amount=" + amount + "]";
	}
	
	
	

}
