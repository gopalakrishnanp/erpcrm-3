package com.myntra.rightnow.crm.constants;

public class RightnowReportColumnConstants {
	
	//Common Columns
	public static final String TASK_ID_COLUMN = "Task ID";
	public static final String INCIDENT_ID_COLUMN = "Incident ID";

	//Report Name -->> engg_contact_email
	public static final String CONTACT_ID_COLUMN = "Contact ID";
	public static final String EMAIL_ADDRESS_COLUMN = "Email Address";
	
	
	//Report name -->> engg_category
	public static final String CATEGORY_COLUMN = "Category";
	public static final String CATEGORY_LEVEL_1_COLUMN = "Category Level 1";
	public static final String CATEGORY_LEVEL_2_COLUMN = "Category Level 2";
	public static final String CATEGORY_LEVEL_3_COLUMN = "Category Level 3";
	
	//Report name -->> closed_tasks
	public static final String TASK_STATUS_COLUMN = "Task Status";
	
	
	public static final String CATEGORY_ID_COLUMN = "Cat ID";
	
}
