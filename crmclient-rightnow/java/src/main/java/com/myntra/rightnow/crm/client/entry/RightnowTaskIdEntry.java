/**
 * 
 */
package com.myntra.rightnow.crm.client.entry;

import java.util.List;

import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author preetam
 * 
 */
@XmlRootElement(name = "rightnowTaskIdEntry")
public class RightnowTaskIdEntry extends RightnowBaseTaskEntry {

	private static final long serialVersionUID = 1L;

	private List<String> taskId;

	public RightnowTaskIdEntry() {
	}

	public RightnowTaskIdEntry(List<String> taskId) {
		this.taskId = taskId;
	}

	@XmlElementWrapper(name = "taskIds")
	public List<String> getTaskId() {
		return taskId;
	}

	public void setTaskId(List<String> taskId) {
		this.taskId = taskId;
	}

	@Override
	public String toString() {
		return "RightnowTaskIdEntry [taskId=" + taskId + "]";
	}

}
