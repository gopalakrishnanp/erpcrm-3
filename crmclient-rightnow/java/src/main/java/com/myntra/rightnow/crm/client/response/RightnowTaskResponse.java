package com.myntra.rightnow.crm.client.response;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import com.myntra.commons.response.AbstractResponse;
import com.myntra.rightnow.crm.client.entry.RightnowTaskEntry;
import com.myntra.rightnow.crm.client.entry.RightnowTaskListEntry;

/**
 * Response for Rightnow Task Service
 * 
 */
@XmlRootElement(name = "rightnowTaskResponse")
public class RightnowTaskResponse extends AbstractResponse {

	private static final long serialVersionUID = 1L;

	private List<RightnowTaskEntry> rightnowTaskEntryList;
	private RightnowTaskListEntry rightnowTaskListEntry;

	public RightnowTaskResponse() {
	}

	public RightnowTaskResponse(RightnowTaskListEntry rightnowTaskListEntry) {
		this.rightnowTaskListEntry = rightnowTaskListEntry;
	}

	@XmlElementWrapper(name = "rightnowTaskEntryList")
	@XmlElement(name = "rightnowTaskEntry")
	public List<RightnowTaskEntry> getRightnowTaskEntryList() {
		return rightnowTaskEntryList;
	}

	public void setRightnowTaskEntryList(List<RightnowTaskEntry> rightnowTaskEntryList) {
		this.rightnowTaskEntryList = rightnowTaskEntryList;
	}

	@XmlElement(name = "rightnowTaskListEntry")
	public RightnowTaskListEntry getRightnowTaskListEntry() {
		return rightnowTaskListEntry;
	}

	public void setRightnowTaskListEntry(RightnowTaskListEntry rightnowTaskListEntry) {
		this.rightnowTaskListEntry = rightnowTaskListEntry;
	}

	@Override
	public String toString() {
		return "RightnowTaskResponse [rightnowTaskEntryList=" + rightnowTaskEntryList + ", rightnowTaskListEntry="
				+ rightnowTaskListEntry + "]";
	}

}