package com.myntra.rightnow.crm.client.response;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import com.myntra.commons.response.AbstractResponse;
import com.myntra.rightnow.crm.client.entry.RightnowReportColumnEntry;
import com.myntra.rightnow.crm.client.entry.RightnowReportFilterEntry;
import com.myntra.rightnow.crm.client.entry.RightnowReportRowEntry;

/**
 * Response for Rightnow Task Service
 * 
 */
@XmlRootElement(name = "rightnowReportResponse")
public class RightnowReportResponse extends AbstractResponse {

	private static final long serialVersionUID = 1L;

	private List<RightnowReportColumnEntry> rightnowReportColumnEntryList;
	private List<RightnowReportRowEntry> rightnowReportRowEntryList;
	private List<RightnowReportFilterEntry> rightnowReportFilterEntryList;

	public RightnowReportResponse() {
	}

	public RightnowReportResponse(List<RightnowReportFilterEntry> rightnowReportFilterEntryList) {
		this.rightnowReportFilterEntryList = rightnowReportFilterEntryList;
	}

	public void setRightnowReportColumnEntryList(List<RightnowReportColumnEntry> rightnowReportColumnEntryList) {
		this.rightnowReportColumnEntryList = rightnowReportColumnEntryList;
	}

	@XmlElementWrapper(name = "RightnowReportRowList")
	@XmlElement(name = "RightnowReportRow")
	public List<RightnowReportRowEntry> getRightnowReportRowEntryList() {
		return rightnowReportRowEntryList;
	}

	@XmlElementWrapper(name = "RightnowReportColumnList")
	@XmlElement(name = "RightnowReportColumn")
	public List<RightnowReportColumnEntry> getRightnowReportColumnEntryList() {
		return rightnowReportColumnEntryList;
	}

	public void setRightnowReportRowEntryList(List<RightnowReportRowEntry> rightnowReportRowEntryList) {
		this.rightnowReportRowEntryList = rightnowReportRowEntryList;
	}

	// either xml element should be similar to entry xml root element or should
	// not exist here
	@XmlElementWrapper(name = "RightnowReportFilterList")
	@XmlElement(name = "rightnowReportFilterEntry")
	public List<RightnowReportFilterEntry> getRightnowReportFilterEntryList() {
		return rightnowReportFilterEntryList;
	}

	public void setRightnowReportFilterEntryList(List<RightnowReportFilterEntry> rightnowReportFilterEntryList) {
		this.rightnowReportFilterEntryList = rightnowReportFilterEntryList;
	}

	@Override
	public String toString() {
		return "RightnowReportResponse [rightnowReportColumnEntryList=" + rightnowReportColumnEntryList
				+ ", rightnowReportRowEntryList=" + rightnowReportRowEntryList + ", rightnowReportFilterEntryList="
				+ rightnowReportFilterEntryList + "]";
	}
}