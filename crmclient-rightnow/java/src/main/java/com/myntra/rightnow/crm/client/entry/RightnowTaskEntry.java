package com.myntra.rightnow.crm.client.entry;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import com.myntra.commons.entries.BaseEntry;

@XmlRootElement(name = "rightnowTaskEntry")
public class RightnowTaskEntry extends BaseEntry{

	private static final long serialVersionUID = 1L;
	
	//task fields
	private String note;
	private String filePath;
	private String fileName;
	private String contentType;
	private String srStatus;
	
	//incident
	private Long incidentId;
	
	//order
	private long orderId;
	
	//categories
	private String mainCategory;
	private String subCategory;
	private String subSubCategory;
	private Long categoryId;
	
	private List<RightnowNoteEntry> noteList;
	private List<RightnowFileAttachmentEntry> fileList;

	@XmlElementWrapper(name="rightnowNoteList")
	@XmlElement(name = "rightnowNoteEntry")
	public List<RightnowNoteEntry> getNoteList() {
		return noteList;
	}

	public void setNoteList(List<RightnowNoteEntry> noteList) {
		this.noteList = noteList;
	}
	
	@XmlElementWrapper(name="rightnowFileList")
	@XmlElement(name="rightnowFileAttachmentEntry")
	public List<RightnowFileAttachmentEntry> getFileList() {
		return fileList;
	}
	
	public void setFileList(List<RightnowFileAttachmentEntry> fileList) {
		this.fileList = fileList;
	}

	
	public RightnowTaskEntry() {
		// TODO Auto-generated constructor stub
	}
	
	public String getNote() {
		return note;
	}
	
	public void setNote(String note) {
		this.note = note;
	}
	
	public String getContentType() {
		return contentType;
	}
	
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}
	
	public String getSrStatus() {
		return srStatus;
	}
	
	public void setSrStatus(String srStatus) {
		this.srStatus = srStatus;
	}
	
	public long getOrderId() {
		return orderId;
	}
	
	public void setOrderId(long orderId) {
		this.orderId = orderId;
	}		

	public String getMainCategory() {
		return mainCategory;
	}

	public void setMainCategory(String mainCategory) {
		this.mainCategory = mainCategory;
	}

	public String getSubCategory() {
		return subCategory;
	}

	public void setSubCategory(String subCategory) {
		this.subCategory = subCategory;
	}

	public String getSubSubCategory() {
		return subSubCategory;
	}

	public void setSubSubCategory(String subSubCategory) {
		this.subSubCategory = subSubCategory;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	
	public Long getIncidentId() {
		return incidentId;
	}
	
	public void setIncidentId(Long incidentId) {
		this.incidentId = incidentId;
	}
	
	public Long getCategoryId() {
		return categoryId;
	}
	
	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}

	@Override
	public String toString() {
		return "RightnowTaskEntry [note=" + note + ", filePath=" + filePath
				+ ", fileName=" + fileName + ", contentType=" + contentType
				+ ", srStatus=" + srStatus + ", incidentId=" + incidentId
				+ ", orderId=" + orderId + ", mainCategory=" + mainCategory
				+ ", subCategory=" + subCategory + ", subSubCategory="
				+ subSubCategory + ", categoryId=" + categoryId + ", noteList="
				+ noteList + ", fileList=" + fileList + "]";
	}

	
}