package com.myntra.drishti.crm.client.response;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import com.myntra.commons.response.AbstractResponse;
import com.myntra.drishti.crm.client.entry.DrishtiShipmentEntry;

@XmlRootElement(name = "drishtiOrderResponse")
public class DrishtiOrderResponse extends AbstractResponse {

	private static final long serialVersionUID = 1L;
	int numberOfShipments;
	
	private String customerLoyaltyType; 
	private List<DrishtiShipmentEntry> drishtiOrderShipments;
	
	public String getCustomerLoyaltyType() {
		return customerLoyaltyType;
	}

	public void setCustomerLoyaltyType(String customerLoyaltyType) {
		this.customerLoyaltyType = customerLoyaltyType;
	}

	@XmlElementWrapper(name = "drishtiOrderShipments")
	@XmlElement(name = "drishtiOrderShipment")
	public void setDrishtiOrderShipments(List<DrishtiShipmentEntry> drishtiShipmentEntryList) {
		this.drishtiOrderShipments = drishtiShipmentEntryList;
	}
	
	public List<DrishtiShipmentEntry> getDrishtiOrderShipments() {
		return drishtiOrderShipments;
	}

	public int getNumberOfShipments() {
		return numberOfShipments;
	}

	public void setNumberOfShipments(int numberOfShipments) {
		this.numberOfShipments = numberOfShipments;
	}

}