/**
 *
 */
package com.myntra.crm.service.impl;

import com.myntra.crm.manager.BaseCRMManager;
import com.myntra.crm.service.BaseCRMService;
import org.apache.log4j.Logger;

/**
 * @author pravin 
 *
 * Base class to be extended by all SercviceImpls
 */
public class BaseCRMServiceImpl<R, E> implements BaseCRMService<R, E> {

    private static final Logger LOGGER = Logger.getLogger(BaseCRMServiceImpl.class);
    protected BaseCRMManager<R, E> manager;

    /**
     * @return the manager
     */
    public BaseCRMManager<R, E> getManager() {
        return manager;
    }

    /**
     * @param manager the manager to set
     */
    public void setManager(BaseCRMManager<R, E> manager) {
        this.manager = manager;
    }
}
