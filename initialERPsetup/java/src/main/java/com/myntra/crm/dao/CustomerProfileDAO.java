package com.myntra.crm.dao;

import com.myntra.commons.dao.BaseDAO;
import com.myntra.commons.exception.ERPServiceException;
import com.myntra.crm.entity.CustomerProfileEntity;
import java.util.List;

public interface CustomerProfileDAO extends BaseDAO<CustomerProfileEntity> {
    
    public List<CustomerProfileEntity> getCustomerProfileByLogin(String login) throws ERPServiceException;
    public boolean createCustomerProfile() throws ERPServiceException;
}
