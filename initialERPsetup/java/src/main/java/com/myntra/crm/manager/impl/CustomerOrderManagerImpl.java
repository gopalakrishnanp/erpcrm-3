package com.myntra.crm.manager.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.myntra.client.wms.SkuServiceClient;
import com.myntra.client.wms.location.WarehouseServiceClient;
import com.myntra.client.wms.response.SkuEntry;
import com.myntra.client.wms.response.SkuResponse;
import com.myntra.client.wms.response.location.WarehouseEntry;
import com.myntra.client.wms.response.location.WarehouseResponse;

import com.myntra.commons.codes.StatusResponse;
import com.myntra.commons.exception.ERPServiceException;
import com.myntra.commons.manager.impl.BaseManagerImpl;
import com.myntra.commons.response.AbstractResponse;
import com.myntra.commons.utils.Context;

import com.myntra.crm.client.codes.CRMErrorCodes;
import com.myntra.crm.client.codes.CRMSuccessCodes;
import com.myntra.crm.client.response.CustomerOrderResponse;

import com.myntra.crm.entry.CustomerOrderBillingAddressEntry;
import com.myntra.crm.entry.CustomerOrderEntry;
import com.myntra.crm.entry.CustomerOrderItemEntry;
import com.myntra.crm.entry.CustomerOrderShipmentEntry;
import com.myntra.crm.entry.CustomerOrderTrackingDetailEntry;

import com.myntra.crm.manager.CustomerOrderManager;

import com.myntra.lms.client.OrderTrackingServiceClient;
import com.myntra.lms.client.response.OrderTrackingDetailEntry;
import com.myntra.lms.client.response.OrderTrackingEntry;
import com.myntra.lms.client.response.OrderTrackingResponse;

import com.myntra.oms.client.OrderClient;
import com.myntra.oms.client.entry.BillingAddressEntry;
import com.myntra.oms.client.entry.OrderEntry;
import com.myntra.oms.client.entry.OrderLineEntry;
import com.myntra.oms.client.entry.OrderReleaseEntry;
import com.myntra.oms.client.response.OrderResponse;

/**
 * Manager implementation for customer order search web service which integrates
 * detail of order, shipments, skus, warehouse, tracking, trip detail etc..
 * 
 * @author Arun Kumar
 */
public class CustomerOrderManagerImpl extends
		BaseManagerImpl<CustomerOrderResponse, CustomerOrderEntry> implements
		CustomerOrderManager {
	
	private static final Logger LOGGER = Logger
			.getLogger(CustomerOrderManagerImpl.class);

	@Override
	public CustomerOrderResponse getOrderSummary(int start, int fetchSize,
			String sortBy, String sortOrder, String searchTerms)
			throws ERPServiceException {
		
		LOGGER.debug("Entering getOrderSummary in implementation");

		// Initialise customer order response and entry
		CustomerOrderResponse response = new CustomerOrderResponse();
		List<CustomerOrderEntry> custOrderEntryList = new ArrayList<CustomerOrderEntry>();

		try {

			// call OMS service to get order response
			OrderResponse orderResponse = OrderClient.search(start, fetchSize,
					sortBy, sortOrder, searchTerms, null,
					Context.getContextInfo());
			List<OrderEntry> orderEntryList = orderResponse.getData();

			// Set<Long> skuIdSet = new TreeSet<Long>();

			for (OrderEntry orderEntry : orderEntryList) {

				CustomerOrderEntry singleOrderEntry = new CustomerOrderEntry();

				singleOrderEntry.setOrderId(orderEntry.getId());
				singleOrderEntry.setLogin(orderEntry.getLogin());
				singleOrderEntry.setCustomerName(orderEntry.getCustomerName());
				singleOrderEntry.setUserContactNo(orderEntry.getUserContactNo());

				// add order billing detail to order
				CustomerOrderBillingAddressEntry customerBillingAddressEntry = getCustomerOrderBillingDetailEntry(orderEntry);
				singleOrderEntry.setBillingAddress(customerBillingAddressEntry);
				
				singleOrderEntry.setShippingCharge(orderEntry
						.getShippingCharge());

				List<CustomerOrderShipmentEntry> custOrderShipmentEntryList = new ArrayList<CustomerOrderShipmentEntry>();

				for (OrderReleaseEntry orderShipment : orderEntry
						.getOrderReleases()) {

					CustomerOrderShipmentEntry singleShipmentEntry = new CustomerOrderShipmentEntry();
					singleShipmentEntry.setShipmentId(orderShipment
							.getOrderId());
					singleShipmentEntry.setCourierCode(orderShipment
							.getCourierCode());
					singleShipmentEntry
							.setPackedOn(orderShipment.getPackedOn());
					singleShipmentEntry.setDeliveredOn(orderShipment
							.getDeliveredOn());
					singleShipmentEntry
							.setQuantity(orderShipment.getQuantity());					
					
					// add warehouse detail to shipment
					WarehouseEntry warehouseEntry = getWarehouseEntry(orderShipment);
					if(warehouseEntry != null){
						singleShipmentEntry.setWarehouseName(warehouseEntry
								.getName());
						singleShipmentEntry.setWarehouseAddress(warehouseEntry
								.getAddress());
					}
					

					// add items detail to shipment
					List<CustomerOrderItemEntry> custOrderItemEntryList = new ArrayList<CustomerOrderItemEntry>();

					for (OrderLineEntry orderItem : orderShipment
							.getOrderLines()) {

						CustomerOrderItemEntry singleItemEntry = new CustomerOrderItemEntry();
						singleItemEntry.setId(orderItem.getId());
						singleItemEntry.setSkuId(orderItem.getSkuId());
						singleItemEntry.setUnitPrice(orderItem.getUnitPrice());
						singleItemEntry.setStatus(orderItem.getStatus());
						singleItemEntry.setStatusDisplayName(orderItem
								.getStatusDisplayName());
						singleItemEntry.setQuantity(orderItem.getQuantity());

						// appent sku detail to item						
						SkuEntry skuEntry = getSKUEntry(orderItem);
						if(skuEntry != null){
							singleItemEntry.setSkuId(skuEntry.getId());
							singleItemEntry.setSize(skuEntry.getSize());
							singleItemEntry.setArticleTypeName(skuEntry
									.getArticleTypeName());
							singleItemEntry.setBrandName(skuEntry.getBrandName());	
						}												

						// skuIdSet.add(orderItem.getSkuId());						
						custOrderItemEntryList.add(singleItemEntry);
					}

					// set items list for shipment
					singleShipmentEntry.setOrderItems(custOrderItemEntryList);

					// add tracking detail to shipment					
					OrderTrackingEntry orderTrackingEntry = getOrderTrackingEntry(orderShipment);
					if(orderTrackingEntry != null){
						singleShipmentEntry.setDeliveryStatus(orderTrackingEntry
								.getDeliveryStatus());
						singleShipmentEntry.setFailedAttempts(orderTrackingEntry
								.getFailedAttempts());
						
						List<OrderTrackingDetailEntry> orderTrackingDetailEntryList = orderTrackingEntry
								.getOrderTrackingDetailEntry();
						List<CustomerOrderTrackingDetailEntry> customerOrderTrackingDetailEntryList = new ArrayList<CustomerOrderTrackingDetailEntry>();

						if(orderTrackingDetailEntryList != null){
							for (OrderTrackingDetailEntry orderTrackingDetailItem : orderTrackingDetailEntryList) {
								CustomerOrderTrackingDetailEntry singleCustomerOrderTrackingDetailEntry = new CustomerOrderTrackingDetailEntry(
										orderTrackingDetailItem.getLocation(),
										orderTrackingDetailItem.getActionDate(),
										orderTrackingDetailItem.getActivityType(),
										orderTrackingDetailItem.getRemark());
								
								customerOrderTrackingDetailEntryList
										.add(singleCustomerOrderTrackingDetailEntry);
							}
						}
						
						// set tracking detail list for shipment
						singleShipmentEntry
								.setOrderTrackingDetailEntry(customerOrderTrackingDetailEntryList);
						
						// add trip detail to shipment
						

					}
					
					custOrderShipmentEntryList.add(singleShipmentEntry);
				}
				
				// set shipment list for order
				singleOrderEntry.setOrderShipments(custOrderShipmentEntryList);
				custOrderEntryList.add(singleOrderEntry);
			}

			// String skuSearchTerms = "id.in:"+Misc.idToString(skuIdSet);
			// SkuResponse skuResponse = SkuServiceClient.search(0, -1, null,
			// null, skuSearchTerms, null, TestHelper.dummyContextInfo1);
			
			// set the customer order response
			response.setData(custOrderEntryList);

		} catch (ERPServiceException e) {
			LOGGER.error("Error validating/retreiving data", e);
			StatusResponse error = new StatusResponse(e.getCode(),
					e.getMessage(), StatusResponse.Type.ERROR);
			response.setStatus(error);
			throw new ERPServiceException(response);
		} catch (Exception e) {
			LOGGER.error("error", e);
			StatusResponse error = new StatusResponse(
					CRMErrorCodes.ERR_RETRIEVING, StatusResponse.Type.ERROR);
			response.setStatus(error);
			throw new ERPServiceException(response);
		}

		StatusResponse success = new StatusResponse(
				CRMSuccessCodes.CONTACT_RETRIEVED,
				StatusResponse.Type.SUCCESS, 1);
		response.setStatus(success);
		return response;
	}
	
	private CustomerOrderBillingAddressEntry getCustomerOrderBillingDetailEntry(
			OrderEntry orderEntry) {

		BillingAddressEntry billingAddress = orderEntry.getBillingAddress();
		CustomerOrderBillingAddressEntry customerBillingAddress = new CustomerOrderBillingAddressEntry(
				billingAddress.getBillingFirstName(),
				billingAddress.getBillingLastName(),
				billingAddress.getBillingAddress(),
				billingAddress.getBillingCity(),
				billingAddress.getBillingCounty(),
				billingAddress.getBillingState(),
				billingAddress.getBillingZipCode(),
				billingAddress.getBillingMobile(),
				billingAddress.getBillingEmail());

		return customerBillingAddress;
	}
	
	private WarehouseEntry getWarehouseEntry(
			OrderReleaseEntry orderShipment) throws ERPServiceException {
		
		Integer warehuseId = orderShipment.getWarehouseId();
		
		if(warehuseId != null && warehuseId != 0){
			WarehouseResponse warehouseResponse = WarehouseServiceClient.findById(
					null, (long) warehuseId,
					Context.getContextInfo());
			
			if(warehouseResponse != null){
				List<WarehouseEntry> warehouseEntryList = warehouseResponse.getData();
				
				WarehouseEntry warehouseEntry = warehouseEntryList.iterator().next();
				return warehouseEntry;
			}	
		}
		
		return null;		
		
	}
	
	private SkuEntry getSKUEntry(OrderLineEntry orderItem)
			throws ERPServiceException {
		
		Long skuId = orderItem.getSkuId();
		
		if(skuId != null && skuId != 0){
			SkuResponse skuResponse = SkuServiceClient.findById(null,
					skuId, Context.getContextInfo());
			
			if(skuResponse != null){
				List<SkuEntry> skuEntryList = skuResponse.getData();
				
				SkuEntry skuEntry = skuEntryList.iterator().next();
				return skuEntry;
			}	
		}
		
		return null;		
	}
	
	private OrderTrackingEntry getOrderTrackingEntry(
			OrderReleaseEntry orderShipment) throws ERPServiceException {
		
		String trackingNo = orderShipment.getTrackingNo();
		
		if(trackingNo != null && trackingNo != ""){
			OrderTrackingResponse orderTrackingResponse = OrderTrackingServiceClient
					.getOrderTrackingDetail(null, orderShipment.getCourierCode(),
							trackingNo, Context.getContextInfo());
			
			if(orderTrackingResponse !=null){
				List<OrderTrackingEntry> orderTrackingEntryList = orderTrackingResponse
						.getOrderTrackings();
				
				OrderTrackingEntry orderTrackingEntry = orderTrackingEntryList
						.iterator().next();
				
				return orderTrackingEntry;	
			}	
		}
		
		return null;		
	}

	@Override
	public AbstractResponse create(CustomerOrderEntry e)
			throws ERPServiceException {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	@Override
	public AbstractResponse update(CustomerOrderEntry e, Long l)
			throws ERPServiceException {
		throw new UnsupportedOperationException("Not supported yet."); 
	}

	@Override
	public AbstractResponse search(int i, int i1, String string,
			String string1, String string2) throws ERPServiceException {
		throw new UnsupportedOperationException("Not supported yet."); 
	}

	@Override
	public AbstractResponse findById(Long l) throws ERPServiceException {
		throw new UnsupportedOperationException("Not supported yet.");
	}
}
