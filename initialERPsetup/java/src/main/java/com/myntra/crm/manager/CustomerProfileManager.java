/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myntra.crm.manager;

import com.myntra.commons.exception.ERPServiceException;
import com.myntra.commons.manager.BaseManager;
import com.myntra.crm.client.response.CustomerProfileEntry;
import com.myntra.crm.client.response.CustomerProfileResponse;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author pravin
 */
public interface CustomerProfileManager extends BaseManager<CustomerProfileResponse, CustomerProfileEntry>{
    public CustomerProfileResponse getCustomerProfileByLogin(String login) throws ERPServiceException;
    
    @Transactional(rollbackFor=Exception.class)
    public CustomerProfileResponse createCustomerProfile() throws ERPServiceException;
}