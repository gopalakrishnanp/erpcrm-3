package com.myntra.crm.service.impl;

import org.springframework.transaction.TransactionSystemException;

import com.myntra.commons.exception.ERPServiceException;
import com.myntra.commons.response.AbstractResponse;
import com.myntra.commons.service.impl.BaseServiceImpl;
import com.myntra.crm.client.response.CustomerOrderResponse;
import com.myntra.crm.entry.CustomerOrderEntry;
import com.myntra.crm.manager.CustomerOrderManager;
import com.myntra.crm.service.CustomerOrderService;

/**
 * Customer order search web service implementation which integrates detail of
 * order, shipments, skus, warehouse, tracking, trip detail etc..
 * 
 * @author Arun Kumar
 */
public class CustomerOrderServiceImpl extends
		BaseServiceImpl<CustomerOrderResponse, CustomerOrderEntry> implements
		CustomerOrderService {

	@Override
	public AbstractResponse getOrderSummary(int start, int fetchSize,
			String sortBy, String sortOrder, String searchTerms) {
		try {
			return ((CustomerOrderManager) getManager()).getOrderSummary(start,
					fetchSize, sortBy, sortOrder, searchTerms);

		} catch (ERPServiceException e) {
			return e.getResponse();

		} catch (TransactionSystemException ex) {
			return ((ERPServiceException) ex.getApplicationException())
					.getResponse();
		}

	}
}
