/**
 * Copyright (c) 2011, Myntra and/or its affiliates. All rights reserved.
 * MYNTRA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.myntra.crm.dao.impl;

import com.myntra.commons.dao.impl.BaseDataConnection;
import java.lang.reflect.ParameterizedType;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.log4j.Logger;

import com.myntra.commons.utils.Context;
import com.myntra.commons.utils.ReadWriteInfo;
import com.myntra.crm.dao.BaseCRMDAO;
import com.myntra.crm.entity.BaseCRMEntity;
import com.myntra.profiler.Profiler;

/**
 * @author ramu
 * 
 */
public class BaseCRMDAOImpl<T extends BaseCRMEntity> extends BaseDataConnection implements BaseCRMDAO<T> {

	private static final Logger LOGGER = Logger.getLogger(BaseCRMDAOImpl.class);

	@PersistenceContext
	private EntityManager entityManager;

	protected Class<T> entityClass = null;

	public BaseCRMDAOImpl() {
		ParameterizedType genericSuperclass = (ParameterizedType) getClass().getGenericSuperclass();
		this.entityClass = (Class<T>) genericSuperclass.getActualTypeArguments()[0];
	}

	/**
	 * @deprecated This method shouldn't be used from outside. We should use
	 *             {@code BaseDAO#getEntityManager(boolean)}
	 * 
	 * @return the entityManager
	 */
	public EntityManager getEntityManager() {
		return entityManager;
	}

	/**
	 * @param entityManager
	 *            the entityManager to set
	 */
	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	public static Logger getLogger() {
		return LOGGER;
	}

        @Override
	public EntityManager getEntityManager(boolean readOnly) {
		ReadWriteInfo rwInfo = Context.getReadWriteInfo();
		StringBuilder sb = new StringBuilder(this.getClass().getName());
		sb.append(".");
		sb.append("getEntityManager");
		Profiler.setContext(sb.toString());
		if (readOnly && rwInfo.useSlave()) {
			LOGGER.info("Using read only (slave) EntityManager");
			Profiler.increment("slave_read");
			return getRoEntityManager();
		} else {
			Profiler.increment("master_read");
			LOGGER.info("Using read-write (master) EntityManager.");
			return getEntityManager();
		}
	}
}
