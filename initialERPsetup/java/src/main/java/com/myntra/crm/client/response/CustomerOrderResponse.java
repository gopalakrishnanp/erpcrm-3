package com.myntra.crm.client.response;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import com.myntra.commons.response.AbstractResponse;
import com.myntra.crm.entry.CustomerOrderEntry;

/**
 * Response for customer order search web service with detail of shipments, skus,
 * warehouse, tracking, trip detail etc..
 * 
 * @author Arun Kumar
 */
@XmlRootElement(name = "customerOrderResponse")
public class CustomerOrderResponse extends AbstractResponse {

	private static final long serialVersionUID = 1L;
	private List<CustomerOrderEntry> orderEntry;

	@XmlElementWrapper(name = "data")
	@XmlElement(name = "order")
	public List<CustomerOrderEntry> getData() {
		return orderEntry;
	}

	public CustomerOrderResponse() {
	}

	public CustomerOrderResponse(List<CustomerOrderEntry> orderEntry) {		
		this.orderEntry = orderEntry;
	}

	public void setData(List<CustomerOrderEntry> orderEntry) {
		this.orderEntry = orderEntry;
	}

	@Override
	public String toString() {
		return "OrderResponse [data=" + orderEntry + ", getStatus()="
				+ getStatus() + "]";
	}
}
