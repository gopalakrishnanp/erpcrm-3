package com.myntra.crm.entry;

import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import com.myntra.commons.entries.BaseEntry;
import com.myntra.commons.tranformer.TransformIgnoreAttribute;

/**
 * Entry for customer order response with relevant order fields
 * 
 * @author Arun Kumar
 */
@XmlRootElement(name = "orderEntry")
public class CustomerOrderEntry extends BaseEntry {

	private static final long serialVersionUID = 1;

	// order unique ids
	private Long orderId;
	private String invoiceId;

	// customer detail of the order
	private String login;
	private String userContactNo;
	private String customerName;
	private CustomerOrderBillingAddressEntry billingAddress;

	// shipment detail
	private List<CustomerOrderShipmentEntry> orderShipments;

	// different amount of order
	private Double mrpTotal;
	private Double finalAmount;
	private Double discount;
	private Double couponDiscount;
	private Double cartDiscount;
	private Double cashRedeemed;
	private Double pgDiscount;
	private Double shippingCharge;
	private Double giftCharge;
	private Double codCharge;
	private Double cashbackOffered;
	private Double taxAmount;

	// dates
	private Date queuedOn;
	private Date cancelledOn;

	// order status and reasons
	private String orderStatus;
	private String orderStatusDisplay;
	private String cancellationReason;
	private Long cancellationReasonId;
	private String onHoldReason;
	private Long onHoldReasonId;
	private boolean onHold;
	private boolean giftOrder;
	private String notes;

	// discount coupon/cashback
	private String couponCode;
	private String cashBackCouponCode;

	// payment detail
	private String paymentMethod;
	private String paymentMethodDisplay;
	private String requestServer;
	private String responseServer;

	public CustomerOrderEntry() {
		// TODO Auto-generated constructor stub
	}

	@XmlElementWrapper(name = "orderShipments")
	@XmlElement(name = "orderShipmentEntry")
	@TransformIgnoreAttribute
	public List<CustomerOrderShipmentEntry> getOrderReleases() {
		return orderShipments;
	}

	public void setOrderShipments(
			List<CustomerOrderShipmentEntry> orderShipments) {
		this.orderShipments = orderShipments;
	}

	@TransformIgnoreAttribute
	public CustomerOrderBillingAddressEntry getBillingAddress() {
		return billingAddress;
	}

	public void setBillingAddress(
			CustomerOrderBillingAddressEntry billingAddress) {
		this.billingAddress = billingAddress;
	}

	public Long getOrderId() {
		return orderId;
	}

	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}

	public boolean getOnHold() {
		return onHold;
	}

	public void setOnHold(boolean onHold) {
		this.onHold = onHold;
	}

	@TransformIgnoreAttribute
	public Long getOnHoldReasonId() {
		return onHoldReasonId;
	}

	public void setOnHoldReasonId(Long onHoldReasonId) {
		this.onHoldReasonId = onHoldReasonId;
	}

	@TransformIgnoreAttribute
	public Long getCancellationReasonId() {
		return cancellationReasonId;
	}

	public void setCancellationReasonId(Long cancellationReasonId) {
		this.cancellationReasonId = cancellationReasonId;
	}

	public String getInvoiceId() {
		return invoiceId;
	}

	public void setInvoiceId(String invoiceId) {
		this.invoiceId = invoiceId;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	@TransformIgnoreAttribute
	public String getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}

	@TransformIgnoreAttribute
	public String getOrderStatusDisplay() {
		return orderStatusDisplay;
	}

	public void setOrderStatusDisplay(String orderStatusDisplay) {
		this.orderStatusDisplay = orderStatusDisplay;
	}

	@TransformIgnoreAttribute
	public String getPaymentMethod() {
		return paymentMethod;
	}

	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	public void setPaymentMethodDisplay(String paymentMethodDisplay) {
		this.paymentMethodDisplay = paymentMethodDisplay;
	}

	@TransformIgnoreAttribute
	public String getPaymentMethodDisplay() {
		return paymentMethodDisplay;
	}

	public String getCouponCode() {
		return couponCode;
	}

	public void setCouponCode(String couponCode) {
		this.couponCode = couponCode;
	}

	public String getCashBackCouponCode() {
		return cashBackCouponCode;
	}

	public void setCashBackCouponCode(String cashBackCouponCode) {
		this.cashBackCouponCode = cashBackCouponCode;
	}

	@TransformIgnoreAttribute
	public Double getMrpTotal() {
		return mrpTotal;
	}

	public void setMrpTotal(Double mrpTotal) {
		this.mrpTotal = mrpTotal;
	}

	@TransformIgnoreAttribute
	public Double getDiscount() {
		return discount;
	}

	public void setDiscount(Double discount) {
		this.discount = discount;
	}

	@TransformIgnoreAttribute
	public Double getCouponDiscount() {
		return couponDiscount;
	}

	public void setCouponDiscount(Double couponDiscount) {
		this.couponDiscount = couponDiscount;
	}

	@TransformIgnoreAttribute
	public Double getCartDiscount() {
		return cartDiscount;
	}

	@TransformIgnoreAttribute
	public void setCartDiscount(Double cartDiscount) {
		this.cartDiscount = cartDiscount;
	}

	@TransformIgnoreAttribute
	public Double getCashRedeemed() {
		return cashRedeemed;
	}

	public void setCashRedeemed(Double cashRedeemed) {
		this.cashRedeemed = cashRedeemed;
	}

	@TransformIgnoreAttribute
	public Double getPgDiscount() {
		return pgDiscount;
	}

	public void setPgDiscount(Double pgDiscount) {
		this.pgDiscount = pgDiscount;
	}

	@TransformIgnoreAttribute
	public Double getShippingCharge() {
		return shippingCharge;
	}

	public void setShippingCharge(Double shippingCharge) {
		this.shippingCharge = shippingCharge;
	}

	@TransformIgnoreAttribute
	public Double getGiftCharge() {
		return giftCharge;
	}

	public void setGiftCharge(Double giftCharge) {
		this.giftCharge = giftCharge;
	}

	@TransformIgnoreAttribute
	public Double getCodCharge() {
		return codCharge;
	}

	public void setCodCharge(Double codCharge) {
		this.codCharge = codCharge;
	}

	@TransformIgnoreAttribute
	public Double getFinalAmount() {
		return finalAmount;
	}

	public void setFinalAmount(Double finalAmount) {
		this.finalAmount = finalAmount;
	}

	@TransformIgnoreAttribute
	public Double getCashbackOffered() {
		return cashbackOffered;
	}

	public void setCashbackOffered(Double cashbackOffered) {
		this.cashbackOffered = cashbackOffered;
	}

	@TransformIgnoreAttribute
	public Double getTaxAmount() {
		return taxAmount;
	}

	public void setTaxAmount(Double taxAmount) {
		this.taxAmount = taxAmount;
	}

	public String getRequestServer() {
		return requestServer;
	}

	public void setRequestServer(String requestServer) {
		this.requestServer = requestServer;
	}

	public String getResponseServer() {
		return responseServer;
	}

	public void setResponseServer(String responseServer) {
		this.responseServer = responseServer;
	}

	@TransformIgnoreAttribute
	public String getCancellationReason() {
		return cancellationReason;
	}

	public void setCancellationReason(String cancellationReason) {
		this.cancellationReason = cancellationReason;
	}

	@TransformIgnoreAttribute
	public String getOnHoldReason() {
		return onHoldReason;
	}

	public void setOnHoldReason(String onHoldReason) {
		this.onHoldReason = onHoldReason;
	}

	public String getUserContactNo() {
		return userContactNo;
	}

	public void setUserContactNo(String userContactNo) {
		this.userContactNo = userContactNo;
	}

	public Date getQueuedOn() {
		return queuedOn;
	}

	public void setQueuedOn(Date queuedOn) {
		this.queuedOn = queuedOn;
	}

	public Date getCancelledOn() {
		return cancelledOn;
	}

	public void setCancelledOn(Date cancelledOn) {
		this.cancelledOn = cancelledOn;
	}

	public boolean isGiftOrder() {
		return giftOrder;
	}

	public void setGiftOrder(boolean giftOrder) {
		this.giftOrder = giftOrder;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	@Override
	public String toString() {
		return "OrderEntry [billingAddress=" + billingAddress
				+ ", cancellationReason=" + cancellationReason
				+ ", cancellationReasonId=" + cancellationReasonId
				+ ", cancelledOn=" + cancelledOn + ", cartDiscount="
				+ cartDiscount + ", cashBackCouponCode=" + cashBackCouponCode
				+ ", cashRedeemed=" + cashRedeemed + ", cashbackOffered="
				+ cashbackOffered + ", codCharge=" + codCharge
				+ ", couponCode=" + couponCode + ", couponDiscount="
				+ couponDiscount + ", discount=" + discount + ", finalAmount="
				+ finalAmount + ", giftCharge=" + giftCharge + ", invoiceId="
				+ invoiceId + ", login=" + login + ", mrpTotal=" + mrpTotal
				+ ", onHold=" + onHold + ", onHoldReason=" + onHoldReason
				+ ", onHoldReasonId=" + onHoldReasonId + ", orderReleases="
				+ orderShipments + ", orderStatus=" + orderStatus
				+ ", orderStatusDisplay=" + orderStatusDisplay
				+ ", paymentMethod=" + paymentMethod + ",customerName="
				+ customerName + ", paymentMethodDisplay="
				+ paymentMethodDisplay + ", pgDiscount=" + pgDiscount
				+ ", queuedOn=" + queuedOn + ", requestServer=" + requestServer
				+ ", responseServer=" + responseServer + ", shippingCharge="
				+ shippingCharge + ", taxAmount=" + taxAmount
				+ ", userContactNo=" + userContactNo + ", getCreatedBy()="
				+ getCreatedBy() + ", getCreatedOn()=" + getCreatedOn()
				+ ", getId()=" + getId() + ", getLastModifiedOn()="
				+ getLastModifiedOn() + ", giftOrder=" + giftOrder + ",notes="
				+ notes + "]";
	}

}
