package com.myntra.crm.client.response;

import com.myntra.commons.response.AbstractResponse;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Response for person web service
 * 
 */
@XmlRootElement(name = "customerProfileResponse")
public class CustomerProfileResponse extends AbstractResponse {

	List<CustomerProfileEntry> customerProfileEntry;
	
	@XmlElementWrapper(name = "data")
	@XmlElement(name = "customerProfile")
	public List<CustomerProfileEntry> getData() {
		return customerProfileEntry;
	}

	public CustomerProfileResponse() {
	}

	public CustomerProfileResponse(List<CustomerProfileEntry> customerProfileEntry) {
		this.customerProfileEntry=customerProfileEntry;
	}

	public void setData(List<CustomerProfileEntry> customerProfileEntry) {
		this.customerProfileEntry=customerProfileEntry;
	}	
}
