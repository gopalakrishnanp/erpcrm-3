1. Ensure appropriate ports are set in conf/server.xml

2. Create rollback-dbconfig.properties into conf folder with following contents
jdbc.driver=com.mysql.jdbc.Driver
jdbc.url=jdbc:mysql://localhost:3306/myntra_rollback
jdbc.user=<appropriate value>
jdbc.password=<appropriate value>
jdbc.maxConnections=25

3. Ensure following properties files are present in the tomcat/conf folder of running apache server
crm-dbconfig.properties			(copied from target/webapp/src/main/tomcatconf folder)
serviceurls.properties			(copied from target/webapp/src/main/tomcatconf folder)
rollback-dbconfig.properties		(Step to create this described earlier) 

4. Add garepos in /etc/hosts file
180.179.145.38  garepos.myntra.com

5. Create seed database on localsetup by running these sql files:
webapp/src/main/resources/sql/createschema.sql
webapp/src/main/resources/sql/insertdata.sql

Create database myntra_rollback if it does not exist.

6. Copy settings.xml into maven folder as mentioned in this page
http://192.168.20.20:8090/display/engg/Release+Automation


7. Make these changes in tomcat's bin/catalina.sh file to  include "-XX:MaxPermSize=512m -Djavamelody.log=true"

if [ -z "$LOGGING_MANAGER" ]; then
  JAVA_OPTS="$JAVA_OPTS -Djava.util.logging.manager=org.apache.juli.ClassLoaderLogManager -XX:MaxPermSize=512m -Djavamelody.log=true"
else
  JAVA_OPTS="$JAVA_OPTS $LOGGING_MANAGER -XX:MaxPermSize=512m -Djavamelody.log=true"
fi


8. Add this property in webapp/pom.xml in cargo configuration properties
<cargo.jvmargs>-Xms512m -Xmx512m -XX:PermSize=256m -XX:MaxPermSize=256m</cargo.jvmargs>
