package com.myntra.portal.crm.client.constant;


/**
 * Constants required for crm portal client 
 * @author arunkumar
 *
 */
public class CRMPortalClientConstants {
	public static final String CRM_PORTAL_URL = "crmPortalURL";	
}
