package com.myntra.portal.crm.client.response;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.myntra.commons.response.AbstractResponse;
import com.myntra.portal.crm.client.entry.CustomerProfileEntry;

/**
 * Response for person web service
 * 
 */
@XmlRootElement(name = "customerProfileResponse")
public class CustomerProfileResponse extends AbstractResponse {
	
	private static final long serialVersionUID = 1L;

	private CustomerProfileEntry customerProfileEntry;
	private boolean isCreatedRN = false;
	
	@XmlElement(name = "customerProfileEntry")
	public CustomerProfileEntry getCustomerProfileEntry() {
		return customerProfileEntry;
	}

	public void setCustomerProfileEntry(CustomerProfileEntry customerProfileEntry) {
		this.customerProfileEntry = customerProfileEntry;
	}
	
	public boolean isCreatedRN() {
		return isCreatedRN;
	}

	public void setCreatedRN(boolean isCreatedRN) {
		this.isCreatedRN = isCreatedRN;
	}

	public CustomerProfileResponse() {
		super();
	}

	@Override
	public String toString() {
		return "CustomerProfileResponse [customerProfileEntry="
				+ customerProfileEntry + ", isCreatedRN=" + isCreatedRN + "]";
	}
	
}