/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myntra.portal.crm.client.entry;

import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

import com.myntra.commons.entries.BaseEntry;

/**
 * Entry for cashback fields
 * 
 * @author Pravin Mehta
 */
@XmlRootElement(name = "couponEntry")
public class CouponEntry extends BaseEntry {
	
	private static final long serialVersionUID = 1;

    private String coupon;
    private Double minimum;
    private Integer times;
    private String per_user;
    private Integer times_used;
    private String couponType;
    private Double mrpAmount;
    private Double mrpPercentage;
    private Date expire;
    private String status;
    private String styleid;
    private Date startdate;
    private String groupName;
    private Boolean isInfinite;
    private Integer maxUsageByUser;
    private Boolean isInfinitePerUser;
    private String couponStatus;
    private Integer orderid;
    private Date useddate;
    
	public String getCoupon() {
		return coupon;
	}
	
	public void setCoupon(String coupon) {
		this.coupon = coupon;
	}

	public Double getMinimum() {
		return minimum;
	}

	public void setMinimum(Double minimum) {
		this.minimum = minimum;
	}

	public Integer getTimes() {
		return times;
	}

	public void setTimes(Integer times) {
		this.times = times;
	}

	public String getPer_user() {
		return per_user;
	}

	public void setPer_user(String per_user) {
		this.per_user = per_user;
	}

	public Integer getTimes_used() {
		return times_used;
	}

	public void setTimes_used(Integer times_used) {
		this.times_used = times_used;
	}

	public String getCouponType() {
		return couponType;
	}

	public void setCouponType(String couponType) {
		this.couponType = couponType;
	}

	public Double getMrpAmount() {
		return mrpAmount;
	}

	public void setMrpAmount(Double mrpAmount) {
		this.mrpAmount = mrpAmount;
	}

	public Double getMrpPercentage() {
		return mrpPercentage;
	}

	public void setMrpPercentage(Double mrpPercentage) {
		this.mrpPercentage = mrpPercentage;
	}

	public Date getExpire() {
		return expire;
	}

	public void setExpire(Date expire) {
		this.expire = expire;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getStyleid() {
		return styleid;
	}

	public void setStyleid(String styleid) {
		this.styleid = styleid;
	}

	public Date getStartdate() {
		return startdate;
	}

	public void setStartdate(Date startdate) {
		this.startdate = startdate;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public Boolean getIsInfinite() {
		return isInfinite;
	}

	public void setIsInfinite(Boolean isInfinite) {
		this.isInfinite = isInfinite;
	}

	public Integer getMaxUsageByUser() {
		return maxUsageByUser;
	}

	public void setMaxUsageByUser(Integer maxUsageByUser) {
		this.maxUsageByUser = maxUsageByUser;
	}

	public Boolean getIsInfinitePerUser() {
		return isInfinitePerUser;
	}

	public void setIsInfinitePerUser(Boolean isInfinitePerUser) {
		this.isInfinitePerUser = isInfinitePerUser;
	}

	public String getCouponStatus() {
		return couponStatus;
	}

	public void setCouponStatus(String couponStatus) {
		this.couponStatus = couponStatus;
	}
	
	public Integer getOrderid() {
		return orderid;
	}

	public void setOrderid(Integer orderid) {
		this.orderid = orderid;
	}

	public Date getUseddate() {
		return useddate;
	}

	public void setUseddate(Date useddate) {
		this.useddate = useddate;
	}

	public CouponEntry() {
		// super();
	}

	public CouponEntry(String coupon, Double minimum, Integer times,
			String per_user, Integer times_used, String couponType,
			Double mrpAmount, Double mrpPercentage, Date expire, String status,
			String styleid, Date startdate, String groupName,
			Boolean isInfinite, Integer maxUsageByUser,
			Boolean isInfinitePerUser, String couponStatus, Integer orderid,
			Date useddate) {
		this.coupon = coupon;
		this.minimum = minimum;
		this.times = times;
		this.per_user = per_user;
		this.times_used = times_used;
		this.couponType = couponType;
		this.mrpAmount = mrpAmount;
		this.mrpPercentage = mrpPercentage;
		this.expire = expire;
		this.status = status;
		this.styleid = styleid;
		this.startdate = startdate;
		this.groupName = groupName;
		this.isInfinite = isInfinite;
		this.maxUsageByUser = maxUsageByUser;
		this.isInfinitePerUser = isInfinitePerUser;
		this.couponStatus = couponStatus;
		this.orderid = orderid;
		this.useddate = useddate;
	}

	@Override
	public String toString() {
		return "CouponEntry [coupon=" + coupon + ", minimum=" + minimum
				+ ", times=" + times + ", per_user=" + per_user
				+ ", times_used=" + times_used + ", couponType=" + couponType
				+ ", mrpAmount=" + mrpAmount + ", mrpPercentage="
				+ mrpPercentage + ", expire=" + expire + ", status=" + status
				+ ", styleid=" + styleid + ", startdate=" + startdate
				+ ", groupName=" + groupName + ", isInfinite=" + isInfinite
				+ ", maxUsageByUser=" + maxUsageByUser + ", isInfinitePerUser="
				+ isInfinitePerUser + ", couponStatus=" + couponStatus
				+ ", orderid=" + orderid + ", useddate=" + useddate + "]";
	}
}

