package com.myntra.portal.crm.client.code;

import com.myntra.commons.codes.ERPErrorCodes;
import com.myntra.commons.codes.StatusCodes;

/**
 * CRM error codes
 * 
 * @author Arun Kumar
 * 
 */
public class CRMPortalErrorCodes extends ERPErrorCodes {	
	
	public static final StatusCodes NO_CUSTOMER_RETURN_RETRIEVED = new CRMPortalErrorCodes(803, "NO_CUSTOMER_RETURN_RETRIEVED");	
    public static final StatusCodes NO_CUSTOMER_PROFILE_RETRIEVED = new CRMPortalErrorCodes(804, "NO_CUSTOMER_PROFILE_RETRIEVED");
	public static final StatusCodes NO_COUPON_RETRIEVED = new CRMPortalErrorCodes(805, "NO_COUPON_RETRIEVED");	
	public static final StatusCodes NO_PAYMENT_LOG = new CRMPortalErrorCodes(808, "NO_PAYMENT_LOG");
	public static final StatusCodes NO_ORDER_COMMENT_LOG = new CRMPortalErrorCodes(809, "NO_ORDER_COMMENT_LOG");
	public static final StatusCodes NO_COD_ORDER_OH_REASON_LOG = new CRMPortalErrorCodes(810, "NO_COD_ORDER_OH_REASON_LOG");
	public static final StatusCodes NO_SERVICE_REQUEST_RETRIEVED = new CRMPortalErrorCodes(811, "NO_SERVICE_REQUEST_RETRIEVED");

	private static final String BUNDLE_NAME = "CRMPortalErrorCodes";

	public CRMPortalErrorCodes(int errorCode, String errorMessage) {
		setAll(errorCode, errorMessage, BUNDLE_NAME);
	}

}
