package com.myntra.portal.crm.client.response;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import com.myntra.commons.response.AbstractResponse;
import com.myntra.portal.crm.client.entry.CodOnHoldLogEntry;

/**
 * Response for COD order on hold reason log web service
 * 
 * @author Arun Kumar
 */
@XmlRootElement(name = "codOnHoldLogResponse")
public class CodOnHoldLogResponse extends AbstractResponse {
	private static final long serialVersionUID = 3688398701763484215L;
	
	private List<CodOnHoldLogEntry> codOnHoldLogEntryList;

	public CodOnHoldLogResponse() {
	}

	@XmlElementWrapper(name = "codOnHoldLogEntryList")
	@XmlElement(name = "codOnHoldLogEntry")
	public List<CodOnHoldLogEntry> getCodOnHoldLogEntryList() {
		return codOnHoldLogEntryList;
	}

	public void setCodOnHoldLogEntryList(List<CodOnHoldLogEntry> codOnHoldLogEntryList) {
		this.codOnHoldLogEntryList = codOnHoldLogEntryList;
	}

	@Override
	public String toString() {
		return "CodOnHoldLogResponse [codOnHoldLogEntryList=" + codOnHoldLogEntryList + "]";
	}
}