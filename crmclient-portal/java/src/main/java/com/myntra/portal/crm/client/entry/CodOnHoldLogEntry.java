package com.myntra.portal.crm.client.entry;

import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

import com.myntra.commons.entries.BaseEntry;

/**
 * Entry for customer COD order onhold reason log
 * 
 * @author Arun Kumar
 */
@XmlRootElement(name = "codOnHoldLogEntry")
public class CodOnHoldLogEntry extends BaseEntry {

	private static final long serialVersionUID = 1L;

	private Long orderId;
	private String disposition;
	private String reason;
	private String reasonDisplayName;
	private Integer trialNumber;
	private String createdBy;
	private String comment;
	private Date createdDate;

	public CodOnHoldLogEntry() {

	}

	public CodOnHoldLogEntry(Long orderId, String disposition, String reason, String reasonDisplayName,
			Integer trialNumber, String createdBy, String comment, Date createdDate) {
		this.orderId = orderId;
		this.disposition = disposition;
		this.reason = reason;
		this.reasonDisplayName = reasonDisplayName;
		this.trialNumber = trialNumber;
		this.createdBy = createdBy;
		this.comment = comment;
		this.createdDate = createdDate;
	}

	public Long getOrderId() {
		return orderId;
	}

	public String getDisposition() {
		return disposition;
	}

	public String getReason() {
		return reason;
	}

	public String getReasonDisplayName() {
		return reasonDisplayName;
	}

	public Integer getTrialNumber() {
		return trialNumber;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public String getComment() {
		return comment;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}

	public void setDisposition(String disposition) {
		this.disposition = disposition;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public void setReasonDisplayName(String reasonDisplayName) {
		this.reasonDisplayName = reasonDisplayName;
	}

	public void setTrialNumber(Integer trialNumber) {
		this.trialNumber = trialNumber;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	@Override
	public String toString() {
		return "codOnHoldLogEntry [orderId=" + orderId + ", disposition=" + disposition + ", reason=" + reason
				+ ", reasonDisplayName=" + reasonDisplayName + ", trialNumber=" + trialNumber + ", createdBy="
				+ createdBy + ", comment=" + comment + ", createdDate=" + createdDate + "]";
	}	
}