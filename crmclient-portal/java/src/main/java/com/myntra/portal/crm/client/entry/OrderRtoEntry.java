package com.myntra.portal.crm.client.entry;

import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.annotation.XmlRootElement;

import com.myntra.commons.entries.BaseEntry;

/**
 * Entry for order RTO
 * 
 * @author Arun Kumar
 */
@XmlRootElement(name = "orderRtoEntry")
public class OrderRtoEntry extends BaseEntry {

	private static final long serialVersionUID = 1L;

	private Long orderId;
	private String rtoStatus;
	private String rtoStatusDisplay;
	
	private Map<String,String> rtoStatusMap;	
	
	public Map<String,String> getRtoStatusMap() {
		return rtoStatusMap;
	}
	
	public void setRtoStatusMap(Map<String,String> rtoStatusMap) {
		this.rtoStatusMap = rtoStatusMap;
	}

	public OrderRtoEntry() {		
		// initialize rto staus map
		Map<String, String> rtoMap = new HashMap<String, String>();
		
		rtoMap.put("RTOPREQ", "RTO Pre Queue State");
		rtoMap.put("RTOQ", "RTO Queued");
		rtoMap.put("RTOCAL1", "RTO Calling Attempt - 1");
		rtoMap.put("RTOCAL2", "RTO Calling Attempt - 2");
		rtoMap.put("RTOCAL3", "RTO Calling Attempt - 3");
		rtoMap.put("RTORSC", "RTO Reshipped");
		rtoMap.put("RTOC", "RTO Cancelled");
		rtoMap.put("RTORS", "RTO to be Reshipped");
		rtoMap.put("RTORF", "RTO Refunded");
		rtoMap.put("RTOAI", "Items Restocked");
		rtoMap.put("CMMNT", "Add Comment");
		rtoMap.put("END", "Processing Complete");
		
		this.setRtoStatusMap(rtoMap);
	}

	public OrderRtoEntry(Long orderId, String rtoStatus, String rtoStatusDisplay) {		
		this.orderId = orderId;
		this.rtoStatus = rtoStatus;
		this.rtoStatusDisplay = rtoStatusDisplay;
	}


	public Long getOrderId() {
		return orderId;
	}

	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}

	public String getRtoStatus() {
		return rtoStatus;
	}

	public void setRtoStatus(String rtoStatus) {
		this.rtoStatus = rtoStatus;
	}

	public String getRtoStatusDisplay() {
		return rtoStatusDisplay;
	}

	public void setRtoStatusDisplay(String rtoStatusDisplay) {
		this.rtoStatusDisplay = rtoStatusDisplay;
	}

	@Override
	public String toString() {
		return "OrderRtoEntry [orderId=" + orderId + ", rtoStatus=" + rtoStatus + ", rtoStatusDisplay="
				+ rtoStatusDisplay + "]";
	}

}
