#!/bin/bash
export filename=`find -name \*.rpm | grep $1 | head -1`
export a=`basename $filename`
export b="1.0.0"
cmd=`echo $a | grep -o $b.*`
#echo "Command: "$cmd
noarch=$(expr index "$cmd"  "noarch")
#echo $noarch
version=${cmd:0:$noarch-2}
echo $version
