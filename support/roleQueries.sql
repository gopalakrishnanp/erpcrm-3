/*
DELTA7:
mysql -h10.136.54.95 -umyntraAppDBUser -p9eguCrustuBR -P3307 myntra_security
jdbc.url=jdbc:mysql://10.136.54.95:3307/myntra_security
jdbc.user=myntraAppDBUser
jdbc.password=9eguCrustuBR


PRODDB
mysql -hproddb -umyntraAppDBUser -p9eguCrustuBR -P3306 myntra_security
  jdbc.write.url: jdbc:mysql://proddb:3306/myntra_security
        jdbc.write.rollback.url: jdbc:mysql://proddb:3306/myntra_rollback
        jdbc.write.username: myntraAppDBUser
        jdbc.write.password: 9eguCrustuBR

Security Roles.

QA DB ...
mysql -umyntraAppDBUser -p9eguCrustuBR -hec2-54-251-97-209.ap-southeast-1.compute.amazonaws.com myntra_security

QA SSo ..
ssoUrl: http://54.251.103.6:7011/faces/sso.xhtml
securityURL: http://54.251.103.6:7008/myntra-security-service/
securityServiceUrl: http://54.251.103.6:7008/myntra-security-service/platform/security/users/authenticate/V2/

*/
use myntra_security;

-- create roles for TASKY Access for QMT and Finance
insert into roles values (null,"TASKY_QMT",    now(),"pravin.mehta",now(),0,null,null);
insert into roles values (null,"TASKY_CATALOGUE",    now(),"pravin.mehta",now(),0,null,null);
insert into roles values (null,"TASKY_FEEDBACK",    now(),"pravin.mehta",now(),0,null,null);
insert into roles values (null,"TASKY_FINANCE",now(),"pravin.mehta",now(),0,null,null);
insert into roles values (null,"TASKY_DELIVERY_ML",now(),"pravin.mehta",now(),0,null,null);
insert into roles values (null,"TASKY_DELIVERY_3PL",now(),"pravin.mehta",now(),0,null,null);
insert into roles values (null,"TASKY_ENGG",now(),"pravin.mehta",now(),0,null,null);
insert into roles values (null,"TASKY_WAREHOUSE",now(),"pravin.mehta",now(),0,null,null);
insert into roles values (null,"TASKY_MARKETING",now(),"pravin.mehta",now(),0,null,null);


-- create user pages for TASKY reports Access for QMT and Finance
insert into user_pages values (null,"tasky.All",             	now(),"pravin.mehta",now(),0, 'TASKY');
insert into user_pages values (null,"tasky.CatalogueReports", 	now(),"pravin.mehta",now(),0, 'TASKY');
insert into user_pages values (null,"tasky.FeedbackReports", 	now(),"pravin.mehta",now(),0, 'TASKY');
insert into user_pages values (null,"tasky.FinanceReports",  	now(),"pravin.mehta",now(),0, 'TASKY');
insert into user_pages values (null,"tasky.DeliveryMLReports",	now(),"pravin.mehta",now(),0, 'TASKY');
insert into user_pages values (null,"tasky.Delivery3PLReports",	now(),"pravin.mehta",now(),0, 'TASKY');
insert into user_pages values (null,"tasky.EnggReports",     	now(),"pravin.mehta",now(),0, 'TASKY');
insert into user_pages values (null,"tasky.WarehouseReports",  	now(),"pravin.mehta",now(),0, 'TASKY');
insert into user_pages values (null,"tasky.MarketingReports",  	now(),"pravin.mehta",now(),0, 'TASKY');

-- assign permission for rules to the user pages
insert into roles_user_pages_permissions (
select null,tmp_roles.role_id,tmp_pages.page_id,7,now(),"pravin.mehta",now(),0
from 
(select id as role_id from roles where name = "TASKY_QMT") tmp_rolesd,
(select id as page_id from user_pages where page_name = "tasky.All") tmp_pages 
);

insert into roles_user_pages_permissions (
select null,tmp_roles.role_id,tmp_pages.page_id,7,now(),"pravin.mehta",now(),0
from 
(select id as role_id from roles where name = "TASKY_CATALOGUE") tmp_roles,
(select id as page_id from user_pages where page_name = "tasky.CatalogueReports") tmp_pages 
);

insert into roles_user_pages_permissions (
select null,tmp_roles.role_id,tmp_pages.page_id,7,now(),"pravin.mehta",now(),0
from 
(select id as role_id from roles where name = "TASKY_FEEDBACK") tmp_roles,
(select id as page_id from user_pages where page_name = "tasky.FeedbackReports") tmp_pages 
);

insert into roles_user_pages_permissions (
  select null,tmp_roles.role_id,tmp_pages.page_id,7,now(),"pravin.mehta",now(),0
  from 
   (select id as role_id from roles where name = "TASKY_FINANCE") tmp_roles,
   (select id as page_id from user_pages where page_name = "tasky.FinanceReports") tmp_pages 
);


insert into roles_user_pages_permissions (
  select null,tmp_roles.role_id,tmp_pages.page_id,7,now(),"pravin.mehta",now(),0
  from 
   (select id as role_id from roles where name = "TASKY_DELIVERY_ML") tmp_roles,
   (select id as page_id from user_pages where page_name = "tasky.DeliveryMLReports") tmp_pages 
);

insert into roles_user_pages_permissions (
  select null,tmp_roles.role_id,tmp_pages.page_id,7,now(),"pravin.mehta",now(),0
  from 
   (select id as role_id from roles where name = "TASKY_DELIVERY_3PL") tmp_roles,
   (select id as page_id from user_pages where page_name = "tasky.Delivery3PLReports") tmp_pages 
);

insert into roles_user_pages_permissions (
  select null,tmp_roles.role_id,tmp_pages.page_id,7,now(),"pravin.mehta",now(),0
  from 
   (select id as role_id from roles where name = "TASKY_ENGG") tmp_roles,
   (select id as page_id from user_pages where page_name = "tasky.EnggReports") tmp_pages 
);

insert into roles_user_pages_permissions (
  select null,tmp_roles.role_id,tmp_pages.page_id,7,now(),"pravin.mehta",now(),0
  from 
   (select id as role_id from roles where name = "TASKY_WAREHOUSE") tmp_roles,
   (select id as page_id from user_pages where page_name = "tasky.WarehouseReports") tmp_pages 
);

insert into roles_user_pages_permissions (
  select null,tmp_roles.role_id,tmp_pages.page_id,7,now(),"pravin.mehta",now(),0
  from 
   (select id as role_id from roles where name = "TASKY_MARKETING") tmp_roles,
   (select id as page_id from user_pages where page_name = "tasky.MarketingReports") tmp_pages 
);

-- assign roles to Tasky Users users
insert into roles_users (
  select null, tmp_roles.role_id, tmp_users.user_id, now(), 'pravin.mehta',now(),0
  from
    (select id as role_id from roles where name = 'TASKY_FINANCE') tmp_roles,
    (select id as user_id from users where login_id in ("pravin.mehta")) tmp_users
);


insert into roles_users (
  select null, tmp_roles.role_id, tmp_users.user_id, now(), 'pravin.mehta',now(),0
  from
    (select id as role_id from roles where name = 'TASKY_QMT') tmp_roles,
    (select id as user_id from users where login_id in ("santhosh.pattanshettar")) tmp_users
);
