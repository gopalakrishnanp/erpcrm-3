package com.myntra.erp.crm.client.entry;

import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

import com.myntra.commons.entries.BaseEntry;

/**
 * Entry for cashback fields
 * 
 * @author Pravin Mehta
 */
@XmlRootElement(name="cashbackLogEntry")
public class CashbackLogEntry extends BaseEntry {

	private static final long serialVersionUID = 1;

	private Long cashbackAccountId;
	private Long itemId;
	private String itemType;
	private String businessProcess;
	private Double creditInflow;
	private Double creditOutflow;
	private Double balance;
	private Date modifiedOn;
	private String modifiedBy;
	private String description;
	
	// We need to split description for easier usage in one view.
	private String description1;
	private String descriptionReturnNo;
	private String descriptionOrderNo;
	private String description2;
	
	private static String[] filtersOrder = 
			new String[]{"order - ","order no. ", "Order No. ", "new exchanged order: ","(Gift card) no. "};
	
	private static String[] filtersReturn = new String[]{"Return No. "    };

	
	public CashbackLogEntry() {
	}
	
	public CashbackLogEntry(Long cashbackAccountId, Long itemId, String itemType,
			String businessProcess, Double creditInflow, Double creditOutflow,
			Double balance, Long modifiedOn, String modifiedBy, String description) {
		this.cashbackAccountId = cashbackAccountId;
		this.itemId = itemId;
		this.itemType = itemType;
		this.businessProcess = businessProcess;
		this.creditInflow = creditInflow;
		this.creditOutflow = creditOutflow;
		this.balance = balance;
		this.modifiedOn = new Date(modifiedOn);
		this.modifiedBy = modifiedBy;
		this.description = description;
		
		updateDescriptionLink();
	}
	
	private void updateDescriptionLink() {
		this.updateDescriptionLink(filtersOrder, "order");
		this.updateDescriptionLink(filtersReturn, "return");
		
		if(this.description1==null) {
			this.description1 = this.description;
		}
	}
	
	private void updateDescriptionLink(String filters[], String type) {
		
		for(int i=0;i<filters.length;i++) {
            int index = this.description.indexOf(filters[i]);
            if(index>=0) {
                this.description1 = this.description.substring(0, index);
                this.description1+=filters[i];

                int j=index+filters[i].length();
                
                while(j<this.description.length()) {
                	if(!Character.isDigit(this.description.charAt(j)))
                		break;
                	j++;
                }
                
                String ID = this.description.substring(index + filters[i].length(), j);
                
                if(type.equals("order") && !ID.trim().equals("")) { 
                	this.descriptionOrderNo = ID;
                } else if(type.equals("return") && !ID.trim().equals("")) { 
                	this.descriptionReturnNo = ID;
                }
                
                if(j!=this.description.length())
                	this.description2 = this.description.substring(this.description1.length() + ID.length());
            }
        }
	}
	
	public Long getCashbackAccountId() {
		return cashbackAccountId;
	}
	
	public void setCashbackAccountId(Long cashbackAccountId) {
		this.cashbackAccountId = cashbackAccountId;
	}
	
	public Long getItemId() {
		return itemId;
	}
	
	public void setItemId(Long itemId) {
		this.itemId = itemId;
	}
	
	public String getItemType() {
		return itemType;
	}
	
	public void setItemType(String itemType) {
		this.itemType = itemType;
	}
	
	public String getBusinessProcess() {
		return businessProcess;
	}
	
	public void setBusinessProcess(String businessProcess) {
		this.businessProcess = businessProcess;
	}
	
	public Double getCreditInflow() {
		return creditInflow;
	}
	
	public void setCreditInflow(Double creditInflow) {
		this.creditInflow = creditInflow;
	}
	
	public Double getCreditOutflow() {
		return creditOutflow;
	}
	
	public void setCreditOutflow(Double creditOutflow) {
		this.creditOutflow = creditOutflow;
	}
	
	public Double getBalance() {
		return balance;
	}
	
	public void setBalance(Double balance) {
		this.balance = balance;
	}
	
	public Date getModifiedOn() {
		return modifiedOn;
	}
	
	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}
	
	public String getModifiedBy() {
		return modifiedBy;
	}
	
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public String getDescription1() {
		return description1;
	}

	public void setDescription1(String description1) {
		this.description1 = description1;
	}

	public String getDescriptionReturnNo() {
		return descriptionReturnNo;
	}

	public void setDescriptionReturnNo(String descriptionReturnNo) {
		this.descriptionReturnNo = descriptionReturnNo;
	}

	public String getDescriptionOrderNo() {
		return descriptionOrderNo;
	}

	public void setDescriptionOrderNo(String descriptionOrderNo) {
		this.descriptionOrderNo = descriptionOrderNo;
	}

	public String getDescription2() {
		return description2;
	}

	public void setDescription2(String description2) {
		this.description2 = description2;
	}

	@Override
	public String toString() {
		return "CashbackLogEntry [cashbackAccountId=" + cashbackAccountId
				+ ", itemId=" + itemId + ", itemType=" + itemType
				+ ", businessProcess=" + businessProcess + ", creditInflow="
				+ creditInflow + ", creditOutflow=" + creditOutflow
				+ ", balance=" + balance + ", modifiedOn=" + modifiedOn
				+ ", modifiedBy=" + modifiedBy + ", description=" + description
				+ ", description1=" + description1 + ", descriptionReturnNo="
				+ descriptionReturnNo + ", descriptionOrderNo="
				+ descriptionOrderNo + ", description2=" + description2 + "]";
	}
	
	/*public static void main(String args[]) {
		CashbackLogEntry entry = new CashbackLogEntry();
		entry.setDescription("Usage on order no. 7310470");
		entry.updateDescriptionLink();
		System.out.println(entry);
	}*/
}
