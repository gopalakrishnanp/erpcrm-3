package com.myntra.erp.crm.client.response;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import com.myntra.commons.response.AbstractResponse;
import com.myntra.erp.crm.client.entry.CashbackBalanceEntry;
import com.myntra.erp.crm.client.entry.CashbackLogEntry;

/**
 * Response for Cashback Service
 * 
 */
@XmlRootElement(name = "cashbackResponse")
public class CashbackResponse extends AbstractResponse {

	private static final long serialVersionUID = 1L;

	private CashbackBalanceEntry cashbackBalanceEntry;
	private List<CashbackLogEntry> cashbackLogEntryList;

	@XmlElementWrapper(name = "cashbackLogEntryList")
	@XmlElement(name = "cashbackLogEntry")
	public List<CashbackLogEntry> getCashbackLogEntryList() {
		return cashbackLogEntryList;
	}

	@XmlElement(name = "cashbackBalanceEntry")
	public CashbackBalanceEntry getCashbackBalanceEntry() {
		return cashbackBalanceEntry;
	}

	public CashbackResponse() {
	}

	public void setCashbackBalanceEntry(CashbackBalanceEntry cashbackBalanceEntry) {
		this.cashbackBalanceEntry = cashbackBalanceEntry;
	}

	public void setCashbackLogEntryList(List<CashbackLogEntry> cashbackLogEntryList) {
		this.cashbackLogEntryList = cashbackLogEntryList;
	}

	@Override
	public String toString() {
		return "CashbackResponse [cashbackBalanceEntry=" + cashbackBalanceEntry + ", cashbackLogEntryList="
				+ cashbackLogEntryList + "]";
	}

}