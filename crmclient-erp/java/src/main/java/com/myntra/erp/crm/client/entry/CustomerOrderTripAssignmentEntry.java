package com.myntra.erp.crm.client.entry;

import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.myntra.commons.entries.BaseEntry;
import com.myntra.lms.client.status.DeliveryPickupReasonCode;
import com.myntra.lms.client.status.NotificationStatus;
import com.myntra.lms.client.status.ShipmentType;
import com.myntra.lms.client.status.TripOrderStatus;
import com.myntra.lms.client.status.TripStatus;

/**
 * Entry for customer order trip assignment fields
 * 
 * @author Arun Kumar
 */
@XmlRootElement(name = "customerOrderTripAssignmentEntry")
@XmlAccessorType(XmlAccessType.FIELD)
public class CustomerOrderTripAssignmentEntry extends BaseEntry {

	private static final long serialVersionUID = 1L;

	private String trackingNumber;
	private String tripNumber;
	private String deliveryStaffName;	
	private Long deliveryStaffId;
	private Long deliveryCenterId;
	private String deliveryStaffMobile;

	private TripOrderStatus tripOrderStatus;
	private String tripOrderStatusValue;
	private TripStatus correspondingTripStatus;
	private NotificationStatus notificationStatus;
	private DeliveryPickupReasonCode deliveryReasonCode;
	private DeliveryPickupReasonCode pickupReasonCode;
	private ShipmentType shipmentType;

	private String itemDescription;
	private String remark;

	private Date deliveryTime;
	private Date tripStartTime;
	private Date tripEndTime;

	public CustomerOrderTripAssignmentEntry() {
	}

	public String getDeliveryStaffName() {
		return deliveryStaffName;
	}

	public void setDeliveryStaffName(String deliveryStaffName) {
		this.deliveryStaffName = deliveryStaffName;
	}
	
	public Long getDeliveryStaffId() {
		return deliveryStaffId;
	}

	public void setDeliveryStaffId(Long deliveryStaffId) {
		this.deliveryStaffId = deliveryStaffId;
	}
	
	public Long getDeliveryCenterId() {
		return deliveryCenterId;
	}

	public void setDeliveryCenterId(Long deliveryCenterId) {
		this.deliveryCenterId = deliveryCenterId;
	}
	
	public String getDeliveryStaffMobile() {
		return deliveryStaffMobile;
	}

	public void setDeliveryStaffMobile(String deliveryStaffMobile) {
		this.deliveryStaffMobile = deliveryStaffMobile;
	}

	public TripStatus getCorrespondingTripStatus() {
		return correspondingTripStatus;
	}

	public void setCorrespondingTripStatus(TripStatus correspondingTripStatus) {
		this.correspondingTripStatus = correspondingTripStatus;
	}

	public Date getDeliveryTime() {
		return deliveryTime;
	}

	public void setDeliveryTime(Date deliveryTime) {
		this.deliveryTime = deliveryTime;
	}

	public String getTripNumber() {
		return tripNumber;
	}

	public void setTripNumber(String tripNumber) {
		this.tripNumber = tripNumber;
	}
	
	public String getTripOrderStatusValue() {
		return tripOrderStatusValue;
	}

	public void setTripOrderStatusValue(String tripStatusValue) {
		this.tripOrderStatusValue = tripStatusValue;
	}

	public NotificationStatus getNotificationStatus() {
		return notificationStatus;
	}

	public void setNotificationStatus(NotificationStatus notificationStatus) {
		this.notificationStatus = notificationStatus;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getTrackingNumber() {
		return trackingNumber;
	}

	public void setTrackingNumber(String trackingNumber) {
		this.trackingNumber = trackingNumber;
	}

	public TripOrderStatus getTripOrderStatus() {
		return tripOrderStatus;
	}

	public void setTripOrderStatus(TripOrderStatus tripOrderStatus) {
		this.tripOrderStatus = tripOrderStatus;
	}

	public ShipmentType getShipmentType() {
		return shipmentType;
	}

	public void setShipmentType(ShipmentType shipmentType) {
		this.shipmentType = shipmentType;
	}

	public DeliveryPickupReasonCode getPickupReasonCode() {
		return pickupReasonCode;
	}

	public void setPickupReasonCode(DeliveryPickupReasonCode pickupReasonCode) {
		this.pickupReasonCode = pickupReasonCode;
	}

	public DeliveryPickupReasonCode getDeliveryReasonCode() {
		return deliveryReasonCode;
	}

	public void setDeliveryReasonCode(DeliveryPickupReasonCode deliveryReasonCode) {
		this.deliveryReasonCode = deliveryReasonCode;
	}

	public String getItemDescription() {
		return itemDescription;
	}

	public void setItemDescription(String itemDescription) {
		this.itemDescription = itemDescription;
	}

	public Date getTripEndTime() {
		return tripEndTime;
	}

	public void setTripEndTime(Date tripEndTime) {
		this.tripEndTime = tripEndTime;
	}

	public Date getTripStartTime() {
		return tripStartTime;
	}

	public void setTripStartTime(Date tripStartTime) {
		this.tripStartTime = tripStartTime;
	}
	

	@Override
	public String toString() {
		return "CustomerOrderTripAssignmentEntry [trackingNumber="
				+ trackingNumber + ", tripNumber=" + tripNumber
				+ ", deliveryStaffName=" + deliveryStaffName	
				+ ", deliveryStaffId=" + deliveryStaffId
				+ ", deliveryCenterId=" + deliveryCenterId
				+ ", deliveryStaffMobile=" + deliveryStaffMobile
				+ ", tripOrderStatus=" + tripOrderStatus
				+ ", tripOrderStatusValue=" + tripOrderStatusValue
				+ ", correspondingTripStatus=" + correspondingTripStatus
				+ ", notificationStatus=" + notificationStatus
				+ ", deliveryReasonCode=" + deliveryReasonCode
				+ ", pickupReasonCode=" + pickupReasonCode + ", shipmentType="
				+ shipmentType + ", itemDescription=" + itemDescription
				+ ", remark=" + remark + ", deliveryTime=" + deliveryTime
				+ ", tripStartTime=" + tripStartTime + ", tripEndTime="
				+ tripEndTime + "]";
	}

}