package com.myntra.erp.crm.client.entry;

import javax.xml.bind.annotation.XmlRootElement;

import com.myntra.commons.entries.BaseEntry;

/**
 * Entry for trip delivery staff/center fields
 * 
 * @author Arun Kumar
 */
@XmlRootElement(name = "tripDeliveryStaffEntry")
public class TripDeliveryStaffEntry extends BaseEntry {

	private static final long serialVersionUID = 1L;

	// staff detail
	private String firstName;
	private String lastName;
	private String mobile;
	private Boolean available;
	private Boolean deleted;
	private String code;
	
	// delivery center detail for the staff
	private String dcCode;
	private String dcName;
	private String dcManager;
	private String dcContactNumber;
	private Boolean dcActive;
	private String dcCity;
	private String dcCityCode;
	private String dcState;
	private String dcAddress;
	private String dcCourierCode;
	private String dcPincode;
	private Boolean dcSelfShipSupport;	
	
	public TripDeliveryStaffEntry() {
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public String getMobile() {
		return mobile;
	}

	public Boolean getAvailable() {
		return available;
	}

	public Boolean getDeleted() {
		return deleted;
	}

	public String getCode() {
		return code;
	}

	public String getDcCode() {
		return dcCode;
	}

	public String getDcName() {
		return dcName;
	}

	public String getDcManager() {
		return dcManager;
	}

	public String getDcContactNumber() {
		return dcContactNumber;
	}

	public Boolean getDcActive() {
		return dcActive;
	}

	public String getDcCity() {
		return dcCity;
	}

	public String getDcCityCode() {
		return dcCityCode;
	}

	public String getDcState() {
		return dcState;
	}

	public String getDcAddress() {
		return dcAddress;
	}

	public String getDcCourierCode() {
		return dcCourierCode;
	}

	public String getDcPincode() {
		return dcPincode;
	}

	public Boolean getDcSelfShipSupport() {
		return dcSelfShipSupport;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public void setAvailable(Boolean available) {
		this.available = available;
	}

	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setDcCode(String dcCode) {
		this.dcCode = dcCode;
	}

	public void setDcName(String dcName) {
		this.dcName = dcName;
	}

	public void setDcManager(String dcManager) {
		this.dcManager = dcManager;
	}

	public void setDcContactNumber(String dcContactNumber) {
		this.dcContactNumber = dcContactNumber;
	}

	public void setDcActive(Boolean dcActive) {
		this.dcActive = dcActive;
	}

	public void setDcCity(String dcCity) {
		this.dcCity = dcCity;
	}

	public void setDcCityCode(String dcCityCode) {
		this.dcCityCode = dcCityCode;
	}

	public void setDcState(String dcState) {
		this.dcState = dcState;
	}

	public void setDcAddress(String dcAddress) {
		this.dcAddress = dcAddress;
	}

	public void setDcCourierCode(String dcCourierCode) {
		this.dcCourierCode = dcCourierCode;
	}

	public void setDcPincode(String dcPincode) {
		this.dcPincode = dcPincode;
	}

	public void setDcSelfShipSupport(Boolean dcSelfShipSupport) {
		this.dcSelfShipSupport = dcSelfShipSupport;
	}

	@Override
	public String toString() {
		return "TripDeliveryStaffEntry [firstName=" + firstName + ", lastName=" + lastName + ", mobile=" + mobile
				+ ", available=" + available + ", deleted=" + deleted + ", code=" + code + ", dcCode=" + dcCode
				+ ", dcName=" + dcName + ", dcManager=" + dcManager + ", dcContactNumber=" + dcContactNumber
				+ ", dcActive=" + dcActive + ", dcCity=" + dcCity + ", dcCityCode=" + dcCityCode + ", dcState="
				+ dcState + ", dcAddress=" + dcAddress + ", dcCourierCode=" + dcCourierCode + ", dcPincode="
				+ dcPincode + ", dcSelfShipSupport=" + dcSelfShipSupport + "]";
	}	
}