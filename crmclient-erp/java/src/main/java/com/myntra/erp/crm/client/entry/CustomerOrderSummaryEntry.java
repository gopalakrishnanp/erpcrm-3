package com.myntra.erp.crm.client.entry;

import javax.xml.bind.annotation.XmlRootElement;

import com.myntra.commons.entries.BaseEntry;

/**
 * Entry for Customer order summary fields
 * 
 * @author Pravin Mehta
 */
@XmlRootElement(name = "customerOrderSummary")
public class CustomerOrderSummaryEntry extends BaseEntry {

	private static final long serialVersionUID = 1;
	
	private ShipmentStatusRevenueEntry shipmentStatusRevenue;

	public CustomerOrderSummaryEntry() {
		super();
	}

	public ShipmentStatusRevenueEntry getShipmentStatusRevenue() {
		return shipmentStatusRevenue;
	}

	public void setShipmentStatusRevenue(ShipmentStatusRevenueEntry shipmentStatusRevenue) {
		this.shipmentStatusRevenue = shipmentStatusRevenue;
	}

}
