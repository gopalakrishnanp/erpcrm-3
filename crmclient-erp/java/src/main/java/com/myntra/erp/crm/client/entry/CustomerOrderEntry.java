package com.myntra.erp.crm.client.entry;

import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import com.myntra.commons.entries.BaseEntry;
import com.myntra.commons.tranformer.TransformIgnoreAttribute;

/**
 * Entry for customer order fields
 * 
 * @author Arun Kumar
 */
@XmlRootElement(name = "customerOrderEntry")
public class CustomerOrderEntry extends BaseEntry {

	private static final long serialVersionUID = 1;

	// order unique ids
	private Long orderId;
	private String invoiceId;

	// customer detail of the order
	private String login;
	private String userContactNo;
	private String customerName;
	private String receiverName;
	
	private CustomerOrderBillingAddressEntry billingAddress;

	// shipment detail
	private List<CustomerOrderShipmentEntry> orderShipments;

	// different amount of order
	private Double mrpTotal;
	private Double finalAmount;
	private Double discount;
	private Double couponDiscount;
	private Double cartDiscount;
	private Double cashRedeemed;
	private Double pgDiscount;
	private Double shippingCharge;
	private Double giftCharge;
	private Double codCharge;
	private Double cashbackOffered;
	private Double taxAmount;
	private Double emiCharge;
	
	// dates
	private Date queuedOn;
	private Date cancelledOn;

	// order status and reasons
	private String orderStatus;
	private String orderStatusDisplay;
	private String orderType;
	private String orderTypeDisplay;
	private String cancellationReason;
	private Long cancellationReasonId;
	private String onHoldReason;
	private Long onHoldReasonId;
	private boolean onHold;
	private boolean giftOrder;
	private String notes;

	// discount coupon/cashback
	private String couponCode;
	private String cashBackCouponCode;

	// payment detail
	private String paymentMethod;
	private String paymentMethodDisplay;
	private String paymentMethodCardBankName;

	private String requestServer;
	private String responseServer;
	
	private Integer quantity;
	private boolean isMinimumOrder;

	public CustomerOrderEntry() {
	}

	@XmlElementWrapper(name = "customerOrderShipments")
	@XmlElement(name = "customerOrderShipment")
	@TransformIgnoreAttribute
	public List<CustomerOrderShipmentEntry> getOrderShipments() {
		return orderShipments;
	}

	public void setOrderShipments(List<CustomerOrderShipmentEntry> orderShipments) {
		this.orderShipments = orderShipments;
	}

	@XmlElement(name = "customerOrderBillingAddress")
	@TransformIgnoreAttribute
	public CustomerOrderBillingAddressEntry getBillingAddress() {
		return billingAddress;
	}

	public void setBillingAddress(CustomerOrderBillingAddressEntry billingAddress) {
		this.billingAddress = billingAddress;
	}

	public Long getOrderId() {
		return orderId;
	}

	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}

	public boolean getOnHold() {
		return onHold;
	}

	public void setOnHold(boolean onHold) {
		this.onHold = onHold;
	}

	@TransformIgnoreAttribute
	public Long getOnHoldReasonId() {
		return onHoldReasonId;
	}

	public void setOnHoldReasonId(Long onHoldReasonId) {
		this.onHoldReasonId = onHoldReasonId;
	}

	@TransformIgnoreAttribute
	public Long getCancellationReasonId() {
		return cancellationReasonId;
	}

	public void setCancellationReasonId(Long cancellationReasonId) {
		this.cancellationReasonId = cancellationReasonId;
	}

	public String getInvoiceId() {
		return invoiceId;
	}

	public void setInvoiceId(String invoiceId) {
		this.invoiceId = invoiceId;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	@TransformIgnoreAttribute
	public String getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}

	@TransformIgnoreAttribute
	public String getOrderStatusDisplay() {
		return orderStatusDisplay;
	}

	public void setOrderStatusDisplay(String orderStatusDisplay) {
		this.orderStatusDisplay = orderStatusDisplay;
	}

	public String getOrderType() {
		return orderType;
	}	

	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}
	
	public String getOrderTypeDisplay() {
		return orderTypeDisplay;
	}

	public void setOrderTypeDisplay(String orderTypeDisplay) {
		this.orderTypeDisplay = orderTypeDisplay;
	}

	@TransformIgnoreAttribute
	public String getPaymentMethod() {
		return paymentMethod;
	}

	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	public void setPaymentMethodDisplay(String paymentMethodDisplay) {
		this.paymentMethodDisplay = paymentMethodDisplay;
	}

	@TransformIgnoreAttribute
	public String getPaymentMethodDisplay() {
		return paymentMethodDisplay;
	}
	
	public String getPaymentMethodCardBankName() {
		return paymentMethodCardBankName;
	}

	public void setPaymentMethodCardBankName(String paymentCardBankName) {
		this.paymentMethodCardBankName = paymentCardBankName;
	}

	public String getCouponCode() {
		return couponCode;
	}

	public void setCouponCode(String couponCode) {
		this.couponCode = couponCode;
	}

	public String getCashBackCouponCode() {
		return cashBackCouponCode;
	}

	public void setCashBackCouponCode(String cashBackCouponCode) {
		this.cashBackCouponCode = cashBackCouponCode;
	}

	@TransformIgnoreAttribute
	public Double getMrpTotal() {
		return mrpTotal;
	}

	public void setMrpTotal(Double mrpTotal) {
		this.mrpTotal = mrpTotal;
	}

	@TransformIgnoreAttribute
	public Double getDiscount() {
		return discount;
	}

	public void setDiscount(Double discount) {
		this.discount = discount;
	}

	@TransformIgnoreAttribute
	public Double getCouponDiscount() {
		return couponDiscount;
	}

	public void setCouponDiscount(Double couponDiscount) {
		this.couponDiscount = couponDiscount;
	}

	@TransformIgnoreAttribute
	public Double getCartDiscount() {
		return cartDiscount;
	}

	@TransformIgnoreAttribute
	public void setCartDiscount(Double cartDiscount) {
		this.cartDiscount = cartDiscount;
	}

	@TransformIgnoreAttribute
	public Double getCashRedeemed() {
		return cashRedeemed;
	}

	public void setCashRedeemed(Double cashRedeemed) {
		this.cashRedeemed = cashRedeemed;
	}

	@TransformIgnoreAttribute
	public Double getPgDiscount() {
		return pgDiscount;
	}

	public void setPgDiscount(Double pgDiscount) {
		this.pgDiscount = pgDiscount;
	}

	@TransformIgnoreAttribute
	public Double getShippingCharge() {
		return shippingCharge;
	}

	public void setShippingCharge(Double shippingCharge) {
		this.shippingCharge = shippingCharge;
	}

	@TransformIgnoreAttribute
	public Double getGiftCharge() {
		return giftCharge;
	}

	public void setGiftCharge(Double giftCharge) {
		this.giftCharge = giftCharge;
	}

	@TransformIgnoreAttribute
	public Double getCodCharge() {
		return codCharge;
	}

	public void setCodCharge(Double codCharge) {
		this.codCharge = codCharge;
	}

	@TransformIgnoreAttribute
	public Double getFinalAmount() {
		return finalAmount;
	}

	public void setFinalAmount(Double finalAmount) {
		this.finalAmount = finalAmount;
	}

	@TransformIgnoreAttribute
	public Double getCashbackOffered() {
		return cashbackOffered;
	}

	public void setCashbackOffered(Double cashbackOffered) {
		this.cashbackOffered = cashbackOffered;
	}

	@TransformIgnoreAttribute
	public Double getTaxAmount() {
		return taxAmount;
	}

	public void setTaxAmount(Double taxAmount) {
		this.taxAmount = taxAmount;
	}
	
	public String getRequestServer() {
		return requestServer;
	}

	public void setRequestServer(String requestServer) {
		this.requestServer = requestServer;
	}

	public String getResponseServer() {
		return responseServer;
	}

	public void setResponseServer(String responseServer) {
		this.responseServer = responseServer;
	}

	@TransformIgnoreAttribute
	public String getCancellationReason() {
		return cancellationReason;
	}

	public void setCancellationReason(String cancellationReason) {
		this.cancellationReason = cancellationReason;
	}

	@TransformIgnoreAttribute
	public String getOnHoldReason() {
		return onHoldReason;
	}

	public void setOnHoldReason(String onHoldReason) {
		this.onHoldReason = onHoldReason;
	}

	public String getUserContactNo() {
		return userContactNo;
	}

	public void setUserContactNo(String userContactNo) {
		this.userContactNo = userContactNo;
	}

	public Double getEmiCharge() {
		return emiCharge;
	}

	public void setEmiCharge(Double emiCharge) {
		this.emiCharge = emiCharge;
	}

	public Date getQueuedOn() {
		return queuedOn;
	}

	public void setQueuedOn(Date queuedOn) {
		this.queuedOn = queuedOn;
	}

	public Date getCancelledOn() {
		return cancelledOn;
	}

	public void setCancelledOn(Date cancelledOn) {
		this.cancelledOn = cancelledOn;
	}

	public boolean isGiftOrder() {
		return giftOrder;
	}

	public void setGiftOrder(boolean giftOrder) {
		this.giftOrder = giftOrder;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	
	public String getReceiverName() {
		return receiverName;
	}

	public void setReceiverName(String receiverName) {
		this.receiverName = receiverName;
	}
	
	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	
	public boolean isMinimumOrder() {
		return isMinimumOrder;
	}

	public boolean setIsMinimumOrder(boolean flag) {
		return isMinimumOrder = flag;
	}
	
	public void setMinimumOrder(boolean isMinimumOrder) {
		this.isMinimumOrder = isMinimumOrder;
	}

	@Override
	public String toString() {
		return "CustomerOrderEntry [orderId=" + orderId + ", invoiceId=" + invoiceId + ", login=" + login
				+ ", userContactNo=" + userContactNo + ", customerName=" + customerName + ", receiverName="
				+ receiverName + ", billingAddress=" + billingAddress + ", orderShipments=" + orderShipments
				+ ", mrpTotal=" + mrpTotal + ", finalAmount=" + finalAmount + ", discount=" + discount
				+ ", couponDiscount=" + couponDiscount + ", cartDiscount=" + cartDiscount + ", cashRedeemed="
				+ cashRedeemed + ", pgDiscount=" + pgDiscount + ", shippingCharge=" + shippingCharge + ", giftCharge="
				+ giftCharge + ", codCharge=" + codCharge + ", cashbackOffered=" + cashbackOffered + ", taxAmount="
				+ taxAmount + ", emiCharge=" + emiCharge + ", queuedOn=" + queuedOn + ", cancelledOn=" + cancelledOn
				+ ", orderStatus=" + orderStatus + ", orderStatusDisplay=" + orderStatusDisplay + ", orderType="
				+ orderType + ", orderTypeDisplay=" + orderTypeDisplay + ", cancellationReason=" + cancellationReason
				+ ", cancellationReasonId=" + cancellationReasonId + ", onHoldReason=" + onHoldReason
				+ ", onHoldReasonId=" + onHoldReasonId + ", onHold=" + onHold + ", giftOrder=" + giftOrder + ", notes="
				+ notes + ", couponCode=" + couponCode + ", cashBackCouponCode=" + cashBackCouponCode
				+ ", paymentMethod=" + paymentMethod + ", paymentMethodDisplay=" + paymentMethodDisplay
				+ ", paymentMethodCardBankName=" + paymentMethodCardBankName + ", requestServer=" + requestServer
				+ ", responseServer=" + responseServer + ", quantity=" + quantity + ", isMinimumOrder="
				+ isMinimumOrder + "]";
	}
	
}